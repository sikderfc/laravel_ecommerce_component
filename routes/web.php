<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\WelcomeController;
use App\Http\Controllers\AdminController;
use App\Http\Controllers\ProductControllers;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//website Route Without APP
Route::get('/home', [WelcomeController::class, 'home']);
Route::get('/about', [WelcomeController::class, 'about']);
Route::get('/contact', [WelcomeController::class, 'contact']);
Route::get('/short', [WelcomeController::class, 'short']);
Route::get('/login', [WelcomeController::class, 'login']);
//Quick product Detail file start

Route::get('/product_view_by_id', [WelcomeController::class, 'product_view_by_id']);
//end 
//checkout start

Route::get('/checkout', [WelcomeController::class, 'checkout']);
//end 



// Admin Route

Route::get('/dashboard', [AdminController::class, 'dashboard']);

//product file start

Route::get('/add_product', [ProductControllers::class, 'add_product'])->name('add_product_index');
Route::get('/all_product', [ProductControllers::class, 'all_product'])->name('all_product_index');
Route::get('/show_product/{id}', [ProductControllers::class, 'show_product'])->name('show_product');
Route::get('/delete_product/{id}', [ProductControllers::class, 'delete_product'])->name('delete_product');


//Route::get('/edit_product', [AdminController::class, 'edit_product']);
//end product file

//order Start
Route::get('/all_order', [AdminController::class, 'all_order']);
Route::get('/processing_order', [AdminController::class, 'processing_order']);
Route::get('/pending_order', [AdminController::class, 'pending_order']);
Route::get('/completed_order', [AdminController::class, 'completed_order']);
Route::get('/decline_order', [AdminController::class, 'decline_order']);
//end
//customer
Route::get('/customer_list', [AdminController::class, 'customer_list']);
Route::get('/withdrows', [AdminController::class, 'withdrows']);
Route::get('/transactions', [AdminController::class, 'transactions']);

//end
//website setting Start
Route::get('/home_slider', [AdminController::class, 'home_slider']);
Route::get('/home_featuredbanner', [AdminController::class, 'home_featuredbanner']);
Route::get('/home_service', [AdminController::class, 'home_service']);
Route::get('/home_best_seller', [AdminController::class, 'home_best_seller']);
Route::get('/home_big_save', [AdminController::class, 'home_big_save']);
Route::get('/home_small_banner', [AdminController::class, 'home_small_banner']);
Route::get('/home_large_banner', [AdminController::class, 'home_large_banner']);
Route::get('/home_top_small_banner', [AdminController::class, 'home_top_small_banner']);
Route::get('/page-settings', [AdminController::class, 'page-settings']);
//end
//end







