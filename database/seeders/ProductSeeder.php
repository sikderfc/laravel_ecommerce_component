<?php

namespace Database\Seeders;

use App\Models\Product;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for($i = 1;$i<=100;$i++){
            Product::create([
                'product_name' => 'Pant',
                'product_squ' => '687264238',
                'current_price' => '300',
                'privious_price' => '560',
                'current_stock' => '800',
                'description' => 'lorem ipsom  lorem ipsomlorem ipsomv lorem ipsom lorem ipsom lorem ipsom',
                'tag' => 'p343',
            ]);
        }
        
    }
}
