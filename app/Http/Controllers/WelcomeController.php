<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class WelcomeController extends Controller
{
    public function home(){
        return view('website/home');
    }
    public function about(){
        return view('website/about');
    }
    public function contact(){
        return view('website/contact');
    }
    public function short(){
        return view('website/short');
    }
    public function login(){
        return view('website/login');
    }
    public function product_view_by_id(){
        return view('website/product_view_by_id');
    }
    public function checkout(){
        return view('website/checkout');
    }
}
