<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AdminController extends Controller
{
    public function dashboard(){
        return view('admin/dashboard');
    }
    // Product Section
    
    
    //Order Section
    public function all_order(){
        return view('admin/order/all_order');
    }
    public function processing_order(){
        return view('admin/order/processing_order');
    }
    public function pending_order(){
        return view('admin/order/pending_order');
    }
    public function completed_order(){
        return view('admin/order/completed_order');
    }
    public function decline_order(){
        return view('admin/order/decline_order');
    }
    //Customer Section
    public function customer_list(){
        return view('admin/customer/customer_list');
    }
    public function withdrows(){
        return view('admin/customer/withdrows');
    }
    public function transactions(){
        return view('admin/customer/transactions');
    }
}
