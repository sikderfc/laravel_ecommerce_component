	<!--  Ending of subscribe-pre-loader Area   -->
	<section class="top-header">
		<div class="container">
			<div class="row">
				<div class="col-lg-12 remove-padding">
					<div class="content">
						<div class="left-content">
							<div class="list">
								<ul>


									<li>
										<div class="language-selector">
											<i class="fas fa-globe-americas"></i>
											<select name="language" class="language selectors nice">
												<option
													value="https://royalscripts.com/product/kingcommerce/fashion/language/1">
													English</option>
												<option
													value="https://royalscripts.com/product/kingcommerce/fashion/language/2"
													selected>English</option>
											</select>
										</div>
									</li>

									<li>
										<div class="currency-selector">
											<span></span>
											<select name="currency" class="currency selectors nice">
												
												<option
													value="">
													BDT</option>
												
												
											</select>
										</div>
									</li>


								</ul>
							</div>
						</div>
						<div class="right-content">
							<div class="list">
								<ul>
									<li class="login">
										<a href="{{url('login') }}" class="sign-log">
											<div class="links">
												<span class="sign-in">Sign in</span> <span>|</span>
												<span class="join">Join</span>
											</div>
										</a>
									</li>


									<li>
									<li>
										<a href="javascript:;" data-toggle="modal" data-target="#vendor-login"
											class="sell-btn">Sell</a>
									</li>


								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- Top Header Area End -->

	<!-- Logo Header Area Start -->
	<section class="logo-header">
		<div class="container">
			<div class="row ">
				<div class="col-lg-2 col-sm-6 col-5 remove-padding">
					<div class="logo">
						<a href="{{url('home') }}">
							<img src="{{ asset('assets/website/images') }}/1581932983logo.png" alt="">
						</a>
					</div>
				</div>
				<div class="col-lg-8 col-sm-12 remove-padding order-last order-sm-2 order-md-2">
					<div class="search-box-wrapper">
						<div class="search-box">
							<div class="categori-container" id="catSelectForm">
								<select name="category" id="category_select" class="categoris">
									<option value="">All Categories</option>
									<option value="Accessories">Accessories</option>
									<option value="Beauty">Beauty</option>
									<option value="Bags">Bags</option>
									<option value="Shoes">Shoes</option>
									<option value="Jewellery">Jewellery</option>
									<option value="Jeans">Jeans</option>
									<option value="Shirts">Shirts</option>
									<option value="Coats-and-Jackets">Coats &amp; Jackets</option>
									<option value="T-shirt-and-Tops">T-shirt &amp; Tops</option>
									<option value="Shorts">Shorts</option>
								</select>
							</div>

							<form id="searchForm" class="search-form"
								action="https://royalscripts.com/product/kingcommerce/fashion/category" method="GET">
								<input type="text" id="prod_name" name="search" placeholder="Search For Product"
									value="" autocomplete="off">
								<div class="autocomplete">
									<div id="myInputautocomplete-list" class="autocomplete-items">
									</div>
								</div>
								<button type="submit"><i class="icofont-search-1"></i></button>
							</form>
						</div>
					</div>
				</div>
				<div class="col-lg-2 col-sm-6 col-7 remove-padding order-lg-last">
					<div class="helpful-links">
						<ul class="helpful-links-inner">
							<li class="my-dropdown" data-toggle="tooltip" data-placement="top" title="Cart">
								<a href="{{url('checkout')}}" class="cart carticon">
									<div class="icon">
										<i class="icofont-cart"></i>
										<span class="cart-quantity" id="cart-count">0</span>
									</div>

								</a>
								<div class="my-dropdown-menu" id="cart-items">
									<p class="mt-1 pl-3 text-left">Cart is empty.</p>
								</div>
							</li>
							<li class="wishlist" data-toggle="tooltip" data-placement="top" title="Wish">
								<a href="javascript:;" data-toggle="modal" id="wish-btn" data-target="#comment-log-reg"
									class="wish">
									<i class="far fa-heart"></i>
									<span id="wishlist-count">0</span>
								</a>
							</li>
							<li class="compare" data-toggle="tooltip" data-placement="top" title="Compare">
								<a href="item/compare/view.html" class="wish compare-product">
									<div class="icon">
										<i class="fas fa-exchange-alt"></i>
										<span id="compare-count">0</span>
									</div>
								</a>
							</li>


						</ul>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- Logo Header Area End -->

	<!--Main-Menu Area Start-->
	<div class="mainmenu-area mainmenu-bb">
		<div class="container">
			<div class="row align-items-center mainmenu-area-innner">
				<div class="col-lg-3 col-md-6 categorimenu-wrapper remove-padding">
					<!--categorie menu start-->
					<div class="categories_menu">
						<div class="categories_title">
							<h2 class="categori_toggle"><i class="fa fa-bars"></i> Categories <i
									class="fa fa-angle-down arrow-down"></i></h2>
						</div>
						<div class="categories_menu_inner">
							<ul>

								<li class="dropdown_list ">
									<div class="img">
										<img src="{{ asset('assets/website/images/categories') }}/1615873050eyeglasses.png" alt="">
									</div>
									<div class="link-area">
										<span><a href="category/Accessories.html">Accessories</a></span>
										<a href="javascript:;">
											<i class="fa fa-angle-right" aria-hidden="true"></i>
										</a>
									</div>


									<ul class="categories_mega_menu column_1">
										<li>
											<a href="category/Accessories/Belts.html">Belts</a>
										</li>
										<li>
											<a href="category/Accessories/Sunglasses.html">Sunglasses</a>
										</li>
										<li>
											<a href="category/Accessories/Hats.html">Hats</a>
										</li>
									</ul>


								</li>





								<li class="dropdown_list ">
									<div class="img">
										<img src="{{ asset('assets/website/images/categories') }}/1615873983makeover.png" alt="">
									</div>
									<div class="link-area">
										<span><a href="category/Beauty.html">Beauty</a></span>
										<a href="javascript:;">
											<i class="fa fa-angle-right" aria-hidden="true"></i>
										</a>
									</div>


									<ul class="categories_mega_menu column_1">
										<li>
											<a href="category/Beauty/Hair-Care.html">Hair Care</a>
										</li>
										<li>
											<a href="category/Beauty/Makeup.html">Makeup</a>
										</li>
										<li>
											<a href="category/Beauty/Nails.html">Nails</a>
										</li>
										<li>
											<a href="category/Beauty/Skin-Care.html">Skin Care</a>
										</li>
									</ul>


								</li>





								<li class="dropdown_list ">
									<div class="img">
										<img src="{{ asset('assets/website/images/categories') }}/1615874066bag.png" alt="">
									</div>
									<div class="link-area">
										<span><a href="category/Bags.html">Bags</a></span>
										<a href="javascript:;">
											<i class="fa fa-angle-right" aria-hidden="true"></i>
										</a>
									</div>


									<ul class="categories_mega_menu column_1">
										<li>
											<a href="category/Bags/Backpacks.html">Backpacks</a>
										</li>
										<li>
											<a href="category/Bags/Mini-Bags.html">Mini Bags</a>
										</li>
										<li>
											<a href="category/Bags/Wallets.html">Wallets</a>
										</li>
									</ul>


								</li>





								<li class="dropdown_list ">
									<div class="img">
										<img src="{{ asset('assets/website/images/categories') }}/1615874559high-heels.png" alt="">
									</div>
									<div class="link-area">
										<span><a href="category/Shoes.html">Shoes</a></span>
										<a href="javascript:;">
											<i class="fa fa-angle-right" aria-hidden="true"></i>
										</a>
									</div>


									<ul class="categories_mega_menu column_1">
										<li>
											<a href="category/Shoes/Boots.html">Boots</a>
										</li>
										<li>
											<a href="category/Shoes/Heels.html">Heels</a>
										</li>
										<li>
											<a href="category/Shoes/Sandals.html">Sandals</a>
										</li>
									</ul>


								</li>





								<li class="dropdown_list ">
									<div class="img">
										<img src="{{ asset('assets/website/images/categories') }}/1615874586gems.png" alt="">
									</div>
									<div class="link-area">
										<span><a href="category/Jewellery.html">Jewellery</a></span>
										<a href="javascript:;">
											<i class="fa fa-angle-right" aria-hidden="true"></i>
										</a>
									</div>


									<ul class="categories_mega_menu column_1">
										<li>
											<a href="category/Jewellery/Neckless.html">Neckless</a>
										</li>
										<li>
											<a href="category/Jewellery/Bracelets.html">Bracelets</a>
										</li>
										<li>
											<a href="category/Jewellery/Rings.html">Rings</a>
										</li>
									</ul>


								</li>





								<li class=" ">
									<a href="category/Jeans.html"><img
											src="{{ asset('assets/website/images/categories') }}/1615874606jeans.png"> Jeans</a>


								</li>





								<li class=" ">
									<a href="category/Shirts.html"><img
											src="{{ asset('assets/website/images/categories') }}/1615874633cloth.png"> Shirts</a>


								</li>





								<li class=" ">
									<a href="category/Coats-and-Jackets.html"><img
											src="{{ asset('assets/website/images/categories') }}/1615874670jacket.png"> Coats &amp; Jackets</a>


								</li>





								<li class=" ">
									<a href="category/T-shirt-and-Tops.html"><img
											src="{{ asset('assets/website/images/categories') }}/1615874704shirt.png"> T-shirt &amp; Tops</a>


								</li>





								<li class=" ">
									<a href="category/Shorts.html"><img
											src="{{ asset('assets/website/images/categories') }}/1615874751shorts.png"> Shorts</a>


								</li>





							</ul>
						</div>
					</div>
					<!--categorie menu end-->
				</div>
				<div class="col-lg-9 col-md-6 mainmenu-wrapper remove-padding">
					<nav hidden>
						<div class="nav-header">
							<button class="toggle-bar"><span class="fa fa-bars"></span></button>
						</div>
						<ul class="menu">
							<li><a href="blog.html">Blog</a></li>
							<li><a href="faq.html">Faq</a></li>
							<li><a href="{{url('about') }}">About Us</a></li>
							<li><a href="{{url('contact') }}">Contact Us</a></li>
							<li>
								<a href="javascript:;" data-toggle="modal" data-target="#track-order-modal"
									class="track-btn">Track Order</a>
							</li>
						</ul>

					</nav>
				</div>
			</div>
		</div>
	</div>
	<!--Main-Menu Area End-->