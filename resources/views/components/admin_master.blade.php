
<!doctype html>
<html lang="en" dir="ltr">

<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
    	<meta name="author" content="GeniusOcean">
    	<meta name="csrf-token" content="94hYTM2AyRLTfZyJPvfIxLf7aCnuXc9H6EF2GeDK">
		<!-- Title -->
		<title>Kingcommerce</title>
		<!-- favicon -->
		<link rel="icon"  type="image/x-icon" href="{{ asset('assets/admin/images') }}/1571567283favicon.png"/>
		<!-- Bootstrap -->
		<link href="{{ asset('assets/admin/css') }}/bootstrap.min.css" rel="stylesheet" />
		<!-- Fontawesome -->
		<link rel="stylesheet" href="{{ asset('assets/admin/css') }}/fontawesome.css">
		<!-- icofont -->
		<link rel="stylesheet" href="{{ asset('assets/admin/css') }}/icofont.min.css">
		<!-- Sidemenu Css -->
		<link href="{{ asset('assets/admin/css') }}/dark-side-style.css" rel="stylesheet" />
		<link href="{{ asset('assets/admin/css') }}/waves.min.css" rel="stylesheet" />

		<link href="{{ asset('assets/admin/css') }}/plugin.css" rel="stylesheet" />

		<link href="{{ asset('assets/admin/css') }}/jquery.tagit.css" rel="stylesheet" />
    	<link rel="stylesheet" href="{{ asset('assets/admin/css') }}/bootstrap-coloroicker.css">
		<!-- Main Css -->

		<!-- stylesheet -->
		
		<link href="{{ asset('assets/admin/css') }}/style.css" rel="stylesheet"/>
		<link href="{{ asset('assets/admin/css') }}/custom.css" rel="stylesheet"/>
		<link href="{{ asset('assets/admin/css') }}/responsive.css" rel="stylesheet" />
		<link href="{{ asset('assets/admin/css') }}/common.css" rel="stylesheet" />
		
		
	</head>
	<body>
		<div class="page">
			<div class="page-main">
				<!-- Header Menu Area Start -->
				
				<div class="header">
					<div class="container-fluid">
						<div class="d-flex justify-content-between">
							<div class="menu-toggle-button">
								<a class="nav-link" href="javascript:;" id="sidebarCollapse">
									<div class="my-toggl-icon">
											<span class="bar1"></span>
											<span class="bar2"></span>
											<span class="bar3"></span>
									</div>
								</a>
							</div>
				
							<div class="right-eliment">
								<ul class="list">
				
									<li class="bell-area">
									<a id="notf_conv" class="dropdown-toggle-1" target="_blank" href="https://royalscripts.com/demo/kingcommerce">
										<i class="fas fa-globe-americas"></i>
										</a>
				
									</li>
				
				
									<li class="bell-area">
										<a id="notf_conv" class="dropdown-toggle-1" href="javascript:;">
											<i class="far fa-envelope"></i>
											<span data-href="https://royalscripts.com/demo/kingcommerce/admin/conv/notf/count" id="conv-notf-count">0</span>
										</a>
										<div class="dropdown-menu">
											<div class="dropdownmenu-wrapper" data-href="https://royalscripts.com/demo/kingcommerce/admin/conv/notf/show" id="conv-notf-show">
										</div>
										</div>
									</li>
				
									<li class="bell-area">
										<a id="notf_product" class="dropdown-toggle-1" href="javascript:;">
											<i class="icofont-cart"></i>
											<span data-href="https://royalscripts.com/demo/kingcommerce/admin/product/notf/count" id="product-notf-count">0</span>
										</a>
										<div class="dropdown-menu">
											<div class="dropdownmenu-wrapper" data-href="https://royalscripts.com/demo/kingcommerce/admin/product/notf/show" id="product-notf-show">
										</div>
										</div>
									</li>
				
									<li class="bell-area">
										<a id="notf_user" class="dropdown-toggle-1" href="javascript:;">
											<i class="far fa-user"></i>
											<span data-href="https://royalscripts.com/demo/kingcommerce/admin/user/notf/count" id="user-notf-count">0</span>
										</a>
										<div class="dropdown-menu">
											<div class="dropdownmenu-wrapper" data-href="https://royalscripts.com/demo/kingcommerce/admin/user/notf/show" id="user-notf-show">
										</div>
										</div>
									</li>
				
									<li class="bell-area">
										<a id="notf_order" class="dropdown-toggle-1" href="javascript:;">
											<i class="far fa-newspaper"></i>
											<span data-href="https://royalscripts.com/demo/kingcommerce/admin/order/notf/count" id="order-notf-count">0</span>
										</a>
										<div class="dropdown-menu">
											<div class="dropdownmenu-wrapper" data-href="https://royalscripts.com/demo/kingcommerce/admin/order/notf/show" id="order-notf-show">
										</div>
										</div>
									</li>
				
									<li class="login-profile-area">
										<a class="dropdown-toggle-1" href="javascript:;">
											<div class="user-img">
												<img src="{{ asset('assets/admin/images') }}/admins/1556780563user.png" alt="">
											</div>
										</a>
										<div class="dropdown-menu">
											<div class="dropdownmenu-wrapper">
													<ul>
														<h5>Welcome!</h5>
															<li>
																<a href="https://royalscripts.com/demo/kingcommerce/admin/profile"><i class="fas fa-user"></i> Edit Profile</a>
															</li>
															<li>
																<a href="https://royalscripts.com/demo/kingcommerce/admin/password"><i class="fas fa-cog"></i> Change Password</a>
															</li>
															<li>
																<a href="https://royalscripts.com/demo/kingcommerce/admin/logout"><i class="fas fa-power-off"></i> Logout</a>
															</li>
														</ul>
											</div>
										</div>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
					





				<!-- Header Menu Area End -->
				<div class="wrapper">
					<!-- Side Menu Area Start -->
					<nav id="sidebar" class="nav-sidebar">
						<ul class="list-unstyled components" id="accordion">
							<li>
								<a href="https://royalscripts.com/demo/kingcommerce/admin" class="wave-effect active"><i class="fa fa-home mr-2"></i>Dashboard</a>
							</li>
														<li>
        <a href="#order" class="accordion-toggle wave-effect" data-toggle="collapse" aria-expanded="false"><i class="fas fa-hand-holding-usd"></i>Orders</a>
        <ul class="collapse list-unstyled" id="order" data-parent="#accordion" >
               <li>
                <a href="{{url('all_order')}}"> All Orders</a>
            </li>
            <li>
                <a href="{{url('pending_order')}}"> Pending Orders</a>
            </li>
            <li>
                <a href="{{url('processing_order')}}"> Processing Orders</a>
            </li>
            <li>
                <a href="{{url('completed_order')}}"> Completed Orders</a>
            </li>
            <li>
                <a href="{{url('decline_order')}}"> Declined Orders</a>
            </li>

        </ul>
    </li>
    <li>
        <a href="#menu2" class="accordion-toggle wave-effect" data-toggle="collapse" aria-expanded="false">
            <i class="icofont-cart"></i>Products
        </a>
        <ul class="collapse list-unstyled" id="menu2" data-parent="#accordion">
            <li>
                <a href="{{route('add_product_index')}}"><span>Add New Product</span></a>
            </li>
            <li>
                <a href="https://royalscripts.com/demo/kingcommerce/admin/products"><span>All Products</span></a>
            </li>
            <li>
                <a href="https://royalscripts.com/demo/kingcommerce/admin/products/deactive"><span>Deactivated Product</span></a>
            </li>
            <li>
                <a href="https://royalscripts.com/demo/kingcommerce/admin/products/catalogs"><span>Product Catalogs</span></a>
            </li>
        </ul>
    </li>

    <li>
        <a href="#affiliateprod" class="accordion-toggle wave-effect" data-toggle="collapse" aria-expanded="false">
            <i class="icofont-cart"></i>Affiliate Products
        </a>
        <ul class="collapse list-unstyled" id="affiliateprod" data-parent="#accordion">
            <li>
                <a href="https://royalscripts.com/demo/kingcommerce/admin/products/import/create"><span>Add Affiliate Product</span></a>
            </li>
            <li>
                <a href="https://royalscripts.com/demo/kingcommerce/admin/products/import/index"><span>All Affiliate Products</span></a>
            </li>
        </ul>
    </li>

    <li>
        <a href="#menu3" class="accordion-toggle wave-effect" data-toggle="collapse" aria-expanded="false">
            <i class="icofont-user"></i>Customers
        </a>
        <ul class="collapse list-unstyled" id="menu3" data-parent="#accordion">
            <li>
                <a href="{{url('customer_list')}}"><span>Customers List</span></a>
            </li>
            <li>
                <a href="{{url('withdrows')}}"><span>Withdraws</span></a>
            </li>
            <li>
                <a href="{{url('transactions')}}"><span>Transactions</span></a>
            </li>
            
        </ul>
    </li>

    <li>
        <a href="#vendor" class="accordion-toggle wave-effect" data-toggle="collapse" aria-expanded="false">
            <i class="icofont-ui-user-group"></i>Vendors
        </a>
        <ul class="collapse list-unstyled" id="vendor" data-parent="#accordion">
            <li>
                <a href="https://royalscripts.com/demo/kingcommerce/admin/vendors"><span>Vendors List</span></a>
            </li>
            <li>
                <a href="https://royalscripts.com/demo/kingcommerce/admin/vendors/withdraws"><span>Withdraws</span></a>
            </li>
            <li>
                <a href="https://royalscripts.com/demo/kingcommerce/admin/vendors/subs"><span>Vendor Subscriptions</span></a>
            </li>
            <li>
                <a href="https://royalscripts.com/demo/kingcommerce/admin/vendor/color"><span>Default Background</span></a>
            </li>

        </ul>
    </li>

    <li>
        <a href="#vendor1" class="accordion-toggle wave-effect" data-toggle="collapse" aria-expanded="false">
                <i class="icofont-verification-check"></i>Vendor Verifications
        </a>
        <ul class="collapse list-unstyled" id="vendor1" data-parent="#accordion">
            <li>
                <a href="https://royalscripts.com/demo/kingcommerce/admin/verificatons"><span>All Verifications</span></a>
            </li>
            <li>
                <a href="https://royalscripts.com/demo/kingcommerce/admin/verificatons/pendings"><span>Pending Verifications</span></a>
            </li>
        </ul>
    </li>


    <li>
        <a href="https://royalscripts.com/demo/kingcommerce/admin/subscription" class=" wave-effect"><i class="fas fa-dollar-sign"></i>Vendor Subscription Plans</a>
    </li>

    <li>
        <a href="#menu5" class="accordion-toggle wave-effect" data-toggle="collapse" aria-expanded="false"><i class="fas fa-sitemap"></i>Manage Categories</a>
        <ul class="collapse list-unstyled
        " id="menu5" data-parent="#accordion" >
                <li class="">
                    <a href="https://royalscripts.com/demo/kingcommerce/admin/category"><span>Main Category</span></a>
                </li>
                <li class="">
                    <a href="https://royalscripts.com/demo/kingcommerce/admin/subcategory"><span>Sub Category</span></a>
                </li>
                <li class="">
                    <a href="https://royalscripts.com/demo/kingcommerce/admin/childcategory"><span>Child Category</span></a>
                </li>
        </ul>
    </li>

    <li>
        <a href="https://royalscripts.com/demo/kingcommerce/admin/products/import"><i class="fas fa-upload"></i>Bulk Product Upload</a>
    </li>

    <li>
        <a href="#menu4" class="accordion-toggle wave-effect" data-toggle="collapse" aria-expanded="false">
            <i class="icofont-speech-comments"></i>Product Discussion
        </a>
        <ul class="collapse list-unstyled" id="menu4" data-parent="#accordion">
            <li>
                <a href="https://royalscripts.com/demo/kingcommerce/admin/ratings"><span>Product Reviews</span></a>
            </li>
            <li>
                <a href="https://royalscripts.com/demo/kingcommerce/admin/comments"><span>Comments</span></a>
            </li>
            <li>
                <a href="https://royalscripts.com/demo/kingcommerce/admin/reports"><span>Reports</span></a>
            </li>
        </ul>
    </li>

    <li>
        <a href="https://royalscripts.com/demo/kingcommerce/admin/coupon" class=" wave-effect"><i class="fas fa-percentage"></i>Set Coupons</a>
    </li>
    <li>
        <a href="#blog" class="accordion-toggle wave-effect" data-toggle="collapse" aria-expanded="false">
            <i class="fas fa-fw fa-newspaper"></i>Blog
        </a>
        <ul class="collapse list-unstyled" id="blog" data-parent="#accordion">
            <li>
                <a href="https://royalscripts.com/demo/kingcommerce/admin/blog/category"><span>Categories</span></a>
            </li>
            <li>
                <a href="https://royalscripts.com/demo/kingcommerce/admin/blog"><span>Posts</span></a>
            </li>
        </ul>
    </li>

    <li>
        <a href="#msg" class="accordion-toggle wave-effect" data-toggle="collapse" aria-expanded="false">
            <i class="fas fa-fw fa-newspaper"></i>Messages
        </a>
        <ul class="collapse list-unstyled" id="msg" data-parent="#accordion">
            <li>
                <a href="https://royalscripts.com/demo/kingcommerce/admin/tickets"><span>Tickets</span></a>
            </li>
            <li>
                <a href="https://royalscripts.com/demo/kingcommerce/admin/disputes"><span>Disputes</span></a>
            </li>
        </ul>
    </li>

    <li>
        <a href="#general" class="accordion-toggle wave-effect" data-toggle="collapse" aria-expanded="false">
            <i class="fas fa-cogs"></i>General Settings
        </a>
        <ul class="collapse list-unstyled" id="general" data-parent="#accordion">
            <li>
                <a href="https://royalscripts.com/demo/kingcommerce/admin/general-settings/logo"><span>Logo</span></a>
            </li>
            <li>
                <a href="https://royalscripts.com/demo/kingcommerce/admin/general-settings/favicon"><span>Favicon</span></a>
            </li>
            <li>
                <a href="https://royalscripts.com/demo/kingcommerce/admin/general-settings/loader"><span>Loader</span></a>
            </li>
            <li>
                <a href="https://royalscripts.com/demo/kingcommerce/admin/shipping"><span>Shipping Methods</span></a>
            </li>
            <li>
                <a href="https://royalscripts.com/demo/kingcommerce/admin/package"><span>Packagings</span></a>
            </li>
            <li>
                <a href="https://royalscripts.com/demo/kingcommerce/admin/pickup"><span>Pickup Locations</span></a>
            </li>
            <li>
            <a href="https://royalscripts.com/demo/kingcommerce/admin/general-settings/contents"><span>Website Contents</span></a>
            </li>
            <li>
                <a href="https://royalscripts.com/demo/kingcommerce/admin/general-settings/footer"><span>Footer</span></a>
            </li>
            <li>
                <a href="https://royalscripts.com/demo/kingcommerce/admin/general-settings/affilate"><span>Affiliate Information</span></a>
            </li>

            <li>
                <a href="https://royalscripts.com/demo/kingcommerce/admin/general-settings/popup"><span>Popup Banner</span></a>
            </li>


            <li>
                <a href="https://royalscripts.com/demo/kingcommerce/admin/general-settings/error-banner"><span>Error Banner</span></a>
            </li>


            <li>
                <a href="https://royalscripts.com/demo/kingcommerce/admin/general-settings/maintenance"><span>Website Maintenance</span></a>
            </li>

        </ul>
    </li>

    <li>
        <a href="#homepage" class="accordion-toggle wave-effect" data-toggle="collapse" aria-expanded="false">
            <i class="fas fa-edit"></i>Home Page Settings
        </a>
        <ul class="collapse list-unstyled" id="homepage" data-parent="#accordion">
            <li>
                <a href="https://royalscripts.com/demo/kingcommerce/admin/slider"><span>Sliders</span></a>
            </li>
            <li>
                <a href="https://royalscripts.com/demo/kingcommerce/admin/featuredlink"><span>Featured Links</span></a>
            </li>
            <li>
                <a href="https://royalscripts.com/demo/kingcommerce/admin/featuredbanner"><span>Featured Banners</span></a>
            </li>
            <li>
                <a href="https://royalscripts.com/demo/kingcommerce/admin/service"><span>Services</span></a>
            </li>
            <li>
                <a href="https://royalscripts.com/demo/kingcommerce/admin/page-settings/best-seller"><span>Right Side Banner1</span></a>
            </li>
            <li>
                <a href="https://royalscripts.com/demo/kingcommerce/admin/page-settings/big-save"><span>Right Side Banner2</span></a>
            </li>
            <li>
                <a href="https://royalscripts.com/demo/kingcommerce/admin/top/small/banner"><span>Top Small Banners</span></a>
            </li>

            <li>
                <a href="https://royalscripts.com/demo/kingcommerce/admin/large/banner"><span>Large Banners</span></a>
            </li>
            <li>
                <a href="https://royalscripts.com/demo/kingcommerce/admin/bottom/small/banner"><span>Bottom Small Banners</span></a>
            </li>

            <li>
                <a href="https://royalscripts.com/demo/kingcommerce/admin/review"><span>Reviews</span></a>
            </li>
            <li>
                <a href="https://royalscripts.com/demo/kingcommerce/admin/partner"><span>Partners</span></a>
            </li>


            <li>
                <a href="https://royalscripts.com/demo/kingcommerce/admin/page-settings/customize"><span>Home Page Customization</span></a>
            </li>
        </ul>
    </li>

    <li>
        <a href="#menu" class="accordion-toggle wave-effect" data-toggle="collapse" aria-expanded="false">
            <i class="fas fa-file-code"></i>Menu Page Settings
        </a>
        <ul class="collapse list-unstyled" id="menu" data-parent="#accordion">
            <li>
                <a href="https://royalscripts.com/demo/kingcommerce/admin/faq"><span>FAQ Page</span></a>
            </li>
            <li>
                <a href="https://royalscripts.com/demo/kingcommerce/admin/page-settings/contact"><span>Contact Us Page</span></a>
            </li>
            <li>
                <a href="https://royalscripts.com/demo/kingcommerce/admin/page"><span>Other Pages</span></a>
            </li>
        </ul>
    </li>
    <li>
        <a href="#emails" class="accordion-toggle wave-effect" data-toggle="collapse" aria-expanded="false">
            <i class="fas fa-at"></i>Email Settings
        </a>
        <ul class="collapse list-unstyled" id="emails" data-parent="#accordion">
            <li><a href="https://royalscripts.com/demo/kingcommerce/admin/email-templates"><span>Email Template</span></a></li>
            <li><a href="https://royalscripts.com/demo/kingcommerce/admin/email-config"><span>Email Configurations</span></a></li>
            <li><a href="https://royalscripts.com/demo/kingcommerce/admin/groupemail"><span>Group Email</span></a></li>
        </ul>
    </li>
    <li>
        <a href="#payments" class="accordion-toggle wave-effect" data-toggle="collapse" aria-expanded="false">
            <i class="fas fa-file-code"></i>Payment Settings
        </a>
        <ul class="collapse list-unstyled" id="payments" data-parent="#accordion">
            <li><a href="https://royalscripts.com/demo/kingcommerce/admin/payment-informations"><span>Payment Information</span></a></li>
            <li><a href="https://royalscripts.com/demo/kingcommerce/admin/paymentgateway"><span>Payment Gateways</span></a></li>
            <li><a href="https://royalscripts.com/demo/kingcommerce/admin/currency"><span>Currencies</span></a></li>
        </ul>
    </li>
    <li>
        <a href="#socials" class="accordion-toggle wave-effect" data-toggle="collapse" aria-expanded="false">
            <i class="fas fa-paper-plane"></i>Social Settings
        </a>
        <ul class="collapse list-unstyled" id="socials" data-parent="#accordion">
                <li><a href="https://royalscripts.com/demo/kingcommerce/admin/social"><span>Social Links</span></a></li>
                <li><a href="https://royalscripts.com/demo/kingcommerce/admin/social/facebook"><span>Facebook Login</span></a></li>
                <li><a href="https://royalscripts.com/demo/kingcommerce/admin/social/google"><span>Google Login</span></a></li>
        </ul>
    </li>
    <li>
        <a href="#langs" class="accordion-toggle wave-effect" data-toggle="collapse" aria-expanded="false">
            <i class="fas fa-language"></i>Language Settings
        </a>
        <ul class="collapse list-unstyled" id="langs" data-parent="#accordion">
                <li><a href="https://royalscripts.com/demo/kingcommerce/admin/languages"><span>Website Language</span></a></li>
                <li><a href="https://royalscripts.com/demo/kingcommerce/admin/adminlanguages"><span>Admin Panel Language</span></a></li>

        </ul>
    </li>
    <li>
        <a href="#seoTools" class="accordion-toggle wave-effect" data-toggle="collapse" aria-expanded="false">
            <i class="fas fa-wrench"></i>SEO Tools
        </a>
        <ul class="collapse list-unstyled" id="seoTools" data-parent="#accordion">
            <li>
                <a href="https://royalscripts.com/demo/kingcommerce/admin/products/popular/30"><span>Popular Products</span></a>
            </li>
            <li>
                <a href="https://royalscripts.com/demo/kingcommerce/admin/seotools/analytics"><span>Google Analytics</span></a>
            </li
            >
            <li>
                <a href="https://royalscripts.com/demo/kingcommerce/admin/seotools/keywords"><span>Website Meta Keywords</span></a>
            </li>
        </ul>
    </li>
    <li>
        <a href="https://royalscripts.com/demo/kingcommerce/admin/staff" class=" wave-effect"><i class="fas fa-user-secret"></i>Manage Staffs</a>
    </li>

    <li>
        <a href="https://royalscripts.com/demo/kingcommerce/admin/subscribers" class=" wave-effect"><i class="fas fa-users-cog mr-2"></i>Subscribers</a>
    </li>

    <li>
        <a href="https://royalscripts.com/demo/kingcommerce/admin/role" class=" wave-effect"><i class="fas fa-user-tag"></i>Manage Roles</a>
    </li>
    <li>
        <a href="https://royalscripts.com/demo/kingcommerce/admin/cache/clear" class=" wave-effect"><i class="fas fa-sync"></i>Clear Cache</a>
    </li>
							
						</ul>
										<p class="version-name"> Version: 4.0</p>
										</nav>
					<!-- Main Content Area Start -->
					<div class="content-area" id="modalEdit">
    
									{{$slot}}



					<!-- Main Content Area End -->
					</div>
				</div>
			</div>

						<script data-cfasync="false" src="/cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js"></script><script type="text/javascript">
			  var mainurl = "https://royalscripts.com/demo/kingcommerce";
			  var admin_loader = 1;
			  var whole_sell = 6;
			  var getattrUrl = 'https://royalscripts.com/demo/kingcommerce/admin/getattributes';
			  var curr = {"id":1,"name":"USD","sign":"$","value":"1","is_default":"1"};
			  var admin = 1;
			</script>

		<!-- Dashboard Core -->
		<script src="{{ asset('assets/admin/js') }}/jquery-1.12.4.min.js"></script>
    	<script src="{{ asset('assets/admin/js') }}/vue.js"></script>
		<script src="{{ asset('assets/admin/js') }}/bootstrap.min.js"></script>
		<script src="{{ asset('assets/admin/js') }}/jqueryui.min.js"></script>
		<!-- Fullside-menu Js-->
		<script src="{{ asset('assets/admin/css') }}/jquery.slimscroll.min.js"></script>
		<script src="{{ asset('assets/admin/css') }}/waves.min.js"></script>
		<script src="{{ asset('assets/admin/js') }}/plugin.js"></script>
		<script src="{{ asset('assets/admin/js') }}/Chart.min.js"></script>
		<script src="{{ asset('assets/admin/js') }}/tag-it.js"></script>
		<script src="{{ asset('assets/admin/js') }}/nicEdit.js"></script>
        <script src="{{ asset('assets/admin/js') }}/bootstrap-colorpicker.min.js"></script>
        <script src="{{ asset('assets/admin/js') }}/notify.js"></script>
        <script src="{{ asset('assets/admin/js') }}/jquery.canvasjs.min.js"></script>
		<script src="{{ asset('assets/admin/js') }}/load.js"></script>
		<!-- Custom Js-->
		<script src="{{ asset('assets/admin/js') }}/custom.js"></script>
		<!-- AJAX Js-->
		<script src="{{ asset('assets/admin/js') }}/myscript.js"></script>



		
<script language="JavaScript">
    displayLineChart();

    function displayLineChart() {
        var data = {
            labels: [
            '25 Sep','24 Sep','23 Sep','22 Sep','21 Sep','20 Sep','19 Sep','18 Sep','17 Sep','16 Sep','15 Sep','14 Sep','13 Sep','12 Sep','11 Sep','10 Sep','09 Sep','08 Sep','07 Sep','06 Sep','05 Sep','04 Sep','03 Sep','02 Sep','01 Sep','31 Aug','30 Aug','29 Aug','28 Aug','27 Aug',
            ],
            datasets: [{
                label: "Prime and Fibonacci",
                fillColor: "#3dbcff",
                strokeColor: "#0099ff",
                pointColor: "rgba(220,220,220,1)",
                pointStrokeColor: "#fff",
                pointHighlightFill: "#fff",
                pointHighlightStroke: "rgba(220,220,220,1)",
                data: [
                '0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','1','0','0','0','0','0','0','0','0','0','0',
                ]
            }]
        };
        var ctx = document.getElementById("lineChart").getContext("2d");
        var options = {
            responsive: true
        };
        var lineChart = new Chart(ctx).Line(data, options);
    }


    
</script>

<script type="text/javascript">
    $('#poproducts').dataTable( {
      "ordering": false,
          'lengthChange': false,
          'searching'   : false,
          'ordering'    : false,
          'info'        : false,
          'autoWidth'   : false,
          'responsive'  : true,
          'paging'  : false
    } );
    </script>


<script type="text/javascript">
    $('#pproducts').dataTable( {
      "ordering": false,
      'lengthChange': false,
          'searching'   : false,
          'ordering'    : false,
          'info'        : false,
          'autoWidth'   : false,
          'responsive'  : true,
          'paging'  : false
    } );
    </script>

<script type="text/javascript">
        var chart1 = new CanvasJS.Chart("chartContainer-topReference",
            {
                exportEnabled: true,
                animationEnabled: true,

                legend: {
                    cursor: "pointer",
                    horizontalAlign: "right",
                    verticalAlign: "center",
                    fontSize: 16,
                    padding: {
                        top: 20,
                        bottom: 2,
                        right: 20,
                    },
                },
                data: [
                    {
                        type: "pie",
                        showInLegend: true,
                        legendText: "",
                        toolTipContent: "{name}: <strong>{#percent%} (#percent%)</strong>",
                        indexLabel: "#percent%",
                        indexLabelFontColor: "white",
                        indexLabelPlacement: "inside",
                        dataPoints: [
                                                                    {y:33198, name: "preview.codecanyon.net"},
                                                                    {y:9578, name: "codecanyon.net"},
                                                                    {y:4567, name: "geniusocean.com"},
                                                                    {y:280, name: "www.google.com"},
                                                                    {y:121, name: "m.facebook.com"},
                                                        ]
                    }
                ]
            });
        chart1.render();

        var chart = new CanvasJS.Chart("chartContainer-os",
            {
                exportEnabled: true,
                animationEnabled: true,
                legend: {
                    cursor: "pointer",
                    horizontalAlign: "right",
                    verticalAlign: "center",
                    fontSize: 16,
                    padding: {
                        top: 20,
                        bottom: 2,
                        right: 20,
                    },
                },
                data: [
                    {
                        type: "pie",
                        showInLegend: true,
                        legendText: "",
                        toolTipContent: "{name}: <strong>{#percent%} (#percent%)</strong>",
                        indexLabel: "#percent%",
                        indexLabelFontColor: "white",
                        indexLabelPlacement: "inside",
                        dataPoints: [
                                                            {y:34418, name: "Windows 10"},
                                                            {y:15075, name: "Unknown OS Platform"},
                                                            {y:10750, name: "Android"},
                                                            {y:6034, name: "Mac OS X"},
                                                            {y:5590, name: "Windows 7"},
                                                    ]
                    }
                ]
            });
        chart.render();    
</script>



	</body>

</html>
