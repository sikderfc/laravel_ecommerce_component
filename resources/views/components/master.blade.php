
<!DOCTYPE html>
<html lang="en">

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta property="og:title" content="ecommerce" />
	
	
	<title>Ecommerce</title>
	<!-- favicon -->
	<link rel="icon" type="image/x-icon" href="{{ asset('assets/website/images') }}/1571567283favicon.png" />
	<!-- bootstrap -->
	<link rel="stylesheet" href="{{ asset('assets/website/css/bootstrap.min.css') }}">
	<!-- Plugin css -->
	<link rel="stylesheet" href="{{ asset('assets/website/css/plugin.css') }}">
	<link rel="stylesheet" href="{{ asset('assets/website/css/animate.css') }}">
	<link rel="stylesheet" href="{{ asset('assets/website/css/toastr.css') }}">
	<link rel="stylesheet" href="{{ asset('assets/website/css/toastr.css') }}">

	<!-- jQuery Ui Css-->
	<link rel="stylesheet" href="{{ asset('assets/website/jquery-ui/jquery-ui.min.css') }}">
	<link rel="stylesheet" href="{{ asset('assets/website/jquery-ui/jquery-ui.structure.min.css') }}">


	<!-- stylesheet -->
	<link rel="stylesheet" href="{{ asset('assets/website/css/style.css') }}">
	<link rel="stylesheet" href="{{ asset('assets/website/css/custom.css') }}">
	<link rel="stylesheet" href="{{ asset('assets/website/css/common.css') }}">
	<!-- responsive -->
	<link rel="stylesheet" href="{{ asset('assets/website/css/responsive.css') }}">
	<link rel="stylesheet" href="{{ asset('assets/website/css/common-responsive.css') }}">

	<!--Updated CSS-->
	<link rel="stylesheet" id="color"
		href="{{ asset('assets/website/css/style.css') }}assets/front/css/styles3cfc.css?color=e91e63&amp;header_color=ffffff&amp;footer_color=143250&amp;copyright_color=02020c&amp;menu_color=ff5500&amp;menu_hover_color=02020c">





	<!-- Facebook Pixel Code -->
	<script>
		!function (f, b, e, v, n, t, s) {
			if (f.fbq) return; n = f.fbq = function () {
				n.callMethod ?
				n.callMethod.apply(n, arguments) : n.queue.push(arguments)
			};
			if (!f._fbq) f._fbq = n; n.push = n; n.loaded = !0; n.version = '2.0';
			n.queue = []; t = b.createElement(e); t.async = !0;
			t.src = v; s = b.getElementsByTagName(e)[0];
			s.parentNode.insertBefore(t, s)
		}(window, document, 'script',
			'../../../../connect.facebook.net/en_US/fbevents.js');
		fbq('init', '342039713086397');
		fbq('track', 'PageView');
	</script>
	<noscript><img height="1" width="1" style="display:none"
			src="https://www.facebook.com/tr?id=342039713086397&amp;ev=PageView&amp;noscript=1" /></noscript>
	<!-- End Facebook Pixel Code -->

	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-92080023-1"></script>
	<script>
		window.dataLayer = window.dataLayer || [];
		function gtag() { dataLayer.push(arguments); }
		gtag('js', new Date());

		gtag('config', 'UA-92080023-1');
	</script>



</head>

<body>
	
		<x-partials.header/>

		{{$slot}}


		

	<!-- Footer Area Start -->
	<footer class="footer" id="footer">
		<div class="container">
			<div class="row">
				<div class="col-md-6 col-lg-4">
					<div class="footer-info-area">
						<div class="footer-logo">
							<a href="https://royalscripts.com/product/kingcommerce/fashion" class="logo-link">
								<img src="{{ asset('assets/website/image') }}/1581932985footer-logo.png" alt="">
							</a>
						</div>
						<div class="text">
							<p>
								Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque
								laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi
								architecto beatae vitae
							</p>
						</div>
					</div>
					<div class="fotter-social-links">
						<ul>

							<li>
								<a href="https://www.facebook.com/" class="facebook" target="_blank">
									<i class="fab fa-facebook-f"></i>
								</a>
							</li>

							<li>
								<a href="https://plus.google.com/" class="google-plus" target="_blank">
									<i class="fab fa-google-plus-g"></i>
								</a>
							</li>

							<li>
								<a href="https://twitter.com/" class="twitter" target="_blank">
									<i class="fab fa-twitter"></i>
								</a>
							</li>

							<li>
								<a href="https://www.linkedin.com/" class="linkedin" target="_blank">
									<i class="fab fa-linkedin-in"></i>
								</a>
							</li>

							<li>
								<a href="https://dribbble.com/" class="dribbble" target="_blank">
									<i class="fab fa-dribbble"></i>
								</a>
							</li>

						</ul>
					</div>
				</div>
				<div class="col-md-6 col-lg-4">
					<div class="footer-widget info-link-widget">
						<h4 class="title">
							Footer Links
						</h4>
						<ul class="link-list">
							<li>
								<a href="https://royalscripts.com/product/kingcommerce/fashion">
									<i class="fas fa-angle-double-right"></i>Home
								</a>
							</li>

							<li>
								<a href="privacy.html">
									<i class="fas fa-angle-double-right"></i>Privacy &amp; Policy
								</a>
							</li>
							<li>
								<a href="terms.html">
									<i class="fas fa-angle-double-right"></i>Terms &amp; Condition
								</a>
							</li>

							<li>
								<a href="contact.html">
									<i class="fas fa-angle-double-right"></i>Contact Us
								</a>
							</li>
						</ul>
					</div>
				</div>
				<div class="col-md-6 col-lg-4">
					<div class="footer-widget recent-post-widget">
						<h4 class="title">
							Recent Post
						</h4>
						<ul class="post-list">
							<li>
								<div class="post">
									<div class="post-img">
										<img style="width: 73px; height: 59px;"
											src="{{ asset('assets/website/images/blogs') }}/15542700251-min.jpg" alt="">
									</div>
									<div class="post-details">
										<a href="blog/18.html">
											<h4 class="post-title">
												How to design effective arts?
											</h4>
										</a>
										<p class="date">
											Jan 02 - 2019
										</p>
									</div>
								</div>
							</li>
							<li>
								<div class="post">
									<div class="post-img">
										<img style="width: 73px; height: 59px;"
											src="{{ asset('assets/website/images/blogs') }}/15542698954-min.jpg" alt="">
									</div>
									<div class="post-details">
										<a href="blog/22.html">
											<h4 class="post-title">
												How to design effective arts?
											</h4>
										</a>
										<p class="date">
											Jan 02 - 2019
										</p>
									</div>
								</div>
							</li>
							<li>
								<div class="post">
									<div class="post-img">
										<img style="width: 73px; height: 59px;"
											src="{{ asset('assets/website/images/blogs') }}/15557542831-min.jpg" alt="">
									</div>
									<div class="post-details">
										<a href="blog/25.html">
											<h4 class="post-title">
												How to design effective arts?
											</h4>
										</a>
										<p class="date">
											Jan 02 - 2019
										</p>
									</div>
								</div>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>

		<div class="copy-bg">
			<div class="container">
				<div class="row">
					<div class="col-lg-12">
						<div class="content">
							<div class="content">
								<p>COPYRIGHT © 2019. All Rights Reserved By <a
										href="http://geniusocean.com/">GeniusOcean.com</a></p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</footer>
	<!-- Footer Area End -->

	<!-- Back to Top Start -->
	<div class="bottomtotop">
		<i class="fas fa-chevron-right"></i>
	</div>
	<!-- Back to Top End -->

	<!-- LOGIN MODAL -->
	<div class="modal fade" id="comment-log-reg" tabindex="-1" role="dialog" aria-labelledby="comment-log-reg-Title"
		aria-hidden="true">
		<div class="modal-dialog  modal-dialog-centered" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<nav class="comment-log-reg-tabmenu">
						<div class="nav nav-tabs" id="nav-tab" role="tablist">
							<a class="nav-item nav-link login active" id="nav-log-tab1" data-toggle="tab"
								href="#nav-log1" role="tab" aria-controls="nav-log" aria-selected="true">
								Login
							</a>
							<a class="nav-item nav-link" id="nav-reg-tab1" data-toggle="tab" href="#nav-reg1" role="tab"
								aria-controls="nav-reg" aria-selected="false">
								Register
							</a>
						</div>
					</nav>
					<div class="tab-content" id="nav-tabContent">
						<div class="tab-pane fade show active" id="nav-log1" role="tabpanel"
							aria-labelledby="nav-log-tab1">
							<div class="login-area">
								<div class="header-area">
									<h4 class="title">LOGIN NOW</h4>
								</div>
								<div class="login-form signin-form">
									<div class="alert alert-info validation" style="display: none;">
										<p class="text-left"></p>
									</div>
									<div class="alert alert-success validation" style="display: none;">
										<button type="button" class="close alert-close"><span>×</span></button>
										<p class="text-left"></p>
									</div>
									<div class="alert alert-danger validation" style="display: none;">
										<button type="button" class="close alert-close"><span>×</span></button>
										<p class="text-left"></p>
									</div>
									<form class="mloginform"
										action="https://royalscripts.com/product/kingcommerce/fashion/user/login"
										method="POST">
										<input type="hidden" name="_token"
											value="SV3EE1CSn4n55Vk4gExzKrF7I5XIfTB06gMVprKl">
										<div class="form-input">
											<input type="email" name="email" placeholder="Type Email Address"
												required="" value="user@gmail.com">
											<i class="icofont-user-alt-5"></i>
										</div>
										<div class="form-input">
											<input type="password" class="Password" name="password"
												placeholder="Type Password" required="" value="1234">
											<i class="icofont-ui-password"></i>
										</div>
										<div class="form-forgot-pass">
											<div class="left">
												<input type="checkbox" name="remember" id="mrp">
												<label for="mrp">	</label>
											</div>
											<div class="right">
												<a href="javascript:;" id="show-forgot">
													Forgot Password?
												</a>
											</div>
										</div>
										<input type="hidden" name="modal" value="1">
										<input class="mauthdata" type="hidden" value="Authenticating...">
										<button type="submit" class="submit-btn">Login</button>
										<div class="social-area">
											<h3 class="title">Or</h3>
											<p class="text">Sign In with social media</p>
											<ul class="social-links">
												<li>
													<a
														href="https://www.facebook.com/v3.0/dialog/oauth?client_id=964600954306943&amp;redirect_uri=https%3A%2F%2Froyalscripts.com%2Fproduct%2Fkingcommerce%2Ffashion%2Fauth%2Ffacebook%2Fcallback&amp;scope=email&amp;response_type=code&amp;state=kXze68iWiidN9Taj01z353r9f6Brn1HAD7Ec7I3h">
														<i class="fab fa-facebook-f"></i>
													</a>
												</li>
												<li>
													<a
														href="https://accounts.google.com/o/oauth2/auth?client_id=904681031719-sh1aolu42k7l93ik0bkiddcboghbpcfi.apps.googleusercontent.com&amp;redirect_uri=https%3A%2F%2Froyalscripts.com%2Fproduct%2Fkingcommerce%2Ffashion%2Fauth%2Fgoogle%2Fcallback&amp;scope=openid+profile+email&amp;response_type=code&amp;state=DiBuKO0s2fJUvwVDIGb3BDxIcMiFRYn1aAzDo6KV">
														<i class="fab fa-google-plus-g"></i>
													</a>
												</li>
											</ul>
										</div>
									</form>
								</div>
							</div>
						</div>
						<div class="tab-pane fade" id="nav-reg1" role="tabpanel" aria-labelledby="nav-reg-tab1">
							<div class="login-area signup-area">
								<div class="header-area">
									<h4 class="title">Signup Now</h4>
								</div>
								<div class="login-form signup-form">
									<div class="alert alert-info validation" style="display: none;">
										<p class="text-left"></p>
									</div>
									<div class="alert alert-success validation" style="display: none;">
										<button type="button" class="close alert-close"><span>×</span></button>
										<p class="text-left"></p>
									</div>
									<div class="alert alert-danger validation" style="display: none;">
										<button type="button" class="close alert-close"><span>×</span></button>
										<p class="text-left"></p>
									</div>
									<form class="mregisterform"
										action="https://royalscripts.com/product/kingcommerce/fashion/user/register"
										method="POST">
										<input type="hidden" name="_token"
											value="SV3EE1CSn4n55Vk4gExzKrF7I5XIfTB06gMVprKl">

										<div class="form-input">
											<input type="text" class="User Name" name="name" placeholder="Full Name"
												required="">
											<i class="icofont-user-alt-5"></i>
										</div>

										<div class="form-input">
											<input type="email" class="User Name" name="email"
												placeholder="Email Address" required="">
											<i class="icofont-email"></i>
										</div>

										<div class="form-input">
											<input type="text" class="User Name" name="phone" placeholder="Phone Number"
												required="">
											<i class="icofont-phone"></i>
										</div>

										<div class="form-input">
											<input type="text" class="User Name" name="address" placeholder="Address"
												required="">
											<i class="icofont-location-pin"></i>
										</div>

										<div class="form-input">
											<input type="password" class="Password" name="password"
												placeholder="Password" required="">
											<i class="icofont-ui-password"></i>
										</div>

										<div class="form-input">
											<input type="password" class="Password" name="password_confirmation"
												placeholder="Confirm Password" required="">
											<i class="icofont-ui-password"></i>
										</div>



										<ul class="captcha-area">
											<li>
												<p><img class="codeimg1" src="{{ asset('assets/website/images') }}/capcha_code.png" alt=""> <i
														class="fas fa-sync-alt pointer refresh_code "></i></p>
											</li>
										</ul>

										<div class="form-input">
											<input type="text" class="Password" name="codes" placeholder="Enter Code"
												required="">
											<i class="icofont-refresh"></i>
										</div>



										<input class="mprocessdata" type="hidden" value="Processing...">
										<button type="submit" class="submit-btn">Register</button>

									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- LOGIN MODAL ENDS -->

	<!-- FORGOT MODAL -->
	<div class="modal fade" id="forgot-modal" tabindex="-1" role="dialog" aria-labelledby="comment-log-reg-Title"
		aria-hidden="true">
		<div class="modal-dialog  modal-dialog-centered" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">

					<div class="login-area">
						<div class="header-area forgot-passwor-area">
							<h4 class="title">Forgot Password </h4>
							<p class="text">Please Write your Email </p>
						</div>
						<div class="login-form">
							<div class="alert alert-info validation" style="display: none;">
								<p class="text-left"></p>
							</div>
							<div class="alert alert-success validation" style="display: none;">
								<button type="button" class="close alert-close"><span>×</span></button>
								<p class="text-left"></p>
							</div>
							<div class="alert alert-danger validation" style="display: none;">
								<button type="button" class="close alert-close"><span>×</span></button>
								<p class="text-left"></p>
							</div>
							<form id="mforgotform"
								action="https://royalscripts.com/product/kingcommerce/fashion/user/forgot"
								method="POST">
								<input type="hidden" name="_token" value="SV3EE1CSn4n55Vk4gExzKrF7I5XIfTB06gMVprKl">
								<div class="form-input">
									<input type="email" name="email" class="User Name" placeholder="Email Address"
										required="">
									<i class="icofont-user-alt-5"></i>
								</div>
								<div class="to-login-page">
									<a href="javascript:;" id="show-login">
										Login Now
									</a>
								</div>
								<input class="fauthdata" type="hidden" value="Checking...">
								<button type="submit" class="submit-btn">SUBMIT</button>
							</form>
						</div>
					</div>

				</div>
			</div>
		</div>
	</div>
	<!-- FORGOT MODAL ENDS -->


	<!-- VENDOR LOGIN MODAL -->
	<div class="modal fade" id="vendor-login" tabindex="-1" role="dialog" aria-labelledby="vendor-login-Title"
		aria-hidden="true">
		<div class="modal-dialog  modal-dialog-centered" style="transition: .5s;" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<nav class="comment-log-reg-tabmenu">
						<div class="nav nav-tabs" id="nav-tab1" role="tablist">
							<a class="nav-item nav-link login active" id="nav-log-tab11" data-toggle="tab"
								href="#nav-log11" role="tab" aria-controls="nav-log" aria-selected="true">
								Vendor Login
							</a>
							<a class="nav-item nav-link" id="nav-reg-tab11" data-toggle="tab" href="#nav-reg11"
								role="tab" aria-controls="nav-reg" aria-selected="false">
								Vendor Registration
							</a>
						</div>
					</nav>
					<div class="tab-content" id="nav-tabContent">
						<div class="tab-pane fade show active" id="nav-log11" role="tabpanel"
							aria-labelledby="nav-log-tab">
							<div class="login-area">
								<div class="login-form signin-form">
									<div class="alert alert-info validation" style="display: none;">
										<p class="text-left"></p>
									</div>
									<div class="alert alert-success validation" style="display: none;">
										<button type="button" class="close alert-close"><span>×</span></button>
										<p class="text-left"></p>
									</div>
									<div class="alert alert-danger validation" style="display: none;">
										<button type="button" class="close alert-close"><span>×</span></button>
										<p class="text-left"></p>
									</div>
									<form class="mloginform"
										action="https://royalscripts.com/product/kingcommerce/fashion/user/login"
										method="POST">
										<input type="hidden" name="_token"
											value="SV3EE1CSn4n55Vk4gExzKrF7I5XIfTB06gMVprKl">
										<div class="form-input">
											<input type="email" name="email" placeholder="Type Email Address"
												required="" value="vendor@gmail.com">
											<i class="icofont-user-alt-5"></i>
										</div>
										<div class="form-input">
											<input type="password" class="Password" name="password"
												placeholder="Type Password" required="" value="1234">
											<i class="icofont-ui-password"></i>
										</div>
										<div class="form-forgot-pass">
											<div class="left">
												<input type="checkbox" name="remember" id="mrp1">
												<label for="mrp1">Remember Password</label>
											</div>
											<div class="right">
												<a href="javascript:;" id="show-forgot1">
													Forgot Password?
												</a>
											</div>
										</div>
										<input type="hidden" name="modal" value="1">
										<input type="hidden" name="vendor" value="1">
										<input class="mauthdata" type="hidden" value="Authenticating...">
										<button type="submit" class="submit-btn">Login</button>
										<div class="social-area">
											<h3 class="title">Or</h3>
											<p class="text">Sign In with social media</p>
											<ul class="social-links">
												<li>
													<a
														href="https://www.facebook.com/v3.0/dialog/oauth?client_id=964600954306943&amp;redirect_uri=https%3A%2F%2Froyalscripts.com%2Fproduct%2Fkingcommerce%2Ffashion%2Fauth%2Ffacebook%2Fcallback&amp;scope=email&amp;response_type=code&amp;state=SyE011FchwMY3qksb9BVD07jquk0w3YB0ubAOvkI">
														<i class="fab fa-facebook-f"></i>
													</a>
												</li>
												<li>
													<a
														href="https://accounts.google.com/o/oauth2/auth?client_id=904681031719-sh1aolu42k7l93ik0bkiddcboghbpcfi.apps.googleusercontent.com&amp;redirect_uri=https%3A%2F%2Froyalscripts.com%2Fproduct%2Fkingcommerce%2Ffashion%2Fauth%2Fgoogle%2Fcallback&amp;scope=openid+profile+email&amp;response_type=code&amp;state=j1iLIH0iSdsHFNlx8JLQrByvMPO8C8Ubb6ms7xdw">
														<i class="fab fa-google-plus-g"></i>
													</a>
												</li>
											</ul>
										</div>
									</form>
								</div>
							</div>
						</div>
						<div class="tab-pane fade" id="nav-reg11" role="tabpanel" aria-labelledby="nav-reg-tab">
							<div class="login-area signup-area">
								<div class="login-form signup-form">
									<div class="alert alert-info validation" style="display: none;">
										<p class="text-left"></p>
									</div>
									<div class="alert alert-success validation" style="display: none;">
										<button type="button" class="close alert-close"><span>×</span></button>
										<p class="text-left"></p>
									</div>
									<div class="alert alert-danger validation" style="display: none;">
										<button type="button" class="close alert-close"><span>×</span></button>
										<p class="text-left"></p>
									</div>
									<form class="mregisterform"
										action="https://royalscripts.com/product/kingcommerce/fashion/user/register"
										method="POST">
										<input type="hidden" name="_token"
											value="SV3EE1CSn4n55Vk4gExzKrF7I5XIfTB06gMVprKl">

										<div class="row">

											<div class="col-lg-6">
												<div class="form-input">
													<input type="text" class="User Name" name="name"
														placeholder="Full Name" required="">
													<i class="icofont-user-alt-5"></i>
												</div>
											</div>

											<div class="col-lg-6">
												<div class="form-input">
													<input type="email" class="User Name" name="email"
														placeholder="Email Address" required="">
													<i class="icofont-email"></i>
												</div>

											</div>
											<div class="col-lg-6">
												<div class="form-input">
													<input type="text" class="User Name" name="phone"
														placeholder="Phone Number" required="">
													<i class="icofont-phone"></i>
												</div>

											</div>
											<div class="col-lg-6">

												<div class="form-input">
													<input type="text" class="User Name" name="address"
														placeholder="Address" required="">
													<i class="icofont-location-pin"></i>
												</div>
											</div>

											<div class="col-lg-6">
												<div class="form-input">
													<input type="text" class="User Name" name="shop_name"
														placeholder="Shop Name" required="">
													<i class="icofont-cart-alt"></i>
												</div>

											</div>
											<div class="col-lg-6">

												<div class="form-input">
													<input type="text" class="User Name" name="owner_name"
														placeholder="Owner Name" required="">
													<i class="icofont-cart"></i>
												</div>
											</div>
											<div class="col-lg-6">

												<div class="form-input">
													<input type="text" class="User Name" name="shop_number"
														placeholder="Shop Number" required="">
													<i class="icofont-shopping-cart"></i>
												</div>
											</div>
											<div class="col-lg-6">

												<div class="form-input">
													<input type="text" class="User Name" name="shop_address"
														placeholder="Shop Address" required="">
													<i class="icofont-opencart"></i>
												</div>
											</div>
											<div class="col-lg-6">

												<div class="form-input">
													<input type="text" class="User Name" name="reg_number"
														placeholder="Registration Number" required="">
													<i class="icofont-ui-cart"></i>
												</div>
											</div>
											<div class="col-lg-6">

												<div class="form-input">
													<input type="text" class="User Name" name="shop_message"
														placeholder="Message" required="">
													<i class="icofont-envelope"></i>
												</div>
											</div>

											<div class="col-lg-6">
												<div class="form-input">
													<input type="password" class="Password" name="password"
														placeholder="Password" required="">
													<i class="icofont-ui-password"></i>
												</div>

											</div>
											<div class="col-lg-6">
												<div class="form-input">
													<input type="password" class="Password" name="password_confirmation"
														placeholder="Confirm Password" required="">
													<i class="icofont-ui-password"></i>
												</div>
											</div>


											<div class="col-lg-6">


												<ul class="captcha-area">
													<li>
														<p>
															<img class="codeimg1" src="{{ asset('assets/website/images') }}/capcha_code.png"
																alt=""> <i
																class="fas fa-sync-alt pointer refresh_code "></i>
														</p>

													</li>
												</ul>


											</div>

											<div class="col-lg-6">

												<div class="form-input">
													<input type="text" class="Password" name="codes"
														placeholder="Enter Code" required="">
													<i class="icofont-refresh"></i>

												</div>



											</div>


											<input type="hidden" name="vendor" value="1">
											<input class="mprocessdata" type="hidden" value="Processing...">
											<button type="submit" class="submit-btn">Register</button>

										</div>




									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- VENDOR LOGIN MODAL ENDS -->

	<!-- Product Quick View Modal -->

	<div class="modal fade" id="quickview" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-dialog quickview-modal modal-dialog-centered modal-lg" role="document">
			<div class="modal-content">
				<div class="submit-loader">
					<img src="{{ asset('assets/website/images') }}/1564224328loading3.gif" alt="">
				</div>
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<div class="container quick-view-modal">

					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- Product Quick View Modal -->

	<!-- Order Tracking modal Start-->
	<div class="modal fade" id="track-order-modal" tabindex="-1" role="dialog" aria-labelledby="order-tracking-modal"
		aria-hidden="true">
		<div class="modal-dialog  modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h6 class="modal-title"> <b>Order Tracking</b> </h6>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">

					<div class="order-tracking-content">
						<form id="track-form" class="track-form">
							<input type="hidden" name="_token" value="SV3EE1CSn4n55Vk4gExzKrF7I5XIfTB06gMVprKl">
							<input type="text" id="track-code" placeholder="Get Tracking Code" required="">
							<button type="submit" class="mybtn1">View Tracking</button>
							<a href="#" data-toggle="modal" data-target="#order-tracking-modal"></a>
						</form>
					</div>

					<div>
						<div class="submit-loader d-none">
							<img src="{{ asset('assets/website/images') }}/1564224328loading3.gif" alt="">
						</div>
						<div id="track-order">

						</div>
					</div>

				</div>
			</div>
		</div>
	</div>
	

>


	

	<!-- jquery -->
	<script src="{{ asset('assets/website/js/jquery.js') }}"></script>

	<script src="{{ asset('assets/website/jquery-ui/jquery-ui.min.js') }}"></script>
	<!-- popper -->
	<script src="{{ asset('assets/website/js/popper.min.js') }}"></script>
	<!-- bootstrap -->
	<script src="{{ asset('assets/website/js/bootstrap.min.js') }}"></script>
	<!-- plugin js-->
	<script src="{{ asset('assets/website/js/plugin.js') }}"></script>

	<script src="{{ asset('assets/website/js/xzoom.min.js') }}"></script>

	<script src="{{ asset('assets/website/js/jquery.hammer.min.js') }}"></script>

	<script src="{{ asset('assets/website/js/setup.js') }}"></script>

	<script src="{{ asset('assets/website/js/toastr.js') }}"></script>
	<!-- main -->
	<script src="{{ asset('assets/website/js/main.js') }}"></script>
	<!-- custom -->
	<script src="{{ asset('assets/website/js/custom.js') }}"></script>

	<script>//Google Analytics Scriptfffffffffffffffffffffffssssfffffs</script>


	<script>
		$(window).on('load', function () {

			setTimeout(function () {

				$('#extraData').load('extras.html');

			}, 500);
		});

	</script>

</body>




</html>

	