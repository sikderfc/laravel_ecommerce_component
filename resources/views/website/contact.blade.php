
<x-master>

<div class="breadcrumb-area">
	<div class="container">
	  <div class="row">
		<div class="col-lg-12">
		  <ul class="pages">
			<li>
			  <a href="https://royalscripts.com/product/kingcommerce/fashion">
				Home
			  </a>
			</li>
			<li>
			  <a href="about.html">
				Contact Us
			  </a>
			</li>
		  </ul>
		</div>
	  </div>
	</div>
  </div>
  <section class="contact-us">
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<div class="contact-section-title">
						<h4 class="subtitle" style="margin-bottom: 6px; font-weight: 600; line-height: 28px; font-size: 28px; text-transform: uppercase;">WE'D LOVE TO</h4><h2 class="title" style="margin-bottom: 13px;font-weight: 600;line-height: 50px;font-size: 40px;color: #0f78f2;text-transform: uppercase;">HEAR FROM YOU</h2>
						<span style="color: rgb(119, 119, 119);">Send us a message and we' ll respond as soon as possible</span><br>
				</div>
			</div>
		</div>
		<div class="row justify-content-between">
			<div class="col-xl-7 col-lg-7 col-md-6">
				<div class="left-area">
					<div class="contact-form">
						<div class="gocover" style="background: url(assets/images/1564224328loading3.gif) no-repeat scroll center center rgba(45, 45, 45, 0.5);"></div>
						<form id="contactform" action="https://royalscripts.com/product/kingcommerce/fashion/contact" method="POST">
							<input type="hidden" name="_token" value="SV3EE1CSn4n55Vk4gExzKrF7I5XIfTB06gMVprKl">
								<div class="alert alert-success validation" style="display: none;">
  <button type="button" class="close alert-close"><span>×</span></button>
		<p class="text-left"></p> 
  </div>
  <div class="alert alert-danger validation" style="display: none;">
  <button type="button" class="close alert-close"><span>×</span></button>
		<ul class="text-left">
		</ul>
  </div>  

								<div class="form-input">
									<input type="text" name="name" placeholder="Name *" required="">
									<i class="icofont-user-alt-5"></i>
								</div>
								<div class="form-input">
										<input type="text" name="phone" placeholder="Phone Number *">
																					<i class="icofont-ui-call"></i>
								</div>
								<div class="form-input">
										<input type="email" name="email" placeholder="Email Address *" required="">
																					<i class="icofont-envelope"></i>
								</div>
								<div class="form-input">
										<textarea name="text" placeholder="Your Message *" required=""></textarea>
								</div>

								
								<ul class="captcha-area">
									<li>
										<p><img class="codeimg1" src="assets/images/capcha_code.png" alt=""> <i class="fas fa-sync-alt pointer refresh_code"></i></p>
									
									</li>
									<li>
											<input name="codes" type="text" class="input-field" placeholder="Enter Code" required="">
											
										</li>
								</ul>

								

								<input type="hidden" name="to" value="admin@geniusocean.com">
								<button class="submit-btn" type="submit">Send Message</button>
						</form>
					</div>
				</div>
			</div>
			<div class="col-xl-4 col-lg-5 col-md-6">
				<div class="right-area">

											<div class="contact-info ">
						<div class="left ">
								<div class="icon">
										<i class="icofont-email"></i>
								</div>
						</div>
						<div class="content d-flex align-self-center">
							<div class="content">
									 
									<a href="https://geniusocean.com/" target="_blank">https://geniusocean.com/</a>
									<a href="https://royalscripts.com/cdn-cgi/l/email-protection#d2b3b6bfbbbc92b5b7bcbba7a1bdb1b7b3bcfcb1bdbf"><span class="__cf_email__" data-cfemail="badbded7d3d4fadddfd4d3cfc9d5">[email&nbsp;protected]</span>cean.com</a>
																	</div>
						</div>
					</div>
											 
					<div class="contact-info">
							<div class="left">
									<div class="icon">
											<i class="icofont-google-map"></i>
									</div>
							</div>
							<div class="content d-flex align-self-center">
								<div class="content">
										<p>
											 
												3584 Hickory Heights Drive ,Hanover MD 21076, USA
																						</p>
								</div>
							</div>
						</div>
											 
						<div class="contact-info">
								<div class="left">
										<div class="icon">
												<i class="icofont-smart-phone"></i>
										</div>
								</div>
								<div class="content d-flex align-self-center">
									<div class="content">
																					<a href="tel:00 000 000 000">00 000 000 000</a>
										<a href="tel:00 000 000 000">00 000 000 000</a>
																				</div>
								</div>
							</div>
					

							<div class="social-links">
								<h4 class="title">Find Us Here :</h4>
								<ul>

																	   <li>
									<a href="https://www.facebook.com/" class="facebook" target="_blank">
										<i class="fab fa-facebook-f"></i>
									</a>
								  </li>
								  
																		<li>
									<a href="https://plus.google.com/" class="google-plus" target="_blank">
										<i class="fab fa-google-plus-g"></i>
									</a>
								  </li>
								  
																		<li>
									<a href="https://twitter.com/" class="twitter" target="_blank">
										<i class="fab fa-twitter"></i>
									</a>
								  </li>
								  
																		<li>
									<a href="https://www.linkedin.com/" class="linkedin" target="_blank">
										<i class="fab fa-linkedin-in"></i>
									</a>
								  </li>
								  
																		<li>
									<a href="https://dribbble.com/" class="dribbble" target="_blank">
										<i class="fab fa-dribbble"></i>
									</a>
								  </li>
								  
									</ul>
							</div>
				</div>
			</div>
		</div>
	</div>
</section>

</x-master>