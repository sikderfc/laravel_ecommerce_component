
<x-master>
    <section class="login-signup">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-lg-6">
					<nav class="comment-log-reg-tabmenu core-nav"><div class="full-container">
						<div class="nav nav-tabs" id="nav-tab" role="tablist">
							<a class="nav-item nav-link login active" id="nav-log-tab" data-toggle="tab" href="#nav-log" role="tab" aria-controls="nav-log" aria-selected="true">
								Login
							</a>
							<a class="nav-item nav-link" id="nav-reg-tab" data-toggle="tab" href="#nav-reg" role="tab" aria-controls="nav-reg" aria-selected="false">
								Register
							</a>
						</div>
					</div></nav><div class="dropdown-overlay"></div>
					<div class="tab-content" id="nav-tabContent">
						<div class="tab-pane fade show active" id="nav-log" role="tabpanel" aria-labelledby="nav-log-tab">
							<div class="login-area">
								<div class="header-area">
									<a href=""><h4 class="title">LOGIN NOW</h4></a>
								</div>
								<div class="login-form signin-form" action= "{{url('dashboard') }}">
									<div class="alert alert-info validation" style="display: none;">
										<p class="text-left"></p>
									</div>
									<div class="alert alert-success validation" style="display: none;">
										<button type="button" class="close alert-close"><span>×</span></button>
										<p class="text-left"></p>
									</div>
									<div class="alert alert-danger validation" style="display: none;">
										<button type="button" class="close alert-close"><span>×</span></button>
										<p class="text-left"></p>
									</div>
									<form class="" action="{{url('dashboard') }}">
										<input type="hidden" name="_token" value="">
										<div class="form-input">
											<input type="email" name="email" placeholder="Type Email Address" required="" value="user@gmail.com">
											<i class="icofont-user-alt-5"></i>
										</div>
										<div class="form-input">
											<input type="password" class="Password" name="password" placeholder="Type Password" required="" value="1234">
											<i class="icofont-ui-password"></i>
										</div>
										<div class="form-forgot-pass">
											<div class="left">
												<input type="checkbox" name="remember" id="mrp">
												<label for="mrp">Remember Password</label>
											</div>
											<div class="right">
												<a href="forgot.html">
													Forgot Password?
												</a>
											</div>
										</div>
									
										<a href="facebook.com"><button type="btn" class="submit-btn">Losssgin</button></a>
										<div class="social-area">
											<h3 class="title">Or</h3>
											<p class="text">Sign In with social media</p>
											<ul class="social-links">
												<li>
													<a href="https://www.facebook.com/v3.0/dialog/oauth?client_id=964600954306943&amp;redirect_uri=https%3A%2F%2Froyalscripts.com%2Fproduct%2Fkingcommerce%2Ffashion%2Fauth%2Ffacebook%2Fcallback&amp;scope=email&amp;response_type=code&amp;state=fxtGwFXPOvc5RZdknXE9W3mUuR336XRlMKRvpT4G">
														<i class="fab fa-facebook-f"></i>
													</a>
												</li>
												<li>
													<a href="https://accounts.google.com/o/oauth2/auth?client_id=904681031719-sh1aolu42k7l93ik0bkiddcboghbpcfi.apps.googleusercontent.com&amp;redirect_uri=https%3A%2F%2Froyalscripts.com%2Fproduct%2Fkingcommerce%2Ffashion%2Fauth%2Fgoogle%2Fcallback&amp;scope=openid+profile+email&amp;response_type=code&amp;state=vc8ajsDLHyGmVplM77uigV6t4mKeOss7NZLvrFmL">
														<i class="fab fa-google-plus-g"></i>
													</a>
												</li>
											</ul>
										</div>
									</form>
								</div>
							</div>
						</div>
						<div class="tab-pane fade" id="nav-reg" role="tabpanel" aria-labelledby="nav-reg-tab">
							<div class="login-area signup-area">
								<div class="header-area">
									<h4 class="title">Signup Now</h4>
								</div>
								<div class="login-form signup-form">
									<div class="alert alert-info validation" style="display: none;">
										<p class="text-left"></p>
									</div>
									<div class="alert alert-success validation" style="display: none;">
										<button type="button" class="close alert-close"><span>×</span></button>
										<p class="text-left"></p>
									</div>
									<div class="alert alert-danger validation" style="display: none;">
										<button type="button" class="close alert-close"><span>×</span></button>
										<p class="text-left"></p>
									</div>
									<form class="mregisterform" action="https://royalscripts.com/product/kingcommerce/fashion/user/register" method="POST">
										<input type="hidden" name="_token" value="SV3EE1CSn4n55Vk4gExzKrF7I5XIfTB06gMVprKl">

										<div class="form-input">
											<input type="text" class="User Name" name="name" placeholder="Full Name" required="">
											<i class="icofont-user-alt-5"></i>
										</div>

										<div class="form-input">
											<input type="email" class="User Name" name="email" placeholder="Email Address" required="">
											<i class="icofont-email"></i>
										</div>

										<div class="form-input">
											<input type="text" class="User Name" name="phone" placeholder="Phone Number" required="">
											<i class="icofont-phone"></i>
										</div>

										<div class="form-input">
											<input type="text" class="User Name" name="address" placeholder="Address" required="">
											<i class="icofont-location-pin"></i>
										</div>

										<div class="form-input">
											<input type="password" class="Password" name="password" placeholder="Password" required="">
											<i class="icofont-ui-password"></i>
										</div>

										<div class="form-input">
											<input type="password" class="Password" name="password_confirmation" placeholder="Confirm Password" required="">
											<i class="icofont-ui-password"></i>
										</div>


										<ul class="captcha-area">
											<li>
												<p><img class="codeimg1" src="../assets/images/capcha_code.png" alt="">
													<i class="fas fa-sync-alt pointer refresh_code "></i></p>
											</li>
										</ul>

										<div class="form-input">
											<input type="text" class="Password" name="codes" placeholder="Enter Code" required="">
											<i class="icofont-refresh"></i>
										</div>


										<input class="mprocessdata" type="hidden" value="Processing...">
										<button type="submit" class="submit-btn">Register</button>

									</form>
								</div>
							</div>
						</div>
					</div>

				</div>

			</div>
		</div>
	</section>

</x-master>