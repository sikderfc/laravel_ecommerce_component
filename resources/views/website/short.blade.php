<x-master>
	<div class="breadcrumb-area">
	  <div class="container">
		<div class="row">
		  <div class="col-lg-12">
			<ul class="pages">
			  <li>
				<a href="https://royalscripts.com/product/kingcommerce/fashion"> Home </a>
			  </li>
			  <li>
				<a href="about.html"> Short </a>
			  </li>
			</ul>
		  </div>
		</div>
	  </div>
	</div>
	<section class="sub-categori">
	  <div class="container">
		<div class="row">
		  <div class="col-lg-3 col-md-6">
			<div class="left-area">
			  <div class="filter-result-area">
				<div class="header-area">
				  <h4 class="title"> Filter Results By </h4>
				</div>
				<div class="body-area">
				  <form id="catalogForm" action="https://royalscripts.com/product/kingcommerce/fashion/category/Shirts" method="GET">
					<ul class="filter-list">
					  <li>
						<div class="content">
						  <a href="Accessories.html" class="category-link">
							<i class="fas fa-angle-double-right"></i> Accessories </a>
						</div>
					  </li>
					  <li>
						<div class="content">
						  <a href="Beauty.html" class="category-link">
							<i class="fas fa-angle-double-right"></i> Beauty </a>
						</div>
					  </li>
					  <li>
						<div class="content">
						  <a href="Bags.html" class="category-link">
							<i class="fas fa-angle-double-right"></i> Bags </a>
						</div>
					  </li>
					  <li>
						<div class="content">
						  <a href="Shoes.html" class="category-link">
							<i class="fas fa-angle-double-right"></i> Shoes </a>
						</div>
					  </li>
					  <li>
						<div class="content">
						  <a href="Jewellery.html" class="category-link">
							<i class="fas fa-angle-double-right"></i> Jewellery </a>
						</div>
					  </li>
					  <li>
						<div class="content">
						  <a href="Jeans.html" class="category-link">
							<i class="fas fa-angle-double-right"></i> Jeans </a>
						</div>
					  </li>
					  <li>
						<div class="content">
						  <a href="Shirts.html" class="category-link">
							<i class="fas fa-angle-double-right"></i> Shirts </a>
						</div>
					  </li>
					  <li>
						<div class="content">
						  <a href="Coats-and-Jackets.html" class="category-link">
							<i class="fas fa-angle-double-right"></i> Coats &amp; Jackets </a>
						</div>
					  </li>
					  <li>
						<div class="content">
						  <a href="T-shirt-and-Tops.html" class="category-link">
							<i class="fas fa-angle-double-right"></i> T-shirt &amp; Tops </a>
						</div>
					  </li>
					  <li>
						<div class="content">
						  <a href="Shorts.html" class="category-link">
							<i class="fas fa-angle-double-right"></i> Shorts </a>
						</div>
					  </li>
					</ul>
					<div class="price-range-block">
					  <div id="slider-range" class="price-filter-range ui-slider ui-corner-all ui-slider-horizontal ui-widget ui-widget-content" name="rangeInput">
						<div class="ui-slider-range ui-corner-all ui-widget-header" style="left: 0%; width: 100%;"></div>
						<span tabindex="0" class="ui-slider-handle ui-corner-all ui-state-default" style="left: 0%;"></span>
						<span tabindex="0" class="ui-slider-handle ui-corner-all ui-state-default" style="left: 100%;"></span>
					  </div>
					  <div class="livecount">
						<input type="number" min="0" name="min" id="min_price" class="price-range-field">
						<span>To</span>
						<input type="number" min="0" name="max" id="max_price" class="price-range-field">
					  </div>
					</div>
					<button class="filter-btn" type="submit">Search</button>
				  </form>
				</div>
			  </div>
			</div>
		  </div>
		  <div class="col-lg-9 order-first order-lg-last ajax-loader-parent">
			<div class="right-area" id="app">
			  <div class="item-filter">
				<ul class="filter-list">
				  <li class="item-short-area">
					<p>Sort By :</p>
					<select id="sortby" name="sort" class="short-item">
					  <option value="date_desc">Latest Product</option>
					  <option value="date_asc">Oldest Product</option>
					  <option value="price_asc">Lowest Price</option>
					  <option value="price_desc">Highest Price</option>
					</select>
				  </li>
				</ul>
			  </div>
			  <div class="categori-item-area">
				<div class="row" id="ajaxContent">
				  <div class="col-lg-4 col-md-4 col-6 remove-padding">
					<a href="../item/kingcommerce-fashion-products-title-will-be-here-one-01-nb90982laq.html" class="item">
					  <div class="item-img">
						<img class="img-fluid" src="../assets/images/thumbnails/1615885717pseQ775U.jpg" alt="">
					  </div>
					  <div class="info">
						<h5 class="name">KingCommerce Fashion Products Title Will be here one 01</h5>
						<h4 class="price">65$ <del>
							<small>70$</small>
						  </del>
						</h4>
						<div class="stars">
						  <div class="ratings">
							<div class="empty-stars"></div>
							<div class="full-stars" style="width:0%"></div>
						  </div>
						</div>
						<div class="item-cart-area">
						  <ul class="item-cart-options">
							<li>
							  <span href="javascript:;" rel-toggle="tooltip" title="" data-toggle="modal" id="wish-btn" data-target="#comment-log-reg" data-placement="top" data-original-title="Add To Wishlist">
								<i class="icofont-heart-alt"></i>
							  </span>
							</li>
							<li>
							  <span class="quick-view" rel-toggle="tooltip" title="" href="javascript:;" data-href="https://royalscripts.com/product/kingcommerce/fashion/item/quick/view/184" data-toggle="modal" data-target="#quickview" data-placement="top" data-original-title="Quick View">
								<i class="fas fa-shopping-basket"></i>
							  </span>
							</li>
							<li>
							  <span href="javascript:;" class="add-to-compare" data-href="https://royalscripts.com/product/kingcommerce/fashion/item/compare/add/184" data-toggle="tooltip" data-placement="top" title="" data-original-title="Compare">
								<i class="icofont-exchange"></i>
							  </span>
							</li>
						  </ul>
						</div>
					  </div>
					</a>
				  </div>
				  <div class="col-lg-12">
					<div class="page-center mt-5"></div>
				  </div>
				</div>
				<div id="ajaxLoader" class="ajax-loader" style="background: url({{ asset('assets/website/images/sliders') }}/1564224328loading3.gif)) no-repeat scroll center center rgba(0,0,0,.6);"></div>
			  </div>
			</div>
		  </div>
		</div>
	  </div>
	</section>
  </x-master>