
<x-master>

<div class="breadcrumb-area">
	<div class="container">
	  <div class="row">
		<div class="col-lg-12">
		  <ul class="pages">
			<li>
			  <a href="https://royalscripts.com/product/kingcommerce/fashion">
				Product Detail Page
			  </a>
			</li>
			<li>
			  <a href="about.html">
				About Us
			  </a>
			</li>
		  </ul>
		</div>
	  </div>
	</div>
  </div>
  <section class="product-details-page">
  <div class="container">
    <div class="row">
    <div class="col-lg-9">
        <div class="row">

            <div class="col-lg-5 col-md-12">

          <div class="xzoom-container">
              <img class="xzoom5" id="xzoom-magnific" src="https://royalscripts.com/product/kingcommerce/fashion/assets/images/products/1615885461QYTweVn0.png" xoriginal="https://royalscripts.com/product/kingcommerce/fashion/assets/images/products/1615885461QYTweVn0.png" style="width: 102%;">
              <div class="xzoom-thumbs">

                <div class="all-slider owl-carousel owl-theme owl-loaded">

                    

                

                    

                

                    

                
                <div class="owl-stage-outer"><div class="owl-stage" style="transform: translate3d(0px, 0px, 0px); transition: all 0s ease 0s; width: 244.5px;"><div class="owl-item active" style="width: 81.5px; margin-right: 0px;"><a href="https://royalscripts.com/product/kingcommerce/fashion/assets/images/products/1615885461QYTweVn0.png">
                  <img class="xzoom-gallery5 xactive" width="80" src="https://royalscripts.com/product/kingcommerce/fashion/assets/images/products/1615885461QYTweVn0.png" title="The description goes here">
                    </a></div><div class="owl-item active" style="width: 81.5px; margin-right: 0px;"><a href="https://royalscripts.com/product/kingcommerce/fashion/assets/images/galleries/1615952770cO70OuxO.jpg">
                  <img class="xzoom-gallery5" width="80" src="https://royalscripts.com/product/kingcommerce/fashion/assets/images/galleries/1615952770cO70OuxO.jpg" title="The description goes here">
                    </a></div><div class="owl-item active" style="width: 81.5px; margin-right: 0px;"><a href="https://royalscripts.com/product/kingcommerce/fashion/assets/images/galleries/1615952770b23dlkkB.jpg">
                  <img class="xzoom-gallery5" width="80" src="https://royalscripts.com/product/kingcommerce/fashion/assets/images/galleries/1615952770b23dlkkB.jpg" title="The description goes here">
                    </a></div></div></div><div class="owl-controls"><div class="owl-nav"><div class="owl-prev" style=""><i class="fa fa-angle-left"></i></div><div class="owl-next" style=""><i class="fa fa-angle-right"></i></div></div><div class="owl-dots" style="display: none;"></div></div></div>

              </div>
          </div>

            </div>

            <div class="col-lg-7">
              <div class="right-area">
                <div class="product-info">
                  <h4 class="product-name">KingCommerce Fashion Products Title Will be here one 01</h4>
                  <div class="info-meta-1">
                    <ul>

                                                                  <li class="product-isstook">
                        <p>
                          <i class="icofont-check-circled"></i>
                           In Stock
                        </p>
                      </li>
                                                                  <li>
                        <div class="ratings">
                          <div class="empty-stars"></div>
                          <div class="full-stars" style="width:0%"></div>
                        </div>
                      </li>
                      <li class="review-count">
                        <p>0 Review(s)</p>
                      </li>
                                      </ul>
                  </div>



            <div class="product-price">
              <p class="title">Price :</p>
                    <p class="price"><span id="sizeprice">25$</span>
                      <small><del>28$</del></small></p>
                                        </div>

                  <div class="info-meta-2">
                    <ul>

                      
                    </ul>
                  </div>


                                    <div class="product-size">
                    <p class="title">Size :</p>
                    <ul class="siz-list">
                                                                  <li class="active">
                        <span class="box">XL
                          <input type="hidden" class="size" value="XL">
                          <input type="hidden" class="size_qty" value="0">
                          <input type="hidden" class="size_key" value="0">
                          <input type="hidden" class="size_price" value="0">
                        </span>
                      </li>
                                                                  <li class="">
                        <span class="box">XXL
                          <input type="hidden" class="size" value="XXL">
                          <input type="hidden" class="size_qty" value="4">
                          <input type="hidden" class="size_key" value="1">
                          <input type="hidden" class="size_price" value="0">
                        </span>
                      </li>
                                                                  <li>
                    </li></ul>
                  </div>
                  
                                    <div class="product-color">
                    <p class="title">Color :</p>
                    <ul class="color-list">
                                                                  <li class="active">
                        <span class="box" data-color="#000000" style="background-color: #000000"></span>
                      </li>
                                                                  <li class="">
                        <span class="box" data-color="#ad0000" style="background-color: #ad0000"></span>
                      </li>
                                            
                    </ul>
                  </div>
                  
                  
                  <input type="hidden" id="stock" value="0">
                                    <input type="hidden" id="product_price" value="25">

                  <input type="hidden" id="product_id" value="186">
                  <input type="hidden" id="curr_pos" value="1">
                  <input type="hidden" id="curr_sign" value="$">
                  <div class="info-meta-3">
                    <ul class="meta-list">
                                            <li class="d-block count ">
                        <div class="qty">
                          <ul>
                            <li>
                              <span class="qtminus">
                                <i class="icofont-minus"></i>
                              </span>
                            </li>
                            <li>
                              <span class="qttotal">1</span>
                            </li>
                            <li>
                              <span class="qtplus">
                                <i class="icofont-plus"></i>
                              </span>
                            </li>
                          </ul>
                        </div>
                      </li>
                      
                                            
                                                                  <li class="addtocart">
                        <a href="javascript:;" id="addcrt"><i class="icofont-cart"></i>Add to Cart</a>
                      </li>
                                                    <li class="addtocart">
                            <a id="qaddcrt" href="javascript:;">
                              <i class="icofont-cart"></i>Buy Now
                            </a>
                          </li>
                                                
                      
                                            <li class="favorite">
                        <a href="javascript:;" data-toggle="modal" data-target="#comment-log-reg"><i class="icofont-heart-alt"></i></a>
                      </li>
                                            <li class="compare">
                        <a href="javascript:;" class="add-to-compare" data-href="https://royalscripts.com/product/kingcommerce/fashion/item/compare/add/186"><i class="icofont-exchange"></i></a>
                      </li>
                    </ul>
                  </div>
                  <div class="social-links social-sharing a2a_kit a2a_kit_size_32" style="line-height: 32px;">
                    <ul class="link-list social-links">
                      <li>
                        <a class="facebook a2a_button_facebook" href="/#facebook" target="_blank" rel="nofollow noopener">
                          <i class="fab fa-facebook-f"></i>
                        </a>
                      </li>
                      <li>
                        <a class="twitter a2a_button_twitter" href="/#twitter" target="_blank" rel="nofollow noopener">
                          <i class="fab fa-twitter"></i>
                        </a>
                      </li>
                      <li>
                        <a class="linkedin a2a_button_linkedin" href="/#linkedin" target="_blank" rel="nofollow noopener">
                          <i class="fab fa-linkedin-in"></i>
                        </a>
                      </li>
                      <li>
                        <a class="pinterest a2a_button_pinterest" href="/#pinterest" target="_blank" rel="nofollow noopener">
                          <i class="fab fa-pinterest-p"></i>
                        </a>
                      </li>
                    </ul>
                  </div>
                  <script async="" src="https://static.addtoany.com/menu/page.js"></script>


                                                      <p class="p-sku">
                    Product SKU: <span class="idno">nb90982LAE</span>
                  </p>
                        
      

                    
                    <div class="report-area">
                        <a href="javascript:;" data-toggle="modal" data-target="#comment-log-reg"><i class="fas fa-flag"></i> Report This Item</a>
                    </div>
                    
      

      


                </div>
              </div>
            </div>

          </div>
          <div class="row">
              <div class="col-lg-12">
                  <div id="product-details-tab" class="ui-tabs ui-corner-all ui-widget ui-widget-content">
                    <div class="top-menu-area">
                      <ul class="tab-menu ui-tabs-nav ui-corner-all ui-helper-reset ui-helper-clearfix ui-widget-header" role="tablist">
                        <li role="tab" tabindex="0" class="ui-tabs-tab ui-corner-top ui-state-default ui-tab ui-tabs-active ui-state-active" aria-controls="tabs-1" aria-labelledby="ui-id-1" aria-selected="true" aria-expanded="true"><a href="#tabs-1" role="presentation" tabindex="-1" class="ui-tabs-anchor" id="ui-id-1">DESCRIPTION</a></li>
                        <li role="tab" tabindex="-1" class="ui-tabs-tab ui-corner-top ui-state-default ui-tab" aria-controls="tabs-2" aria-labelledby="ui-id-2" aria-selected="false" aria-expanded="false"><a href="#tabs-2" role="presentation" tabindex="-1" class="ui-tabs-anchor" id="ui-id-2">BUY &amp; RETURN POLICY</a></li>
                        <li role="tab" tabindex="-1" class="ui-tabs-tab ui-corner-top ui-state-default ui-tab" aria-controls="tabs-3" aria-labelledby="ui-id-3" aria-selected="false" aria-expanded="false"><a href="#tabs-3" role="presentation" tabindex="-1" class="ui-tabs-anchor" id="ui-id-3">Reviews(0)</a></li>
                                                <li role="tab" tabindex="-1" class="ui-tabs-tab ui-corner-top ui-state-default ui-tab" aria-controls="tabs-4" aria-labelledby="ui-id-4" aria-selected="false" aria-expanded="false"><a href="#tabs-4" role="presentation" tabindex="-1" class="ui-tabs-anchor" id="ui-id-4">Comment(<span id="comment_count">0</span>)</a></li>
                                              </ul>
                    </div>
                    <div class="tab-content-wrapper">
                      <div id="tabs-1" class="tab-content-area ui-tabs-panel ui-corner-bottom ui-widget-content" aria-labelledby="ui-id-1" role="tabpanel" aria-hidden="false">
                        <p><span style="color: rgb(0, 0, 0); font-family: " open="" sans",="" arial,="" sans-serif;="" text-align:="" justify;"="">Lorem ipsum dolor sit amet, consectetur adipiscing elit. In aliquet tellus ornare lacus bibendum sollicitudin. Nunc interdum libero sit amet sapien suscipit placerat. Curabitur posuere nec diam ac aliquet. Donec iaculis maximus nisl ut convallis. Sed non leo eros. Ut a mollis est, vitae facilisis justo. Aenean et velit elit. Curabitur rutrum vitae tellus nec volutpat. Vestibulum dapibus viverra tempor. Cras a libero pharetra, tempus purus viverra, eleifend nulla.</span><br></p>
                      </div>
                      <div id="tabs-2" class="tab-content-area ui-tabs-panel ui-corner-bottom ui-widget-content" aria-labelledby="ui-id-2" role="tabpanel" aria-hidden="true" style="display: none;">
                        <p><span style="color: rgb(0, 0, 0); font-family: " open="" sans",="" arial,="" sans-serif;="" text-align:="" justify;"="">Lorem ipsum dolor sit amet, consectetur adipiscing elit. In aliquet tellus ornare lacus bibendum sollicitudin. Nunc interdum libero sit amet sapien suscipit placerat. Curabitur posuere nec diam ac aliquet. Donec iaculis maximus nisl ut convallis. Sed non leo eros. Ut a mollis est, vitae facilisis justo. Aenean et velit elit. Curabitur rutrum vitae tellus nec volutpat. Vestibulum dapibus viverra tempor. Cras a libero pharetra, tempus purus viverra, eleifend nulla.</span><br></p>
                      </div>
                      <div id="tabs-3" class="tab-content-area ui-tabs-panel ui-corner-bottom ui-widget-content" aria-labelledby="ui-id-3" role="tabpanel" aria-hidden="true" style="display: none;">
                        <div class="heading-area">
                          <h4 class="title">
                            Ratings &amp; Reviews
                          </h4>
                          <div class="reating-area">
                            <div class="stars"><span id="star-rating">0.0</span> <i class="fas fa-star"></i></div>
                          </div>
                        </div>
                        <div id="replay-area">
                          <div id="reviews-section">
                                                        <p>No Review Found.</p>
                                                      </div>
                                                    <div class="row">
                            <div class="col-lg-12">
                              <br>
                              <h5 class="text-center"><a href="javascript:;" data-toggle="modal" data-target="#comment-log-reg" class="btn login-btn mr-1">Login</a> To Review</h5>
                              <br>
                            </div>
                          </div>
                                                  </div>
                      </div>
                                            <div id="tabs-4" class="tab-content-area ui-tabs-panel ui-corner-bottom ui-widget-content" aria-labelledby="ui-id-4" role="tabpanel" aria-hidden="true" style="display: none;">
                        <div id="comment-area">

                          <div class="row">
<div class="col-lg-12">
<br>
  <h3 class="text-center"><a href="javascript:;" data-toggle="modal" data-target="#comment-log-reg" class="btn login-btn">Login</a> To Comment </h3>
<br>
</div>
</div>

  
<ul class="all-comment">

  </ul>


                        </div>
                      </div>
                                          </div>
                  </div>
                </div>
          </div>
    </div>
    <div class="col-lg-3">

      

      <div class="seller-info mt-3">
        <div class="content">
          <h4 class="title">
            Sold By
          </h4>

          <p class="stor-name">
                         Genius Store
                    </p>

          <div class="total-product">

                         <p>13</p>
                      <span>Total Item</span>
          </div>
        </div>
    
                  



                  <div class="contact-seller">

                    

                    

                    

                    <ul class="list">
                                            <li>
                        <a class="view-stor" href="javascript:;" data-toggle="modal" data-target="#comment-log-reg">
                          <i class="icofont-ui-chat"></i>
                          Contact Seller
                        </a>
                      </li>

                      
                    </ul>

                    
                  </div>

                  

      </div>








      <div class="categori  mt-30">
        <div class="section-top">
            <h2 class="section-title">
                Seller's Products
            </h2>
        </div>
                        <div class="hot-and-new-item-slider owl-carousel owl-theme owl-loaded">

                                                      
                                                      
                                                      
                          
                        <div class="owl-stage-outer"><div class="owl-stage" style="transform: translate3d(-1020px, 0px, 0px); transition: all 0.8s ease 0s; width: 1785px;"><div class="owl-item cloned" style="width: 255px; margin-right: 0px;"><div class="item-slide">
                              <ul class="item-list">
                                                                  <div class="single-box">
											<div class="left-area">
												<img src="https://royalscripts.com/product/kingcommerce/fashion/assets/images/thumbnails/1615885461aMk85tfz.jpg" alt="">
											</div>
											<div class="right-area">
													<h4 class="price">25$ <del>28$</del> </h4>
												<div class="stars">
													<div class="ratings">
														<div class="empty-stars"></div>
														<div class="full-stars" style="width:0%"></div>
													</div>
												</div>
												<p class="text"><a href="https://royalscripts.com/product/kingcommerce/fashion/item/kingcommerce-fashion-products-title-will-be-here-one-01-nb90982lae">KingCommerce Fashion Products Title Will be here one 01</a></p>

												<ul class="action-meta">
														<li>
																									
																<span href="javascript:;" class="wish" rel-toggle="tooltip" title="" data-toggle="modal" id="wish-btn" data-target="#comment-log-reg" data-placement="top" data-original-title="Add To Wishlist">
																	<i class="far fa-heart"></i>
																</span>
									
																														</li>
														<li>
															<span class="cart-btn quick-view" rel-toggle="tooltip" title="" href="javascript:;" data-href="https://royalscripts.com/product/kingcommerce/fashion/item/quick/view/186" data-toggle="modal" data-target="#quickview" data-placement="top" data-original-title="Quick View">
																	<i class="fas fa-shopping-basket"></i>
															</span>
														</li>
														<li>
																<span href="javascript:;" class="compear add-to-compare" data-href="https://royalscripts.com/product/kingcommerce/fashion/item/compare/add/186" data-toggle="tooltip" data-placement="top" title="" data-original-title="Compare">
																<i class="fas fa-random"></i>
															</span>
														</li>
													</ul>																	
											</div>
										</div>

								                                                                  <div class="single-box">
											<div class="left-area">
												<img src="https://royalscripts.com/product/kingcommerce/fashion/assets/images/thumbnails/1615885428lFqCDMth.jpg" alt="">
											</div>
											<div class="right-area">
													<h4 class="price">15$ <del>20$</del> </h4>
												<div class="stars">
													<div class="ratings">
														<div class="empty-stars"></div>
														<div class="full-stars" style="width:0%"></div>
													</div>
												</div>
												<p class="text"><a href="https://royalscripts.com/product/kingcommerce/fashion/item/kingcommerce-fashion-products-title-will-be-here-one-01-nb90982lar">KingCommerce Fashion Products Title Will be here one 01</a></p>

												<ul class="action-meta">
														<li>
																									
																<span href="javascript:;" class="wish" rel-toggle="tooltip" title="" data-toggle="modal" id="wish-btn" data-target="#comment-log-reg" data-placement="top" data-original-title="Add To Wishlist">
																	<i class="far fa-heart"></i>
																</span>
									
																														</li>
														<li>
															<span class="cart-btn quick-view" rel-toggle="tooltip" title="" href="javascript:;" data-href="https://royalscripts.com/product/kingcommerce/fashion/item/quick/view/187" data-toggle="modal" data-target="#quickview" data-placement="top" data-original-title="Quick View">
																	<i class="fas fa-shopping-basket"></i>
															</span>
														</li>
														<li>
																<span href="javascript:;" class="compear add-to-compare" data-href="https://royalscripts.com/product/kingcommerce/fashion/item/compare/add/187" data-toggle="tooltip" data-placement="top" title="" data-original-title="Compare">
																<i class="fas fa-random"></i>
															</span>
														</li>
													</ul>																	
											</div>
										</div>

								                                                                  <div class="single-box">
											<div class="left-area">
												<img src="https://royalscripts.com/product/kingcommerce/fashion/assets/images/thumbnails/1615885365tPtXqQ9W.jpg" alt="">
											</div>
											<div class="right-area">
													<h4 class="price">50$ <del>55$</del> </h4>
												<div class="stars">
													<div class="ratings">
														<div class="empty-stars"></div>
														<div class="full-stars" style="width:0%"></div>
													</div>
												</div>
												<p class="text"><a href="https://royalscripts.com/product/kingcommerce/fashion/item/kingcommerce-fashion-products-title-will-be-here-one-01-nb90982lat">KingCommerce Fashion Products Title Will be here one 01</a></p>

												<ul class="action-meta">
														<li>
																									
																<span href="javascript:;" class="wish" rel-toggle="tooltip" title="" data-toggle="modal" id="wish-btn" data-target="#comment-log-reg" data-placement="top" data-original-title="Add To Wishlist">
																	<i class="far fa-heart"></i>
																</span>
									
																														</li>
														<li>
															<span class="cart-btn quick-view" rel-toggle="tooltip" title="" href="javascript:;" data-href="https://royalscripts.com/product/kingcommerce/fashion/item/quick/view/188" data-toggle="modal" data-target="#quickview" data-placement="top" data-original-title="Quick View">
																	<i class="fas fa-shopping-basket"></i>
															</span>
														</li>
														<li>
																<span href="javascript:;" class="compear add-to-compare" data-href="https://royalscripts.com/product/kingcommerce/fashion/item/compare/add/188" data-toggle="tooltip" data-placement="top" title="" data-original-title="Compare">
																<i class="fas fa-random"></i>
															</span>
														</li>
													</ul>																	
											</div>
										</div>

								                                                              </ul>
                            </div></div><div class="owl-item cloned" style="width: 255px; margin-right: 0px;"><div class="item-slide">
                              <ul class="item-list">
                                                                  <div class="single-box">
											<div class="left-area">
												<img src="https://royalscripts.com/product/kingcommerce/fashion/assets/images/thumbnails/16158853269IywRAmW.jpg" alt="">
											</div>
											<div class="right-area">
													<h4 class="price">40$ <del>45$</del> </h4>
												<div class="stars">
													<div class="ratings">
														<div class="empty-stars"></div>
														<div class="full-stars" style="width:0%"></div>
													</div>
												</div>
												<p class="text"><a href="https://royalscripts.com/product/kingcommerce/fashion/item/kingcommerce-fashion-products-title-will-be-here-one-01-nb90982lay">KingCommerce Fashion Products Title Will be here one 01</a></p>

												<ul class="action-meta">
														<li>
																									
																<span href="javascript:;" class="wish" rel-toggle="tooltip" title="" data-toggle="modal" id="wish-btn" data-target="#comment-log-reg" data-placement="top" data-original-title="Add To Wishlist">
																	<i class="far fa-heart"></i>
																</span>
									
																														</li>
														<li>
															<span class="cart-btn quick-view" rel-toggle="tooltip" title="" href="javascript:;" data-href="https://royalscripts.com/product/kingcommerce/fashion/item/quick/view/189" data-toggle="modal" data-target="#quickview" data-placement="top" data-original-title="Quick View">
																	<i class="fas fa-shopping-basket"></i>
															</span>
														</li>
														<li>
																<span href="javascript:;" class="compear add-to-compare" data-href="https://royalscripts.com/product/kingcommerce/fashion/item/compare/add/189" data-toggle="tooltip" data-placement="top" title="" data-original-title="Compare">
																<i class="fas fa-random"></i>
															</span>
														</li>
													</ul>																	
											</div>
										</div>

								                                                                  <div class="single-box">
											<div class="left-area">
												<img src="https://royalscripts.com/product/kingcommerce/fashion/assets/images/thumbnails/1615885288vGFHcIxR.jpg" alt="">
											</div>
											<div class="right-area">
													<h4 class="price">10$ <del>15$</del> </h4>
												<div class="stars">
													<div class="ratings">
														<div class="empty-stars"></div>
														<div class="full-stars" style="width:0%"></div>
													</div>
												</div>
												<p class="text"><a href="https://royalscripts.com/product/kingcommerce/fashion/item/kingcommerce-fashion-products-title-will-be-here-one-01-nb90982lau">KingCommerce Fashion Products Title Will be here one 01</a></p>

												<ul class="action-meta">
														<li>
																									
																<span href="javascript:;" class="wish" rel-toggle="tooltip" title="" data-toggle="modal" id="wish-btn" data-target="#comment-log-reg" data-placement="top" data-original-title="Add To Wishlist">
																	<i class="far fa-heart"></i>
																</span>
									
																														</li>
														<li>
															<span class="cart-btn quick-view" rel-toggle="tooltip" title="" href="javascript:;" data-href="https://royalscripts.com/product/kingcommerce/fashion/item/quick/view/190" data-toggle="modal" data-target="#quickview" data-placement="top" data-original-title="Quick View">
																	<i class="fas fa-shopping-basket"></i>
															</span>
														</li>
														<li>
																<span href="javascript:;" class="compear add-to-compare" data-href="https://royalscripts.com/product/kingcommerce/fashion/item/compare/add/190" data-toggle="tooltip" data-placement="top" title="" data-original-title="Compare">
																<i class="fas fa-random"></i>
															</span>
														</li>
													</ul>																	
											</div>
										</div>

								                                                              </ul>
                            </div></div><div class="owl-item" style="width: 255px; margin-right: 0px;"><div class="item-slide">
                              <ul class="item-list">
                                                                  <div class="single-box">
											<div class="left-area">
												<img src="https://royalscripts.com/product/kingcommerce/fashion/assets/images/thumbnails/1615885755t9AvwiUc.jpg" alt="">
											</div>
											<div class="right-area">
													<h4 class="price">14$ <del>17$</del> </h4>
												<div class="stars">
													<div class="ratings">
														<div class="empty-stars"></div>
														<div class="full-stars" style="width:0%"></div>
													</div>
												</div>
												<p class="text"><a href="https://royalscripts.com/product/kingcommerce/fashion/item/kingcommerce-fashion-products-title-will-be-here-one-01-nb90982la1">KingCommerce Fashion Products Title Will be here one 01</a></p>

												<ul class="action-meta">
														<li>
																									
																<span href="javascript:;" class="wish" rel-toggle="tooltip" title="" data-toggle="modal" id="wish-btn" data-target="#comment-log-reg" data-placement="top" data-original-title="Add To Wishlist">
																	<i class="far fa-heart"></i>
																</span>
									
																														</li>
														<li>
															<span class="cart-btn quick-view" rel-toggle="tooltip" title="" href="javascript:;" data-href="https://royalscripts.com/product/kingcommerce/fashion/item/quick/view/183" data-toggle="modal" data-target="#quickview" data-placement="top" data-original-title="Quick View">
																	<i class="fas fa-shopping-basket"></i>
															</span>
														</li>
														<li>
																<span href="javascript:;" class="compear add-to-compare" data-href="https://royalscripts.com/product/kingcommerce/fashion/item/compare/add/183" data-toggle="tooltip" data-placement="top" title="" data-original-title="Compare">
																<i class="fas fa-random"></i>
															</span>
														</li>
													</ul>																	
											</div>
										</div>

								                                                                  <div class="single-box">
											<div class="left-area">
												<img src="https://royalscripts.com/product/kingcommerce/fashion/assets/images/thumbnails/1615885717pseQ775U.jpg" alt="">
											</div>
											<div class="right-area">
													<h4 class="price">65$ <del>70$</del> </h4>
												<div class="stars">
													<div class="ratings">
														<div class="empty-stars"></div>
														<div class="full-stars" style="width:0%"></div>
													</div>
												</div>
												<p class="text"><a href="https://royalscripts.com/product/kingcommerce/fashion/item/kingcommerce-fashion-products-title-will-be-here-one-01-nb90982laq">KingCommerce Fashion Products Title Will be here one 01</a></p>

												<ul class="action-meta">
														<li>
																									
																<span href="javascript:;" class="wish" rel-toggle="tooltip" title="" data-toggle="modal" id="wish-btn" data-target="#comment-log-reg" data-placement="top" data-original-title="Add To Wishlist">
																	<i class="far fa-heart"></i>
																</span>
									
																														</li>
														<li>
															<span class="cart-btn quick-view" rel-toggle="tooltip" title="" href="javascript:;" data-href="https://royalscripts.com/product/kingcommerce/fashion/item/quick/view/184" data-toggle="modal" data-target="#quickview" data-placement="top" data-original-title="Quick View">
																	<i class="fas fa-shopping-basket"></i>
															</span>
														</li>
														<li>
																<span href="javascript:;" class="compear add-to-compare" data-href="https://royalscripts.com/product/kingcommerce/fashion/item/compare/add/184" data-toggle="tooltip" data-placement="top" title="" data-original-title="Compare">
																<i class="fas fa-random"></i>
															</span>
														</li>
													</ul>																	
											</div>
										</div>

								                                                                  <div class="single-box">
											<div class="left-area">
												<img src="https://royalscripts.com/product/kingcommerce/fashion/assets/images/thumbnails/1615885666fh5e03mO.jpg" alt="">
											</div>
											<div class="right-area">
													<h4 class="price">16$ <del>20$</del> </h4>
												<div class="stars">
													<div class="ratings">
														<div class="empty-stars"></div>
														<div class="full-stars" style="width:0%"></div>
													</div>
												</div>
												<p class="text"><a href="https://royalscripts.com/product/kingcommerce/fashion/item/kingcommerce-fashion-products-title-will-be-here-one-01-nb90982law">KingCommerce Fashion Products Title Will be here one 01</a></p>

												<ul class="action-meta">
														<li>
																									
																<span href="javascript:;" class="wish" rel-toggle="tooltip" title="" data-toggle="modal" id="wish-btn" data-target="#comment-log-reg" data-placement="top" data-original-title="Add To Wishlist">
																	<i class="far fa-heart"></i>
																</span>
									
																														</li>
														<li>
															<span class="cart-btn quick-view" rel-toggle="tooltip" title="" href="javascript:;" data-href="https://royalscripts.com/product/kingcommerce/fashion/item/quick/view/185" data-toggle="modal" data-target="#quickview" data-placement="top" data-original-title="Quick View">
																	<i class="fas fa-shopping-basket"></i>
															</span>
														</li>
														<li>
																<span href="javascript:;" class="compear add-to-compare" data-href="https://royalscripts.com/product/kingcommerce/fashion/item/compare/add/185" data-toggle="tooltip" data-placement="top" title="" data-original-title="Compare">
																<i class="fas fa-random"></i>
															</span>
														</li>
													</ul>																	
											</div>
										</div>

								                                                              </ul>
                            </div></div><div class="owl-item" style="width: 255px; margin-right: 0px;"><div class="item-slide">
                              <ul class="item-list">
                                                                  <div class="single-box">
											<div class="left-area">
												<img src="https://royalscripts.com/product/kingcommerce/fashion/assets/images/thumbnails/1615885461aMk85tfz.jpg" alt="">
											</div>
											<div class="right-area">
													<h4 class="price">25$ <del>28$</del> </h4>
												<div class="stars">
													<div class="ratings">
														<div class="empty-stars"></div>
														<div class="full-stars" style="width:0%"></div>
													</div>
												</div>
												<p class="text"><a href="https://royalscripts.com/product/kingcommerce/fashion/item/kingcommerce-fashion-products-title-will-be-here-one-01-nb90982lae">KingCommerce Fashion Products Title Will be here one 01</a></p>

												<ul class="action-meta">
														<li>
																									
																<span href="javascript:;" class="wish" rel-toggle="tooltip" title="" data-toggle="modal" id="wish-btn" data-target="#comment-log-reg" data-placement="top" data-original-title="Add To Wishlist">
																	<i class="far fa-heart"></i>
																</span>
									
																														</li>
														<li>
															<span class="cart-btn quick-view" rel-toggle="tooltip" title="" href="javascript:;" data-href="https://royalscripts.com/product/kingcommerce/fashion/item/quick/view/186" data-toggle="modal" data-target="#quickview" data-placement="top" data-original-title="Quick View">
																	<i class="fas fa-shopping-basket"></i>
															</span>
														</li>
														<li>
																<span href="javascript:;" class="compear add-to-compare" data-href="https://royalscripts.com/product/kingcommerce/fashion/item/compare/add/186" data-toggle="tooltip" data-placement="top" title="" data-original-title="Compare">
																<i class="fas fa-random"></i>
															</span>
														</li>
													</ul>																	
											</div>
										</div>

								                                                                  <div class="single-box">
											<div class="left-area">
												<img src="https://royalscripts.com/product/kingcommerce/fashion/assets/images/thumbnails/1615885428lFqCDMth.jpg" alt="">
											</div>
											<div class="right-area">
													<h4 class="price">15$ <del>20$</del> </h4>
												<div class="stars">
													<div class="ratings">
														<div class="empty-stars"></div>
														<div class="full-stars" style="width:0%"></div>
													</div>
												</div>
												<p class="text"><a href="https://royalscripts.com/product/kingcommerce/fashion/item/kingcommerce-fashion-products-title-will-be-here-one-01-nb90982lar">KingCommerce Fashion Products Title Will be here one 01</a></p>

												<ul class="action-meta">
														<li>
																									
																<span href="javascript:;" class="wish" rel-toggle="tooltip" title="" data-toggle="modal" id="wish-btn" data-target="#comment-log-reg" data-placement="top" data-original-title="Add To Wishlist">
																	<i class="far fa-heart"></i>
																</span>
									
																														</li>
														<li>
															<span class="cart-btn quick-view" rel-toggle="tooltip" title="" href="javascript:;" data-href="https://royalscripts.com/product/kingcommerce/fashion/item/quick/view/187" data-toggle="modal" data-target="#quickview" data-placement="top" data-original-title="Quick View">
																	<i class="fas fa-shopping-basket"></i>
															</span>
														</li>
														<li>
																<span href="javascript:;" class="compear add-to-compare" data-href="https://royalscripts.com/product/kingcommerce/fashion/item/compare/add/187" data-toggle="tooltip" data-placement="top" title="" data-original-title="Compare">
																<i class="fas fa-random"></i>
															</span>
														</li>
													</ul>																	
											</div>
										</div>

								                                                                  <div class="single-box">
											<div class="left-area">
												<img src="https://royalscripts.com/product/kingcommerce/fashion/assets/images/thumbnails/1615885365tPtXqQ9W.jpg" alt="">
											</div>
											<div class="right-area">
													<h4 class="price">50$ <del>55$</del> </h4>
												<div class="stars">
													<div class="ratings">
														<div class="empty-stars"></div>
														<div class="full-stars" style="width:0%"></div>
													</div>
												</div>
												<p class="text"><a href="https://royalscripts.com/product/kingcommerce/fashion/item/kingcommerce-fashion-products-title-will-be-here-one-01-nb90982lat">KingCommerce Fashion Products Title Will be here one 01</a></p>

												<ul class="action-meta">
														<li>
																									
																<span href="javascript:;" class="wish" rel-toggle="tooltip" title="" data-toggle="modal" id="wish-btn" data-target="#comment-log-reg" data-placement="top" data-original-title="Add To Wishlist">
																	<i class="far fa-heart"></i>
																</span>
									
																														</li>
														<li>
															<span class="cart-btn quick-view" rel-toggle="tooltip" title="" href="javascript:;" data-href="https://royalscripts.com/product/kingcommerce/fashion/item/quick/view/188" data-toggle="modal" data-target="#quickview" data-placement="top" data-original-title="Quick View">
																	<i class="fas fa-shopping-basket"></i>
															</span>
														</li>
														<li>
																<span href="javascript:;" class="compear add-to-compare" data-href="https://royalscripts.com/product/kingcommerce/fashion/item/compare/add/188" data-toggle="tooltip" data-placement="top" title="" data-original-title="Compare">
																<i class="fas fa-random"></i>
															</span>
														</li>
													</ul>																	
											</div>
										</div>

								                                                              </ul>
                            </div></div><div class="owl-item active" style="width: 255px; margin-right: 0px;"><div class="item-slide">
                              <ul class="item-list">
                                                                  <div class="single-box">
											<div class="left-area">
												<img src="https://royalscripts.com/product/kingcommerce/fashion/assets/images/thumbnails/16158853269IywRAmW.jpg" alt="">
											</div>
											<div class="right-area">
													<h4 class="price">40$ <del>45$</del> </h4>
												<div class="stars">
													<div class="ratings">
														<div class="empty-stars"></div>
														<div class="full-stars" style="width:0%"></div>
													</div>
												</div>
												<p class="text"><a href="https://royalscripts.com/product/kingcommerce/fashion/item/kingcommerce-fashion-products-title-will-be-here-one-01-nb90982lay">KingCommerce Fashion Products Title Will be here one 01</a></p>

												<ul class="action-meta">
														<li>
																									
																<span href="javascript:;" class="wish" rel-toggle="tooltip" title="" data-toggle="modal" id="wish-btn" data-target="#comment-log-reg" data-placement="top" data-original-title="Add To Wishlist">
																	<i class="far fa-heart"></i>
																</span>
									
																														</li>
														<li>
															<span class="cart-btn quick-view" rel-toggle="tooltip" title="" href="javascript:;" data-href="https://royalscripts.com/product/kingcommerce/fashion/item/quick/view/189" data-toggle="modal" data-target="#quickview" data-placement="top" data-original-title="Quick View">
																	<i class="fas fa-shopping-basket"></i>
															</span>
														</li>
														<li>
																<span href="javascript:;" class="compear add-to-compare" data-href="https://royalscripts.com/product/kingcommerce/fashion/item/compare/add/189" data-toggle="tooltip" data-placement="top" title="" data-original-title="Compare">
																<i class="fas fa-random"></i>
															</span>
														</li>
													</ul>																	
											</div>
										</div>

								                                                                  <div class="single-box">
											<div class="left-area">
												<img src="https://royalscripts.com/product/kingcommerce/fashion/assets/images/thumbnails/1615885288vGFHcIxR.jpg" alt="">
											</div>
											<div class="right-area">
													<h4 class="price">10$ <del>15$</del> </h4>
												<div class="stars">
													<div class="ratings">
														<div class="empty-stars"></div>
														<div class="full-stars" style="width:0%"></div>
													</div>
												</div>
												<p class="text"><a href="https://royalscripts.com/product/kingcommerce/fashion/item/kingcommerce-fashion-products-title-will-be-here-one-01-nb90982lau">KingCommerce Fashion Products Title Will be here one 01</a></p>

												<ul class="action-meta">
														<li>
																									
																<span href="javascript:;" class="wish" rel-toggle="tooltip" title="" data-toggle="modal" id="wish-btn" data-target="#comment-log-reg" data-placement="top" data-original-title="Add To Wishlist">
																	<i class="far fa-heart"></i>
																</span>
									
																														</li>
														<li>
															<span class="cart-btn quick-view" rel-toggle="tooltip" title="" href="javascript:;" data-href="https://royalscripts.com/product/kingcommerce/fashion/item/quick/view/190" data-toggle="modal" data-target="#quickview" data-placement="top" data-original-title="Quick View">
																	<i class="fas fa-shopping-basket"></i>
															</span>
														</li>
														<li>
																<span href="javascript:;" class="compear add-to-compare" data-href="https://royalscripts.com/product/kingcommerce/fashion/item/compare/add/190" data-toggle="tooltip" data-placement="top" title="" data-original-title="Compare">
																<i class="fas fa-random"></i>
															</span>
														</li>
													</ul>																	
											</div>
										</div>

								                                                              </ul>
                            </div></div><div class="owl-item cloned" style="width: 255px; margin-right: 0px;"><div class="item-slide">
                              <ul class="item-list">
                                                                  <div class="single-box">
											<div class="left-area">
												<img src="https://royalscripts.com/product/kingcommerce/fashion/assets/images/thumbnails/1615885755t9AvwiUc.jpg" alt="">
											</div>
											<div class="right-area">
													<h4 class="price">14$ <del>17$</del> </h4>
												<div class="stars">
													<div class="ratings">
														<div class="empty-stars"></div>
														<div class="full-stars" style="width:0%"></div>
													</div>
												</div>
												<p class="text"><a href="https://royalscripts.com/product/kingcommerce/fashion/item/kingcommerce-fashion-products-title-will-be-here-one-01-nb90982la1">KingCommerce Fashion Products Title Will be here one 01</a></p>

												<ul class="action-meta">
														<li>
																									
																<span href="javascript:;" class="wish" rel-toggle="tooltip" title="" data-toggle="modal" id="wish-btn" data-target="#comment-log-reg" data-placement="top" data-original-title="Add To Wishlist">
																	<i class="far fa-heart"></i>
																</span>
									
																														</li>
														<li>
															<span class="cart-btn quick-view" rel-toggle="tooltip" title="" href="javascript:;" data-href="https://royalscripts.com/product/kingcommerce/fashion/item/quick/view/183" data-toggle="modal" data-target="#quickview" data-placement="top" data-original-title="Quick View">
																	<i class="fas fa-shopping-basket"></i>
															</span>
														</li>
														<li>
																<span href="javascript:;" class="compear add-to-compare" data-href="https://royalscripts.com/product/kingcommerce/fashion/item/compare/add/183" data-toggle="tooltip" data-placement="top" title="" data-original-title="Compare">
																<i class="fas fa-random"></i>
															</span>
														</li>
													</ul>																	
											</div>
										</div>

								                                                                  <div class="single-box">
											<div class="left-area">
												<img src="https://royalscripts.com/product/kingcommerce/fashion/assets/images/thumbnails/1615885717pseQ775U.jpg" alt="">
											</div>
											<div class="right-area">
													<h4 class="price">65$ <del>70$</del> </h4>
												<div class="stars">
													<div class="ratings">
														<div class="empty-stars"></div>
														<div class="full-stars" style="width:0%"></div>
													</div>
												</div>
												<p class="text"><a href="https://royalscripts.com/product/kingcommerce/fashion/item/kingcommerce-fashion-products-title-will-be-here-one-01-nb90982laq">KingCommerce Fashion Products Title Will be here one 01</a></p>

												<ul class="action-meta">
														<li>
																									
																<span href="javascript:;" class="wish" rel-toggle="tooltip" title="" data-toggle="modal" id="wish-btn" data-target="#comment-log-reg" data-placement="top" data-original-title="Add To Wishlist">
																	<i class="far fa-heart"></i>
																</span>
									
																														</li>
														<li>
															<span class="cart-btn quick-view" rel-toggle="tooltip" title="" href="javascript:;" data-href="https://royalscripts.com/product/kingcommerce/fashion/item/quick/view/184" data-toggle="modal" data-target="#quickview" data-placement="top" data-original-title="Quick View">
																	<i class="fas fa-shopping-basket"></i>
															</span>
														</li>
														<li>
																<span href="javascript:;" class="compear add-to-compare" data-href="https://royalscripts.com/product/kingcommerce/fashion/item/compare/add/184" data-toggle="tooltip" data-placement="top" title="" data-original-title="Compare">
																<i class="fas fa-random"></i>
															</span>
														</li>
													</ul>																	
											</div>
										</div>

								                                                                  <div class="single-box">
											<div class="left-area">
												<img src="https://royalscripts.com/product/kingcommerce/fashion/assets/images/thumbnails/1615885666fh5e03mO.jpg" alt="">
											</div>
											<div class="right-area">
													<h4 class="price">16$ <del>20$</del> </h4>
												<div class="stars">
													<div class="ratings">
														<div class="empty-stars"></div>
														<div class="full-stars" style="width:0%"></div>
													</div>
												</div>
												<p class="text"><a href="https://royalscripts.com/product/kingcommerce/fashion/item/kingcommerce-fashion-products-title-will-be-here-one-01-nb90982law">KingCommerce Fashion Products Title Will be here one 01</a></p>

												<ul class="action-meta">
														<li>
																									
																<span href="javascript:;" class="wish" rel-toggle="tooltip" title="" data-toggle="modal" id="wish-btn" data-target="#comment-log-reg" data-placement="top" data-original-title="Add To Wishlist">
																	<i class="far fa-heart"></i>
																</span>
									
																														</li>
														<li>
															<span class="cart-btn quick-view" rel-toggle="tooltip" title="" href="javascript:;" data-href="https://royalscripts.com/product/kingcommerce/fashion/item/quick/view/185" data-toggle="modal" data-target="#quickview" data-placement="top" data-original-title="Quick View">
																	<i class="fas fa-shopping-basket"></i>
															</span>
														</li>
														<li>
																<span href="javascript:;" class="compear add-to-compare" data-href="https://royalscripts.com/product/kingcommerce/fashion/item/compare/add/185" data-toggle="tooltip" data-placement="top" title="" data-original-title="Compare">
																<i class="fas fa-random"></i>
															</span>
														</li>
													</ul>																	
											</div>
										</div>

								                                                              </ul>
                            </div></div><div class="owl-item cloned" style="width: 255px; margin-right: 0px;"><div class="item-slide">
                              <ul class="item-list">
                                                                  <div class="single-box">
											<div class="left-area">
												<img src="https://royalscripts.com/product/kingcommerce/fashion/assets/images/thumbnails/1615885461aMk85tfz.jpg" alt="">
											</div>
											<div class="right-area">
													<h4 class="price">25$ <del>28$</del> </h4>
												<div class="stars">
													<div class="ratings">
														<div class="empty-stars"></div>
														<div class="full-stars" style="width:0%"></div>
													</div>
												</div>
												<p class="text"><a href="https://royalscripts.com/product/kingcommerce/fashion/item/kingcommerce-fashion-products-title-will-be-here-one-01-nb90982lae">KingCommerce Fashion Products Title Will be here one 01</a></p>

												<ul class="action-meta">
														<li>
																									
																<span href="javascript:;" class="wish" rel-toggle="tooltip" title="" data-toggle="modal" id="wish-btn" data-target="#comment-log-reg" data-placement="top" data-original-title="Add To Wishlist">
																	<i class="far fa-heart"></i>
																</span>
									
																														</li>
														<li>
															<span class="cart-btn quick-view" rel-toggle="tooltip" title="" href="javascript:;" data-href="https://royalscripts.com/product/kingcommerce/fashion/item/quick/view/186" data-toggle="modal" data-target="#quickview" data-placement="top" data-original-title="Quick View">
																	<i class="fas fa-shopping-basket"></i>
															</span>
														</li>
														<li>
																<span href="javascript:;" class="compear add-to-compare" data-href="https://royalscripts.com/product/kingcommerce/fashion/item/compare/add/186" data-toggle="tooltip" data-placement="top" title="" data-original-title="Compare">
																<i class="fas fa-random"></i>
															</span>
														</li>
													</ul>																	
											</div>
										</div>

								                                                                  <div class="single-box">
											<div class="left-area">
												<img src="https://royalscripts.com/product/kingcommerce/fashion/assets/images/thumbnails/1615885428lFqCDMth.jpg" alt="">
											</div>
											<div class="right-area">
													<h4 class="price">15$ <del>20$</del> </h4>
												<div class="stars">
													<div class="ratings">
														<div class="empty-stars"></div>
														<div class="full-stars" style="width:0%"></div>
													</div>
												</div>
												<p class="text"><a href="https://royalscripts.com/product/kingcommerce/fashion/item/kingcommerce-fashion-products-title-will-be-here-one-01-nb90982lar">KingCommerce Fashion Products Title Will be here one 01</a></p>

												<ul class="action-meta">
														<li>
																									
																<span href="javascript:;" class="wish" rel-toggle="tooltip" title="" data-toggle="modal" id="wish-btn" data-target="#comment-log-reg" data-placement="top" data-original-title="Add To Wishlist">
																	<i class="far fa-heart"></i>
																</span>
									
																														</li>
														<li>
															<span class="cart-btn quick-view" rel-toggle="tooltip" title="" href="javascript:;" data-href="https://royalscripts.com/product/kingcommerce/fashion/item/quick/view/187" data-toggle="modal" data-target="#quickview" data-placement="top" data-original-title="Quick View">
																	<i class="fas fa-shopping-basket"></i>
															</span>
														</li>
														<li>
																<span href="javascript:;" class="compear add-to-compare" data-href="https://royalscripts.com/product/kingcommerce/fashion/item/compare/add/187" data-toggle="tooltip" data-placement="top" title="" data-original-title="Compare">
																<i class="fas fa-random"></i>
															</span>
														</li>
													</ul>																	
											</div>
										</div>

								                                                                  <div class="single-box">
											<div class="left-area">
												<img src="https://royalscripts.com/product/kingcommerce/fashion/assets/images/thumbnails/1615885365tPtXqQ9W.jpg" alt="">
											</div>
											<div class="right-area">
													<h4 class="price">50$ <del>55$</del> </h4>
												<div class="stars">
													<div class="ratings">
														<div class="empty-stars"></div>
														<div class="full-stars" style="width:0%"></div>
													</div>
												</div>
												<p class="text"><a href="https://royalscripts.com/product/kingcommerce/fashion/item/kingcommerce-fashion-products-title-will-be-here-one-01-nb90982lat">KingCommerce Fashion Products Title Will be here one 01</a></p>

												<ul class="action-meta">
														<li>
																									
																<span href="javascript:;" class="wish" rel-toggle="tooltip" title="" data-toggle="modal" id="wish-btn" data-target="#comment-log-reg" data-placement="top" data-original-title="Add To Wishlist">
																	<i class="far fa-heart"></i>
																</span>
									
																														</li>
														<li>
															<span class="cart-btn quick-view" rel-toggle="tooltip" title="" href="javascript:;" data-href="https://royalscripts.com/product/kingcommerce/fashion/item/quick/view/188" data-toggle="modal" data-target="#quickview" data-placement="top" data-original-title="Quick View">
																	<i class="fas fa-shopping-basket"></i>
															</span>
														</li>
														<li>
																<span href="javascript:;" class="compear add-to-compare" data-href="https://royalscripts.com/product/kingcommerce/fashion/item/compare/add/188" data-toggle="tooltip" data-placement="top" title="" data-original-title="Compare">
																<i class="fas fa-random"></i>
															</span>
														</li>
													</ul>																	
											</div>
										</div>

								                                                              </ul>
                            </div></div></div></div><div class="owl-controls"><div class="owl-nav"><div class="owl-prev" style=""><i class="fa fa-angle-left"></i></div><div class="owl-next" style=""><i class="fa fa-angle-right"></i></div></div><div class="owl-dots" style=""><div class="owl-dot"><span></span></div><div class="owl-dot"><span></span></div><div class="owl-dot active"><span></span></div></div></div></div>

    </div>




    </div>

    </div>
    <div class="row">
      <div class="col-lg-12">

      </div>
    </div>
  </div>
  <!-- Trending Item Area Start -->
<div class="trending">
  <div class="container">
    <div class="row">
      <div class="col-lg-12 remove-padding">
        <div class="section-top">
          <h2 class="section-title">
            Related Products
          </h2>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-12 remove-padding">
        <div class="trending-item-slider owl-carousel owl-theme owl-loaded">
                    
                  <div class="owl-stage-outer"><div class="owl-stage" style="transform: translate3d(0px, 0px, 0px); transition: all 0s ease 0s; width: 285px;"><div class="owl-item active" style="width: 285px; margin-right: 0px;"><a href="https://royalscripts.com/product/kingcommerce/fashion/item/kingcommerce-fashion-products-title-will-be-here-one-01-nb90982lat" class="item">
		<div class="item-img">
						<img class="img-fluid" src="https://royalscripts.com/product/kingcommerce/fashion/assets/images/thumbnails/1615885365tPtXqQ9W.jpg" alt="">
		</div>
		<div class="info">
			<h5 class="name">KingCommerce Fashion Products Title Will be here one 01</h5>
			<h4 class="price">50$
				<del><small>55$</small></del></h4>
			<div class="stars">
				<div class="ratings">
					<div class="empty-stars"></div>
					<div class="full-stars" style="width:0%"></div>
				</div>
			</div>
			<div class="item-cart-area">
				
				<ul class="item-cart-options">
					<li>
							
							<span href="javascript:;" rel-toggle="tooltip" title="" data-toggle="modal" id="wish-btn" data-target="#comment-log-reg" data-placement="top" data-original-title="Add To Wishlist">
								<i class="icofont-heart-alt"></i>
							</span>

												</li>
					<li>
						<span class="quick-view" rel-toggle="tooltip" title="" href="javascript:;" data-href="https://royalscripts.com/product/kingcommerce/fashion/item/quick/view/188" data-toggle="modal" data-target="#quickview" data-placement="top" data-original-title="Quick View">
								<i class="fas fa-shopping-basket"></i>
						</span>
					</li>
					<li>
							<span href="javascript:;" class="add-to-compare" data-href="https://royalscripts.com/product/kingcommerce/fashion/item/compare/add/188" data-toggle="tooltip" data-placement="top" title="" data-original-title="Compare">
							<i class="icofont-exchange"></i>
						</span>
					</li>
				</ul>
			</div>
		</div>
	</a></div></div></div><div class="owl-controls"><div class="owl-nav"><div class="owl-prev" style=""><i class="fa fa-angle-left"></i></div><div class="owl-next" style=""><i class="fa fa-angle-right"></i></div></div><div class="owl-dots" style=""><div class="owl-dot active"><span></span></div></div></div></div>
      </div>

    </div>
  </div>
</div>
<!-- Tranding Item Area End -->
</section>

</x-master>