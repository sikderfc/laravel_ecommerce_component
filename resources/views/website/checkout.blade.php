
<x-master>

<div class="breadcrumb-area">
	<div class="container">
	  <div class="row">
		<div class="col-lg-12">
		  <ul class="pages">
			<li>
			  <a href="https://royalscripts.com/product/kingcommerce/fashion">
				Home
			  </a>
			</li>
			<li>
			  <a href="about.html">
				Checkout
			  </a>
			</li>
		  </ul>
		</div>
	  </div>
	</div>
  </div>
  <section class="checkout">
		<div class="container">
			<div class="row">

				<div class="col-lg-12">
					<div class="checkout-area mb-0 pb-0">
						<div class="checkout-process">
							<ul class="nav" role="tablist">
								<li class="nav-item">
									<a class="nav-link active" id="pills-step1-tab" data-toggle="pill" href="#pills-step1" role="tab" aria-controls="pills-step1" aria-selected="true">
									<span>1</span> Address
									<i class="far fa-address-card"></i>
									</a>
								</li>
								<li class="nav-item">
									<a class="nav-link disabled" id="pills-step2-tab" data-toggle="pill" href="#pills-step2" role="tab" aria-controls="pills-step2" aria-selected="false">
										<span>2</span> Orders 
										<i class="fas fa-dolly"></i>
									</a>
								</li>
								<li class="nav-item">
									<a class="nav-link disabled" id="pills-step3-tab" data-toggle="pill" href="#pills-step3" role="tab" aria-controls="pills-step3" aria-selected="false">
											<span>3</span> Payment
											<i class="far fa-credit-card"></i>
									</a>
								</li>
							</ul>
						</div>
					</div>
				</div>


			</div>
			<div class="row">
				


				<div class="col-lg-8 order-last order-lg-first">




		<form id="twocheckout" action="https://royalscripts.com/product/kingcommerce/fashion/paypal-submit" method="POST" class="checkoutform">

						
			<input type="hidden" name="_token" value="CBJWN9bzyVS3PMSovTbjfJWhRLRJj3dwA9upuNn5">

					<div class="checkout-area">
						<div class="tab-content" id="pills-tabContent">
							<div class="tab-pane fade show active" id="pills-step1" role="tabpanel" aria-labelledby="pills-step1-tab">
								<div class="content-box">
								
									<div class="content">
										<div class="personal-info">
											<h5 class="title">
												Personal Information :
											</h5>
											<div class="row">
												<div class="col-lg-6">
													<input type="text" id="personal-name" class="form-control" name="personal_name" placeholder="Enter Your Name" value="">
												</div>
												<div class="col-lg-6">
													<input type="email" id="personal-email" class="form-control" name="personal_email" placeholder="Enter Your Email" value="">
												</div>
											</div>
																						<div class="row">
												<div class="col-lg-12 mt-3">
														<input class="styled-checkbox" id="open-pass" type="checkbox" value="1" name="pass_check">
														<label for="open-pass">Create an account ?</label>
												</div>
											</div>
											<div class="row set-account-pass d-none">
												<div class="col-lg-6">
													<input type="password" name="personal_pass" id="personal-pass" class="form-control" placeholder="Enter Your Password">
												</div>
												<div class="col-lg-6">
													<input type="password" name="personal_confirm" id="personal-pass-confirm" class="form-control" placeholder="Confirm Your Password">
												</div>
											</div>
																					</div>
										<div class="billing-address">
											<h5 class="title">
												Billing Details
											</h5>
											<div class="row">
												<div class="col-lg-6 ">
													<select class="form-control" id="shipop" name="shipping" required="">
														<option value="shipto">Ship To Address</option>
														<option value="pickup">Pick Up</option>
													</select>
												</div>
		
												<div class="col-lg-6 d-none" id="shipshow">
													<select class="form-control nice" name="pickup_location" style="display: none;">
																												<option value="Azampur">Azampur</option>
																												<option value="Dhaka">Dhaka</option>
																												<option value="Kazipara">Kazipara</option>
																												<option value="Kamarpara">Kamarpara</option>
																												<option value="Uttara">Uttara</option>
																											</select><div class="nice-select form-control nice" tabindex="0"><span class="current">Azampur</span><ul class="list"><li data-value="Azampur" class="option selected">Azampur</li><li data-value="Dhaka" class="option">Dhaka</li><li data-value="Kazipara" class="option">Kazipara</li><li data-value="Kamarpara" class="option">Kamarpara</li><li data-value="Uttara" class="option">Uttara</li></ul></div>
												</div>
		
												<div class="col-lg-6">
													<input class="form-control" type="text" name="name" placeholder="Full Name" required="" value="">
												</div>
												<div class="col-lg-6">
													<input class="form-control" type="text" name="phone" placeholder="Phone Number" required="" value="">
												</div>
												<div class="col-lg-6">
													<input class="form-control" type="text" name="email" placeholder="Email" required="" value="">
												</div>
												<div class="col-lg-6">
													<input class="form-control" type="text" name="address" placeholder="Address" required="" value="">
												</div>
												<div class="col-lg-6">
													<select class="form-control" name="customer_country" required="">
														<option value="">Select Country</option>
					<option value="Afghanistan">Afghanistan</option>		
				<option value="Albania">Albania</option>		
				<option value="Algeria">Algeria</option>		
				<option value="American Samoa">American Samoa</option>		
				<option value="Andorra">Andorra</option>		
				<option value="Angola">Angola</option>		
				<option value="Anguilla">Anguilla</option>		
				<option value="Antarctica">Antarctica</option>		
				<option value="Antigua and Barbuda">Antigua and Barbuda</option>		
				<option value="Argentina">Argentina</option>		
				<option value="Armenia">Armenia</option>		
				<option value="Aruba">Aruba</option>		
				<option value="Australia">Australia</option>		
				<option value="Austria">Austria</option>		
				<option value="Azerbaijan">Azerbaijan</option>		
				<option value="Bahamas">Bahamas</option>		
				<option value="Bahrain">Bahrain</option>		
				<option value="Bangladesh">Bangladesh</option>		
				<option value="Barbados">Barbados</option>		
				<option value="Belarus">Belarus</option>		
				<option value="Belgium">Belgium</option>		
				<option value="Belize">Belize</option>		
				<option value="Benin">Benin</option>		
				<option value="Bermuda">Bermuda</option>		
				<option value="Bhutan">Bhutan</option>		
				<option value="Bolivia">Bolivia</option>		
				<option value="Bosnia and Herzegovina">Bosnia and Herzegovina</option>		
				<option value="Botswana">Botswana</option>		
				<option value="Bouvet Island">Bouvet Island</option>		
				<option value="Brazil">Brazil</option>		
				<option value="British Indian Ocean Territory">British Indian Ocean Territory</option>		
				<option value="Brunei Darussalam">Brunei Darussalam</option>		
				<option value="Bulgaria">Bulgaria</option>		
				<option value="Burkina Faso">Burkina Faso</option>		
				<option value="Burundi">Burundi</option>		
				<option value="Cambodia">Cambodia</option>		
				<option value="Cameroon">Cameroon</option>		
				<option value="Canada">Canada</option>		
				<option value="Cape Verde">Cape Verde</option>		
				<option value="Cayman Islands">Cayman Islands</option>		
				<option value="Central African Republic">Central African Republic</option>		
				<option value="Chad">Chad</option>		
				<option value="Chile">Chile</option>		
				<option value="China">China</option>		
				<option value="Christmas Island">Christmas Island</option>		
				<option value="Cocos (Keeling) Islands">Cocos (Keeling) Islands</option>		
				<option value="Colombia">Colombia</option>		
				<option value="Comoros">Comoros</option>		
				<option value="Democratic Republic of the Congo">Democratic Republic of the Congo</option>		
				<option value="Republic of Congo">Republic of Congo</option>		
				<option value="Cook Islands">Cook Islands</option>		
				<option value="Costa Rica">Costa Rica</option>		
				<option value="Croatia (Hrvatska)">Croatia (Hrvatska)</option>		
				<option value="Cuba">Cuba</option>		
				<option value="Cyprus">Cyprus</option>		
				<option value="Czech Republic">Czech Republic</option>		
				<option value="Denmark">Denmark</option>		
				<option value="Djibouti">Djibouti</option>		
				<option value="Dominica">Dominica</option>		
				<option value="Dominican Republic">Dominican Republic</option>		
				<option value="East Timor">East Timor</option>		
				<option value="Ecuador">Ecuador</option>		
				<option value="Egypt">Egypt</option>		
				<option value="El Salvador">El Salvador</option>		
				<option value="Equatorial Guinea">Equatorial Guinea</option>		
				<option value="Eritrea">Eritrea</option>		
				<option value="Estonia">Estonia</option>		
				<option value="Ethiopia">Ethiopia</option>		
				<option value="Falkland Islands (Malvinas)">Falkland Islands (Malvinas)</option>		
				<option value="Faroe Islands">Faroe Islands</option>		
				<option value="Fiji">Fiji</option>		
				<option value="Finland">Finland</option>		
				<option value="France">France</option>		
				<option value="France, Metropolitan">France, Metropolitan</option>		
				<option value="French Guiana">French Guiana</option>		
				<option value="French Polynesia">French Polynesia</option>		
				<option value="French Southern Territories">French Southern Territories</option>		
				<option value="Gabon">Gabon</option>		
				<option value="Gambia">Gambia</option>		
				<option value="Georgia">Georgia</option>		
				<option value="Germany">Germany</option>		
				<option value="Ghana">Ghana</option>		
				<option value="Gibraltar">Gibraltar</option>		
				<option value="Guernsey">Guernsey</option>		
				<option value="Greece">Greece</option>		
				<option value="Greenland">Greenland</option>		
				<option value="Grenada">Grenada</option>		
				<option value="Guadeloupe">Guadeloupe</option>		
				<option value="Guam">Guam</option>		
				<option value="Guatemala">Guatemala</option>		
				<option value="Guinea">Guinea</option>		
				<option value="Guinea-Bissau">Guinea-Bissau</option>		
				<option value="Guyana">Guyana</option>		
				<option value="Haiti">Haiti</option>		
				<option value="Heard and Mc Donald Islands">Heard and Mc Donald Islands</option>		
				<option value="Honduras">Honduras</option>		
				<option value="Hong Kong">Hong Kong</option>		
				<option value="Hungary">Hungary</option>		
				<option value="Iceland">Iceland</option>		
				<option value="India">India</option>		
				<option value="Isle of Man">Isle of Man</option>		
				<option value="Indonesia">Indonesia</option>		
				<option value="Iran (Islamic Republic of)">Iran (Islamic Republic of)</option>		
				<option value="Iraq">Iraq</option>		
				<option value="Ireland">Ireland</option>		
				<option value="Israel">Israel</option>		
				<option value="Italy">Italy</option>		
				<option value="Ivory Coast">Ivory Coast</option>		
				<option value="Jersey">Jersey</option>		
				<option value="Jamaica">Jamaica</option>		
				<option value="Japan">Japan</option>		
				<option value="Jordan">Jordan</option>		
				<option value="Kazakhstan">Kazakhstan</option>		
				<option value="Kenya">Kenya</option>		
				<option value="Kiribati">Kiribati</option>		
				<option value="Korea, Democratic People's Republic of">Korea, Democratic People's Republic of</option>		
				<option value="Korea, Republic of">Korea, Republic of</option>		
				<option value="Kosovo">Kosovo</option>		
				<option value="Kuwait">Kuwait</option>		
				<option value="Kyrgyzstan">Kyrgyzstan</option>		
				<option value="Lao People's Democratic Republic">Lao People's Democratic Republic</option>		
				<option value="Latvia">Latvia</option>		
				<option value="Lebanon">Lebanon</option>		
				<option value="Lesotho">Lesotho</option>		
				<option value="Liberia">Liberia</option>		
				<option value="Libyan Arab Jamahiriya">Libyan Arab Jamahiriya</option>		
				<option value="Liechtenstein">Liechtenstein</option>		
				<option value="Lithuania">Lithuania</option>		
				<option value="Luxembourg">Luxembourg</option>		
				<option value="Macau">Macau</option>		
				<option value="North Macedonia">North Macedonia</option>		
				<option value="Madagascar">Madagascar</option>		
				<option value="Malawi">Malawi</option>		
				<option value="Malaysia">Malaysia</option>		
				<option value="Maldives">Maldives</option>		
				<option value="Mali">Mali</option>		
				<option value="Malta">Malta</option>		
				<option value="Marshall Islands">Marshall Islands</option>		
				<option value="Martinique">Martinique</option>		
				<option value="Mauritania">Mauritania</option>		
				<option value="Mauritius">Mauritius</option>		
				<option value="Mayotte">Mayotte</option>		
				<option value="Mexico">Mexico</option>		
				<option value="Micronesia, Federated States of">Micronesia, Federated States of</option>		
				<option value="Moldova, Republic of">Moldova, Republic of</option>		
				<option value="Monaco">Monaco</option>		
				<option value="Mongolia">Mongolia</option>		
				<option value="Montenegro">Montenegro</option>		
				<option value="Montserrat">Montserrat</option>		
				<option value="Morocco">Morocco</option>		
				<option value="Mozambique">Mozambique</option>		
				<option value="Myanmar">Myanmar</option>		
				<option value="Namibia">Namibia</option>		
				<option value="Nauru">Nauru</option>		
				<option value="Nepal">Nepal</option>		
				<option value="Netherlands">Netherlands</option>		
				<option value="Netherlands Antilles">Netherlands Antilles</option>		
				<option value="New Caledonia">New Caledonia</option>		
				<option value="New Zealand">New Zealand</option>		
				<option value="Nicaragua">Nicaragua</option>		
				<option value="Niger">Niger</option>		
				<option value="Nigeria">Nigeria</option>		
				<option value="Niue">Niue</option>		
				<option value="Norfolk Island">Norfolk Island</option>		
				<option value="Northern Mariana Islands">Northern Mariana Islands</option>		
				<option value="Norway">Norway</option>		
				<option value="Oman">Oman</option>		
				<option value="Pakistan">Pakistan</option>		
				<option value="Palau">Palau</option>		
				<option value="Palestine">Palestine</option>		
				<option value="Panama">Panama</option>		
				<option value="Papua New Guinea">Papua New Guinea</option>		
				<option value="Paraguay">Paraguay</option>		
				<option value="Peru">Peru</option>		
				<option value="Philippines">Philippines</option>		
				<option value="Pitcairn">Pitcairn</option>		
				<option value="Poland">Poland</option>		
				<option value="Portugal">Portugal</option>		
				<option value="Puerto Rico">Puerto Rico</option>		
				<option value="Qatar">Qatar</option>		
				<option value="Reunion">Reunion</option>		
				<option value="Romania">Romania</option>		
				<option value="Russian Federation">Russian Federation</option>		
				<option value="Rwanda">Rwanda</option>		
				<option value="Saint Kitts and Nevis">Saint Kitts and Nevis</option>		
				<option value="Saint Lucia">Saint Lucia</option>		
				<option value="Saint Vincent and the Grenadines">Saint Vincent and the Grenadines</option>		
				<option value="Samoa">Samoa</option>		
				<option value="San Marino">San Marino</option>		
				<option value="Sao Tome and Principe">Sao Tome and Principe</option>		
				<option value="Saudi Arabia">Saudi Arabia</option>		
				<option value="Senegal">Senegal</option>		
				<option value="Serbia">Serbia</option>		
				<option value="Seychelles">Seychelles</option>		
				<option value="Sierra Leone">Sierra Leone</option>		
				<option value="Singapore">Singapore</option>		
				<option value="Slovakia">Slovakia</option>		
				<option value="Slovenia">Slovenia</option>		
				<option value="Solomon Islands">Solomon Islands</option>		
				<option value="Somalia">Somalia</option>		
				<option value="South Africa">South Africa</option>		
				<option value="South Georgia South Sandwich Islands">South Georgia South Sandwich Islands</option>		
				<option value="South Sudan">South Sudan</option>		
				<option value="Spain">Spain</option>		
				<option value="Sri Lanka">Sri Lanka</option>		
				<option value="St. Helena">St. Helena</option>		
				<option value="St. Pierre and Miquelon">St. Pierre and Miquelon</option>		
				<option value="Sudan">Sudan</option>		
				<option value="Suriname">Suriname</option>		
				<option value="Svalbard and Jan Mayen Islands">Svalbard and Jan Mayen Islands</option>		
				<option value="Swaziland">Swaziland</option>		
				<option value="Sweden">Sweden</option>		
				<option value="Switzerland">Switzerland</option>		
				<option value="Syrian Arab Republic">Syrian Arab Republic</option>		
				<option value="Taiwan">Taiwan</option>		
				<option value="Tajikistan">Tajikistan</option>		
				<option value="Tanzania, United Republic of">Tanzania, United Republic of</option>		
				<option value="Thailand">Thailand</option>		
				<option value="Togo">Togo</option>		
				<option value="Tokelau">Tokelau</option>		
				<option value="Tonga">Tonga</option>		
				<option value="Trinidad and Tobago">Trinidad and Tobago</option>		
				<option value="Tunisia">Tunisia</option>		
				<option value="Turkey">Turkey</option>		
				<option value="Turkmenistan">Turkmenistan</option>		
				<option value="Turks and Caicos Islands">Turks and Caicos Islands</option>		
				<option value="Tuvalu">Tuvalu</option>		
				<option value="Uganda">Uganda</option>		
				<option value="Ukraine">Ukraine</option>		
				<option value="United Arab Emirates">United Arab Emirates</option>		
				<option value="United Kingdom">United Kingdom</option>		
				<option value="United States">United States</option>		
				<option value="United States minor outlying islands">United States minor outlying islands</option>		
				<option value="Uruguay">Uruguay</option>		
				<option value="Uzbekistan">Uzbekistan</option>		
				<option value="Vanuatu">Vanuatu</option>		
				<option value="Vatican City State">Vatican City State</option>		
				<option value="Venezuela">Venezuela</option>		
				<option value="Vietnam">Vietnam</option>		
				<option value="Virgin Islands (British)">Virgin Islands (British)</option>		
				<option value="Virgin Islands (U.S.)">Virgin Islands (U.S.)</option>		
				<option value="Wallis and Futuna Islands">Wallis and Futuna Islands</option>		
				<option value="Western Sahara">Western Sahara</option>		
				<option value="Yemen">Yemen</option>		
				<option value="Zambia">Zambia</option>		
				<option value="Zimbabwe">Zimbabwe</option>		
																</select>
												</div>
												<div class="col-lg-6">
													<input class="form-control" type="text" name="state" placeholder="State" required="" value="">
												</div>
												<div class="col-lg-6">
													<input class="form-control" type="text" name="city" placeholder="City" required="" value="">
												</div>
												<div class="col-lg-6">
													<input class="form-control" type="text" name="zip" placeholder="Postal Code" required="" value="">
												</div>
											</div>
										</div>
										<div class="row ">
											<div class="col-lg-12 mt-3">
													<input class="styled-checkbox" id="ship-diff-address" type="checkbox" value="value1">
													<label for="ship-diff-address">Ship to a Different Address?</label>
											</div>
										</div>
										<div class="ship-diff-addres-area d-none">
												<h5 class="title">
														Shipping Details
												</h5>
											<div class="row">
												<div class="col-lg-6">
													<input class="form-control ship_input" type="text" name="shipping_name" id="shippingFull_name" placeholder="Full Name">
														<input type="hidden" name="shipping_email" value="">
												</div>
												<div class="col-lg-6">
													<input class="form-control ship_input" type="text" name="shipping_phone" id="shipingPhone_number" placeholder="Phone Number">
												</div>
											</div>
											<div class="row">
												<div class="col-lg-6">
													<input class="form-control ship_input" type="text" name="shipping_address" id="shipping_address" placeholder="Address">
												</div>

												<div class="col-lg-6">
													<select class="form-control ship_input" name="shipping_country">
														<option value="">Select Country</option>
					<option value="Afghanistan">Afghanistan</option>		
				<option value="Albania">Albania</option>		
				<option value="Algeria">Algeria</option>		
				<option value="American Samoa">American Samoa</option>		
				<option value="Andorra">Andorra</option>		
				<option value="Angola">Angola</option>		
				<option value="Anguilla">Anguilla</option>		
				<option value="Antarctica">Antarctica</option>		
				<option value="Antigua and Barbuda">Antigua and Barbuda</option>		
				<option value="Argentina">Argentina</option>		
				<option value="Armenia">Armenia</option>		
				<option value="Aruba">Aruba</option>		
				<option value="Australia">Australia</option>		
				<option value="Austria">Austria</option>		
				<option value="Azerbaijan">Azerbaijan</option>		
				<option value="Bahamas">Bahamas</option>		
				<option value="Bahrain">Bahrain</option>		
				<option value="Bangladesh">Bangladesh</option>		
				<option value="Barbados">Barbados</option>		
				<option value="Belarus">Belarus</option>		
				<option value="Belgium">Belgium</option>		
				<option value="Belize">Belize</option>		
				<option value="Benin">Benin</option>		
				<option value="Bermuda">Bermuda</option>		
				<option value="Bhutan">Bhutan</option>		
				<option value="Bolivia">Bolivia</option>		
				<option value="Bosnia and Herzegovina">Bosnia and Herzegovina</option>		
				<option value="Botswana">Botswana</option>		
				<option value="Bouvet Island">Bouvet Island</option>		
				<option value="Brazil">Brazil</option>		
				<option value="British Indian Ocean Territory">British Indian Ocean Territory</option>		
				<option value="Brunei Darussalam">Brunei Darussalam</option>		
				<option value="Bulgaria">Bulgaria</option>		
				<option value="Burkina Faso">Burkina Faso</option>		
				<option value="Burundi">Burundi</option>		
				<option value="Cambodia">Cambodia</option>		
				<option value="Cameroon">Cameroon</option>		
				<option value="Canada">Canada</option>		
				<option value="Cape Verde">Cape Verde</option>		
				<option value="Cayman Islands">Cayman Islands</option>		
				<option value="Central African Republic">Central African Republic</option>		
				<option value="Chad">Chad</option>		
				<option value="Chile">Chile</option>		
				<option value="China">China</option>		
				<option value="Christmas Island">Christmas Island</option>		
				<option value="Cocos (Keeling) Islands">Cocos (Keeling) Islands</option>		
				<option value="Colombia">Colombia</option>		
				<option value="Comoros">Comoros</option>		
				<option value="Democratic Republic of the Congo">Democratic Republic of the Congo</option>		
				<option value="Republic of Congo">Republic of Congo</option>		
				<option value="Cook Islands">Cook Islands</option>		
				<option value="Costa Rica">Costa Rica</option>		
				<option value="Croatia (Hrvatska)">Croatia (Hrvatska)</option>		
				<option value="Cuba">Cuba</option>		
				<option value="Cyprus">Cyprus</option>		
				<option value="Czech Republic">Czech Republic</option>		
				<option value="Denmark">Denmark</option>		
				<option value="Djibouti">Djibouti</option>		
				<option value="Dominica">Dominica</option>		
				<option value="Dominican Republic">Dominican Republic</option>		
				<option value="East Timor">East Timor</option>		
				<option value="Ecuador">Ecuador</option>		
				<option value="Egypt">Egypt</option>		
				<option value="El Salvador">El Salvador</option>		
				<option value="Equatorial Guinea">Equatorial Guinea</option>		
				<option value="Eritrea">Eritrea</option>		
				<option value="Estonia">Estonia</option>		
				<option value="Ethiopia">Ethiopia</option>		
				<option value="Falkland Islands (Malvinas)">Falkland Islands (Malvinas)</option>		
				<option value="Faroe Islands">Faroe Islands</option>		
				<option value="Fiji">Fiji</option>		
				<option value="Finland">Finland</option>		
				<option value="France">France</option>		
				<option value="France, Metropolitan">France, Metropolitan</option>		
				<option value="French Guiana">French Guiana</option>		
				<option value="French Polynesia">French Polynesia</option>		
				<option value="French Southern Territories">French Southern Territories</option>		
				<option value="Gabon">Gabon</option>		
				<option value="Gambia">Gambia</option>		
				<option value="Georgia">Georgia</option>		
				<option value="Germany">Germany</option>		
				<option value="Ghana">Ghana</option>		
				<option value="Gibraltar">Gibraltar</option>		
				<option value="Guernsey">Guernsey</option>		
				<option value="Greece">Greece</option>		
				<option value="Greenland">Greenland</option>		
				<option value="Grenada">Grenada</option>		
				<option value="Guadeloupe">Guadeloupe</option>		
				<option value="Guam">Guam</option>		
				<option value="Guatemala">Guatemala</option>		
				<option value="Guinea">Guinea</option>		
				<option value="Guinea-Bissau">Guinea-Bissau</option>		
				<option value="Guyana">Guyana</option>		
				<option value="Haiti">Haiti</option>		
				<option value="Heard and Mc Donald Islands">Heard and Mc Donald Islands</option>		
				<option value="Honduras">Honduras</option>		
				<option value="Hong Kong">Hong Kong</option>		
				<option value="Hungary">Hungary</option>		
				<option value="Iceland">Iceland</option>		
				<option value="India">India</option>		
				<option value="Isle of Man">Isle of Man</option>		
				<option value="Indonesia">Indonesia</option>		
				<option value="Iran (Islamic Republic of)">Iran (Islamic Republic of)</option>		
				<option value="Iraq">Iraq</option>		
				<option value="Ireland">Ireland</option>		
				<option value="Israel">Israel</option>		
				<option value="Italy">Italy</option>		
				<option value="Ivory Coast">Ivory Coast</option>		
				<option value="Jersey">Jersey</option>		
				<option value="Jamaica">Jamaica</option>		
				<option value="Japan">Japan</option>		
				<option value="Jordan">Jordan</option>		
				<option value="Kazakhstan">Kazakhstan</option>		
				<option value="Kenya">Kenya</option>		
				<option value="Kiribati">Kiribati</option>		
				<option value="Korea, Democratic People's Republic of">Korea, Democratic People's Republic of</option>		
				<option value="Korea, Republic of">Korea, Republic of</option>		
				<option value="Kosovo">Kosovo</option>		
				<option value="Kuwait">Kuwait</option>		
				<option value="Kyrgyzstan">Kyrgyzstan</option>		
				<option value="Lao People's Democratic Republic">Lao People's Democratic Republic</option>		
				<option value="Latvia">Latvia</option>		
				<option value="Lebanon">Lebanon</option>		
				<option value="Lesotho">Lesotho</option>		
				<option value="Liberia">Liberia</option>		
				<option value="Libyan Arab Jamahiriya">Libyan Arab Jamahiriya</option>		
				<option value="Liechtenstein">Liechtenstein</option>		
				<option value="Lithuania">Lithuania</option>		
				<option value="Luxembourg">Luxembourg</option>		
				<option value="Macau">Macau</option>		
				<option value="North Macedonia">North Macedonia</option>		
				<option value="Madagascar">Madagascar</option>		
				<option value="Malawi">Malawi</option>		
				<option value="Malaysia">Malaysia</option>		
				<option value="Maldives">Maldives</option>		
				<option value="Mali">Mali</option>		
				<option value="Malta">Malta</option>		
				<option value="Marshall Islands">Marshall Islands</option>		
				<option value="Martinique">Martinique</option>		
				<option value="Mauritania">Mauritania</option>		
				<option value="Mauritius">Mauritius</option>		
				<option value="Mayotte">Mayotte</option>		
				<option value="Mexico">Mexico</option>		
				<option value="Micronesia, Federated States of">Micronesia, Federated States of</option>		
				<option value="Moldova, Republic of">Moldova, Republic of</option>		
				<option value="Monaco">Monaco</option>		
				<option value="Mongolia">Mongolia</option>		
				<option value="Montenegro">Montenegro</option>		
				<option value="Montserrat">Montserrat</option>		
				<option value="Morocco">Morocco</option>		
				<option value="Mozambique">Mozambique</option>		
				<option value="Myanmar">Myanmar</option>		
				<option value="Namibia">Namibia</option>		
				<option value="Nauru">Nauru</option>		
				<option value="Nepal">Nepal</option>		
				<option value="Netherlands">Netherlands</option>		
				<option value="Netherlands Antilles">Netherlands Antilles</option>		
				<option value="New Caledonia">New Caledonia</option>		
				<option value="New Zealand">New Zealand</option>		
				<option value="Nicaragua">Nicaragua</option>		
				<option value="Niger">Niger</option>		
				<option value="Nigeria">Nigeria</option>		
				<option value="Niue">Niue</option>		
				<option value="Norfolk Island">Norfolk Island</option>		
				<option value="Northern Mariana Islands">Northern Mariana Islands</option>		
				<option value="Norway">Norway</option>		
				<option value="Oman">Oman</option>		
				<option value="Pakistan">Pakistan</option>		
				<option value="Palau">Palau</option>		
				<option value="Palestine">Palestine</option>		
				<option value="Panama">Panama</option>		
				<option value="Papua New Guinea">Papua New Guinea</option>		
				<option value="Paraguay">Paraguay</option>		
				<option value="Peru">Peru</option>		
				<option value="Philippines">Philippines</option>		
				<option value="Pitcairn">Pitcairn</option>		
				<option value="Poland">Poland</option>		
				<option value="Portugal">Portugal</option>		
				<option value="Puerto Rico">Puerto Rico</option>		
				<option value="Qatar">Qatar</option>		
				<option value="Reunion">Reunion</option>		
				<option value="Romania">Romania</option>		
				<option value="Russian Federation">Russian Federation</option>		
				<option value="Rwanda">Rwanda</option>		
				<option value="Saint Kitts and Nevis">Saint Kitts and Nevis</option>		
				<option value="Saint Lucia">Saint Lucia</option>		
				<option value="Saint Vincent and the Grenadines">Saint Vincent and the Grenadines</option>		
				<option value="Samoa">Samoa</option>		
				<option value="San Marino">San Marino</option>		
				<option value="Sao Tome and Principe">Sao Tome and Principe</option>		
				<option value="Saudi Arabia">Saudi Arabia</option>		
				<option value="Senegal">Senegal</option>		
				<option value="Serbia">Serbia</option>		
				<option value="Seychelles">Seychelles</option>		
				<option value="Sierra Leone">Sierra Leone</option>		
				<option value="Singapore">Singapore</option>		
				<option value="Slovakia">Slovakia</option>		
				<option value="Slovenia">Slovenia</option>		
				<option value="Solomon Islands">Solomon Islands</option>		
				<option value="Somalia">Somalia</option>		
				<option value="South Africa">South Africa</option>		
				<option value="South Georgia South Sandwich Islands">South Georgia South Sandwich Islands</option>		
				<option value="South Sudan">South Sudan</option>		
				<option value="Spain">Spain</option>		
				<option value="Sri Lanka">Sri Lanka</option>		
				<option value="St. Helena">St. Helena</option>		
				<option value="St. Pierre and Miquelon">St. Pierre and Miquelon</option>		
				<option value="Sudan">Sudan</option>		
				<option value="Suriname">Suriname</option>		
				<option value="Svalbard and Jan Mayen Islands">Svalbard and Jan Mayen Islands</option>		
				<option value="Swaziland">Swaziland</option>		
				<option value="Sweden">Sweden</option>		
				<option value="Switzerland">Switzerland</option>		
				<option value="Syrian Arab Republic">Syrian Arab Republic</option>		
				<option value="Taiwan">Taiwan</option>		
				<option value="Tajikistan">Tajikistan</option>		
				<option value="Tanzania, United Republic of">Tanzania, United Republic of</option>		
				<option value="Thailand">Thailand</option>		
				<option value="Togo">Togo</option>		
				<option value="Tokelau">Tokelau</option>		
				<option value="Tonga">Tonga</option>		
				<option value="Trinidad and Tobago">Trinidad and Tobago</option>		
				<option value="Tunisia">Tunisia</option>		
				<option value="Turkey">Turkey</option>		
				<option value="Turkmenistan">Turkmenistan</option>		
				<option value="Turks and Caicos Islands">Turks and Caicos Islands</option>		
				<option value="Tuvalu">Tuvalu</option>		
				<option value="Uganda">Uganda</option>		
				<option value="Ukraine">Ukraine</option>		
				<option value="United Arab Emirates">United Arab Emirates</option>		
				<option value="United Kingdom">United Kingdom</option>		
				<option value="United States">United States</option>		
				<option value="United States minor outlying islands">United States minor outlying islands</option>		
				<option value="Uruguay">Uruguay</option>		
				<option value="Uzbekistan">Uzbekistan</option>		
				<option value="Vanuatu">Vanuatu</option>		
				<option value="Vatican City State">Vatican City State</option>		
				<option value="Venezuela">Venezuela</option>		
				<option value="Vietnam">Vietnam</option>		
				<option value="Virgin Islands (British)">Virgin Islands (British)</option>		
				<option value="Virgin Islands (U.S.)">Virgin Islands (U.S.)</option>		
				<option value="Wallis and Futuna Islands">Wallis and Futuna Islands</option>		
				<option value="Western Sahara">Western Sahara</option>		
				<option value="Yemen">Yemen</option>		
				<option value="Zambia">Zambia</option>		
				<option value="Zimbabwe">Zimbabwe</option>		
																</select>
												</div>
											</div>
											<div class="row">
												<div class="col-lg-6">
													<input class="form-control ship_input" type="text" name="shipping_city" id="shipping_city" placeholder="City">
												</div>
												<div class="col-lg-6">
													<input class="form-control ship_input" type="text" name="shipping_state" id="shipping_state" placeholder="State">
												</div>
											</div>

											<div class="row">
												<div class="col-lg-6">
													<input class="form-control ship_input" type="text" name="shipping_zip" id="shippingPostal_code" placeholder="Postal Code">
												</div>
											</div>


										</div>

										<div class="order-note mt-3">
											<div class="row">
												<div class="col-lg-12">
												<input type="text" id="Order_Note" class="form-control" name="order_notes" placeholder="Order Note (Optional)">
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-lg-12  mt-3">
												<div class="bottom-area paystack-area-btn">
													<button type="submit" class="mybtn1">Continue</button>
												</div>
												
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="tab-pane fade" id="pills-step2" role="tabpanel" aria-labelledby="pills-step2-tab">
								<div class="content-box">
									<div class="content">
										
										<div class="order-area">
																						<div class="order-item">
												<div class="product-img">
													<div class="d-flex">
														<img src=" https://royalscripts.com/product/kingcommerce/fashion/assets/images/products/1615885099MmFRCP7m.png" height="80" width="80" class="p-1">

													</div>
												</div>
												<div class="product-content">
													<p class="name"><a href="https://royalscripts.com/product/kingcommerce/fashion/item/kingcommerce-fashion-products-title-will-be-here-one-01-nb90982lap" target="_blank">KingCommerce Fashion Products Title Will be here one 01</a></p>
													<div class="unit-price">
														<h5 class="label">Price : </h5>
														<p>10$</p>
													</div>
																										<div class="unit-price">
														<h5 class="label">Size : </h5>
														<p>S</p>
													</div>
																																							<div class="unit-price">
														<h5 class="label">Color : </h5>
														<span id="color-bar" style="border: 10px solid #000000;"></span>
													</div>
																																							<div class="quantity">
														<h5 class="label">Quantity : </h5>
														<span class="qttotal">2 </span>
													</div>
													<div class="total-price">
														<h5 class="label">Total Price : </h5>
														<p>20$
														</p>
													</div>
												</div>
											</div>


																						<div class="order-item">
												<div class="product-img">
													<div class="d-flex">
														<img src=" https://royalscripts.com/product/kingcommerce/fashion/assets/images/products/1615885461QYTweVn0.png" height="80" width="80" class="p-1">

													</div>
												</div>
												<div class="product-content">
													<p class="name"><a href="https://royalscripts.com/product/kingcommerce/fashion/item/kingcommerce-fashion-products-title-will-be-here-one-01-nb90982lae" target="_blank">KingCommerce Fashion Products Title Will be here one 01</a></p>
													<div class="unit-price">
														<h5 class="label">Price : </h5>
														<p>25$</p>
													</div>
																										<div class="unit-price">
														<h5 class="label">Size : </h5>
														<p>XL</p>
													</div>
																																							<div class="unit-price">
														<h5 class="label">Color : </h5>
														<span id="color-bar" style="border: 10px solid #000000;"></span>
													</div>
																																							<div class="quantity">
														<h5 class="label">Quantity : </h5>
														<span class="qttotal">1 </span>
													</div>
													<div class="total-price">
														<h5 class="label">Total Price : </h5>
														<p>25$
														</p>
													</div>
												</div>
											</div>


											
										</div>



										<div class="row">
											<div class="col-lg-12 mt-3">
												<div class="bottom-area">
													<a href="javascript:;" id="step1-btn" class="mybtn1 mr-3">Back</a>
													<a href="javascript:;" id="step3-btn" class="mybtn1">Continue</a>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="tab-pane fade" id="pills-step3" role="tabpanel" aria-labelledby="pills-step3-tab">
								<div class="content-box">
									<div class="content">

											<div class="billing-info-area ">
															<h4 class="title">
																	Shipping Info
															</h4>
													<ul class="info-list">
														<li>
															<p id="shipping_user"></p>
														</li>
														<li>
															<p id="shipping_location"></p>
														</li>
														<li>
															<p id="shipping_phone"></p>
														</li>
														<li>
															<p id="shipping_email"></p>
														</li>
													</ul>
											</div>
											<div class="payment-information">
													<h4 class="title">
														Payment Info
													</h4>
												<div class="row">
													<div class="col-lg-12">
														<div class="nav flex-column" role="tablist" aria-orientation="vertical">
																													<a class="nav-link payment active" data-val="" data-show="no" data-form="https://royalscripts.com/product/kingcommerce/fashion/paypal-submit" data-href="https://royalscripts.com/product/kingcommerce/fashion/checkout/payment/paypal/0" id="v-pills-tab1-tab" data-toggle="pill" href="#v-pills-tab1" role="tab" aria-controls="v-pills-tab1" aria-selected="true">
																	<div class="icon">
																			<span class="radio"></span>
																	</div>
																<p>
																		PayPal Express

																	
																	<small>
																			Pay via your PayPal account.
																	</small>

																	
																</p>
															</a>
																																											<a class="nav-link payment" data-val="" data-show="yes" data-form="https://royalscripts.com/product/kingcommerce/fashion/stripe-submit" data-href="https://royalscripts.com/product/kingcommerce/fashion/checkout/payment/stripe/0" id="v-pills-tab2-tab" data-toggle="pill" href="#v-pills-tab2" role="tab" aria-controls="v-pills-tab2" aria-selected="false">
																	<div class="icon">
																			<span class="radio"></span>
																	</div>
																	<p>
																	Credit Card

																		
																		<small>
																			Pay via your Credit Card.
																		</small>

																		
																	</p>
															</a>
																																										 															<a class="nav-link payment" data-val="" data-show="no" data-form="https://royalscripts.com/product/kingcommerce/fashion/cashondelivery" data-href="https://royalscripts.com/product/kingcommerce/fashion/checkout/payment/cod/0" id="v-pills-tab3-tab" data-toggle="pill" href="#v-pills-tab3" role="tab" aria-controls="v-pills-tab3" aria-selected="false">
																	<div class="icon">
																			<span class="radio"></span>
																	</div>
																	<p>
																			Cash On Delivery

																		
																		<small>
																				Pay with cash upon delivery.
																		</small>

																		
																	</p>
															</a>
														 																																											<a class="nav-link payment" data-val="" data-show="no" data-form="https://royalscripts.com/product/kingcommerce/fashion/instamojo/submit" data-href="https://royalscripts.com/product/kingcommerce/fashion/checkout/payment/instamojo/0" id="v-pills-tab4-tab" data-toggle="pill" href="#v-pills-tab4" role="tab" aria-controls="v-pills-tab4" aria-selected="false">
																	<div class="icon">
																			<span class="radio"></span>
																	</div>
																	<p>
																			Instamojo

																		
																		<small>
																				Pay via your Instamojo account.
																		</small>

																		
																	</p>
															</a>
																																														<a class="nav-link payment" data-val="" data-show="no" data-form="https://royalscripts.com/product/kingcommerce/fashion/paytm-submit" data-href="https://royalscripts.com/product/kingcommerce/fashion/checkout/payment/paytm/0" id="v-pills-tab5-tab" data-toggle="pill" href="#v-pills-tab5" role="tab" aria-controls="v-pills-tab5" aria-selected="false">
																		<div class="icon">
																				<span class="radio"></span>
																		</div>
																		<p>
																				Paytm
	
																				
																			<small>
																					Pay via your Paytm account.
																			</small>
	
																				
																		</p>
																</a>
																																																	<a class="nav-link payment" data-val="" data-show="no" data-form="https://royalscripts.com/product/kingcommerce/fashion/razorpay-submit" data-href="https://royalscripts.com/product/kingcommerce/fashion/checkout/payment/razorpay/0" id="v-pills-tab6-tab" data-toggle="pill" href="#v-pills-tab6" role="tab" aria-controls="v-pills-tab6" aria-selected="false">
																			<div class="icon">
																					<span class="radio"></span>
																			</div>
																			<p>
																					
																				Razorpay
		
																						
																				<small>
																						Pay via your Razorpay account.
																				</small>
		
																						
																			</p>
																	</a>
																																
															<a class="nav-link payment" data-val="paystack" data-show="no" data-form="https://royalscripts.com/product/kingcommerce/fashion/paystack/submit" data-href="https://royalscripts.com/product/kingcommerce/fashion/checkout/payment/paystack/0" id="v-pills-tab7-tab" data-toggle="pill" href="#v-pills-tab7" role="tab" aria-controls="v-pills-tab7" aria-selected="false">
																	<div class="icon">
																			<span class="radio"></span>
																	</div>
																	<p>
																			Paystack

																		
																		<small>
																				Pay via your Paystack account.
																		</small>

																																			</p>
															</a>

															

																														<a class="nav-link payment" data-val="" data-show="no" data-form="https://royalscripts.com/product/kingcommerce/fashion/molly/submit" data-href="https://royalscripts.com/product/kingcommerce/fashion/checkout/payment/molly/0" id="v-pills-tab8-tab" data-toggle="pill" href="#v-pills-tab8" role="tab" aria-controls="v-pills-tab8" aria-selected="false">
																	<div class="icon">
																			<span class="radio"></span>
																	</div>
																	<p>
																			Mollie Payment

																		
																		<small>
																				Pay with Molly Payment.
																		</small>

																																			</p>
															</a>

															

																														<a class="nav-link payment" data-val="" data-show="yes" data-form="https://royalscripts.com/product/kingcommerce/fashion/authorize-submit" data-href="https://royalscripts.com/product/kingcommerce/fashion/checkout/payment/authorize/0" id="v-pills-tab9-tab" data-toggle="pill" href="#v-pills-tab9" role="tab" aria-controls="v-pills-tab9" aria-selected="false">
																	<div class="icon">
																			<span class="radio"></span>
																	</div>
																	<p>
																			Authorize.Net

																		
																		<small>
																				Pay Via Authorize.Net
																		</small>

																																			</p>
															</a>

															

																														<a class="nav-link payment" data-val="" data-show="no" data-form="https://royalscripts.com/product/kingcommerce/fashion/checkout/mercadopago/submit" data-href="https://royalscripts.com/product/kingcommerce/fashion/checkout/payment/mercadopago/0" id="v-pills-tab10-tab" data-toggle="pill" href="#v-pills-tab10" role="tab" aria-controls="v-pills-tab10" aria-selected="false">
																	<div class="icon">
																			<span class="radio"></span>
																	</div>
																	<p>
																		MercadoPago



																		<small>
																			Pay Via MarcadoPago
																		</small>


																	</p>
															</a>

															
															
															
															
															<a class="nav-link payment" data-val="ssl" data-show="no" data-form="https://royalscripts.com/product/kingcommerce/fashion/ssl/submit" data-href="https://royalscripts.com/product/kingcommerce/fashion/checkout/payment/ssl/0" id="v-pills-tab13-tab" data-toggle="pill" href="#v-pills-tab13" role="tab" aria-controls="v-pills-tab13" aria-selected="false">
																	<div class="icon">
																			<span class="radio"></span>
																	</div>
																	<p>
																		SSLCommerz

																		
																		<small>
																				Pay Via SSLCommerz
																		</small>

																																			</p>
															</a>

															

															


													
													
															<a class="nav-link payment" data-val="" data-show="yes" data-form="https://royalscripts.com/product/kingcommerce/fashion/gateway" data-href="https://royalscripts.com/product/kingcommerce/fashion/checkout/payment/other/46" id="v-pills-tab46-tab" data-toggle="pill" href="#v-pills-tab46" role="tab" aria-controls="v-pills-tab46" aria-selected="false">
																	<div class="icon">
																			<span class="radio"></span>
																	</div>
																	<p>
																			Manual Payment

																		
																		<small>
																				Pay via Manual Payment.
																		</small>

																		
																	</p>
															</a>

													
													
														</div>
													</div>
													<div class="col-lg-12">
													  <div class="pay-area d-none">
														<div class="tab-content" id="v-pills-tabContent">
																														<div class="tab-pane fade active show" id="v-pills-tab1" role="tabpanel" aria-labelledby="v-pills-tab1-tab"><input type="hidden" name="method" value="Paypal">
                                <input type="hidden" name="cmd" value="_xclick">
                                <input type="hidden" name="no_note" value="1">
                                <input type="hidden" name="lc" value="UK">
                                <input type="hidden" name="currency_code" value="USD">
                                <input type="hidden" name="bn" value="PP-BuyNowBF:btn_buynow_LG.gif:NonHostedGuest">

















</div>
																																													<div class="tab-pane fade" id="v-pills-tab2" role="tabpanel" aria-labelledby="v-pills-tab2-tab">
															</div>
																																																												<div class="tab-pane fade" id="v-pills-tab3" role="tabpanel" aria-labelledby="v-pills-tab3-tab">
															</div>
																																																													<div class="tab-pane fade" id="v-pills-tab4" role="tabpanel" aria-labelledby="v-pills-tab4-tab">
																</div>
																																														<div class="tab-pane fade" id="v-pills-tab5" role="tabpanel" aria-labelledby="v-pills-tab5-tab">
																</div>
																																														<div class="tab-pane fade" id="v-pills-tab6" role="tabpanel" aria-labelledby="v-pills-tab6-tab">
																</div>
																																														<div class="tab-pane fade" id="v-pills-tab7" role="tabpanel" aria-labelledby="v-pills-tab7-tab">
																</div>
																																														<div class="tab-pane fade" id="v-pills-tab8" role="tabpanel" aria-labelledby="v-pills-tab8-tab">
																</div>
																																														<div class="tab-pane fade" id="v-pills-tab9" role="tabpanel" aria-labelledby="v-pills-tab9-tab">
																</div>
																																														<div class="tab-pane fade" id="v-pills-tab10" role="tabpanel" aria-labelledby="v-pills-tab10-tab">
																</div>
																																																																												<div class="tab-pane fade" id="v-pills-tab13" role="tabpanel" aria-labelledby="v-pills-tab13-tab">
																</div>
																														

																											
															<div class="tab-pane fade" id="v-pills-tab46" role="tabpanel" aria-labelledby="v-pills-tab46-tab">

															</div>

																
																									
													</div>
														</div>
													</div>
												</div>
											</div>
											
										<div class="row">
											<div class="col-lg-12 mt-3">
												<div class="bottom-area">
													<a href="javascript:;" id="step2-btn" class="mybtn1 mr-3">Back</a>
													<button type="submit" id="final-btn" class="mybtn1">Continue</button>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>


                            <input type="hidden" id="shipping-cost" name="shipping_cost" value="0">
							<input type="hidden" id="packing-cost" name="packing_cost" value="0">
															<input type="hidden" id="shipping-title" name="shipping_title" value="Free Shipping">
																						<input type="hidden" id="packing-title" name="packing_title" value="Default Packaging">
							                            <input type="hidden" name="dp" value="0">
                            <input type="hidden" name="tax" value="0">
                            <input type="hidden" name="totalQty" value="3">
                            <input type="hidden" name="vendor_shipping_id" value="0">
                            <input type="hidden" name="vendor_packing_id" value="0">


							                            	<input type="hidden" name="total" id="grandtotal" value="45">
                            	<input type="hidden" id="tgrandtotal" value="45">
							                            <input type="hidden" id="wallet-price" name="wallet_price" value="0">

                            <input type="hidden" name="coupon_code" id="coupon_code" value="">
                            <input type="hidden" name="coupon_discount" id="coupon_discount" value="">
                            <input type="hidden" name="coupon_id" id="coupon_id" value="">
                            <input type="hidden" name="user_id" id="user_id" value="">
							


</form>

				</div>

								<div class="col-lg-4">
					<div class="right-area">
						<div class="order-box">
						<h4 class="title">PRICE DETAILS</h4>
						<ul class="order-list">
							<li>
							<p>
								Total MRP
							</p>
							<p>
								<b class="cart-total">45$</b>
							</p>
							</li>

							



						 


							<li class="discount-bar d-none">
							<p>
								Discount <span class="dpercent"></span>
							</p>
							<p>
								<b id="discount">$</b>
							</p>
							</li>


						



						</ul>

		            <div class="total-price">
		              <p>
		                Total
		              </p>
		              <p>

													<span id="total-cost">45$</span>
						
		              </p>
		            </div>


						<div class="cupon-box">

							<div id="coupon-link">
							<img src="https://royalscripts.com/product/kingcommerce/fashion/assets/front/images/tag.png">
							Have a promotion code?
							</div>

						    <form id="check-coupon-form" class="coupon">
						        <input type="text" placeholder="Coupon Code" id="code" required="" autocomplete="off">
						        <button type="submit">Apply</button>
						    </form>


						</div>

						
						
						<div class="packeging-area">
								<h4 class="title">Shipping Method</h4>

													
								<div class="radio-design">
										<input type="radio" class="shipping" id="free-shepping1" name="shipping" value="0" checked=""> 
										<span class="checkmark"></span>
										<label for="free-shepping1"> 
												Free Shipping
																								<small>(10 - 12 days)</small>
										</label>
								</div>

													
								<div class="radio-design">
										<input type="radio" class="shipping" id="free-shepping2" name="shipping" value="10"> 
										<span class="checkmark"></span>
										<label for="free-shepping2"> 
												Express Shipping
																								+ $10
																								<small>(5 - 6 days)</small>
										</label>
								</div>

									

						</div>
						

						
						<div class="packeging-area">
								<h4 class="title">Packaging</h4>

								

								<div class="radio-design">
										<input type="radio" class="packing" id="free-package1" name="packeging" value="0" checked=""> 
										<span class="checkmark"></span>
										<label for="free-package1"> 
												Default Packaging
																								<small>Default packaging by store</small>
										</label>
								</div>

								

								<div class="radio-design">
										<input type="radio" class="packing" id="free-package2" name="packeging" value="15"> 
										<span class="checkmark"></span>
										<label for="free-package2"> 
												Gift Packaging
																								+ $15
																								<small>Exclusive Gift packaging</small>
										</label>
								</div>

								

						</div>
						

						
						<div class="final-price">
							<span>Final Price :</span>
													<span id="final-cost">45$</span>
						


						</div>
						

						
				

					<div class="wallet-price d-none">
							<span>Wallet Amount:</span>

							 
								<span id="wallet-cost"></span>
												</div>

					

				

						</div>
					</div>
				</div>
							</div>
		</div>
	</section>
</x-master>