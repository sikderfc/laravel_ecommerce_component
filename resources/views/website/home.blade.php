
<x-master>
<style type="text/css">
	@media only screen and (max-width: 767px) {


		.subtitle9 {
			font-size: 12px !important;
		}

		.title9 {
			font-size: 30px !important;
		}

		.details9 {
			font-size: 12px !important;
		}


		.subtitle10 {
			font-size: 12px !important;
		}

		.title10 {
			font-size: 30px !important;
		}

		.details10 {
			font-size: 12px !important;
		}

	}

	@media only screen and (min-width: 768px) and (max-width: 991px) {


		.subtitle9 {
			font-size: 16.8px !important;
		}

		.title9 {
			font-size: 42px !important;
		}

		.details9 {
			font-size: 11.2px !important;
		}


		.subtitle10 {
			font-size: 16.8px !important;
		}

		.title10 {
			font-size: 42px !important;
		}

		.details10 {
			font-size: 11.2px !important;
		}

	}
</style>



<style type="text/css">
	@media only screen and (max-width: 767px) {


		.subtitle9 {
			font-size: 12px !important;
		}

		.title9 {
			font-size: 30px !important;
		}

		.details9 {
			font-size: 12px !important;
		}


		.subtitle10 {
			font-size: 12px !important;
		}

		.title10 {
			font-size: 30px !important;
		}

		.details10 {
			font-size: 12px !important;
		}

	}

	@media only screen and (min-width: 768px) and (max-width: 991px) {


		.subtitle9 {
			font-size: 16.8px !important;
		}

		.title9 {
			font-size: 42px !important;
		}

		.details9 {
			font-size: 11.2px !important;
		}


		.subtitle10 {
			font-size: 16.8px !important;
		}

		.title10 {
			font-size: 42px !important;
		}

		.details10 {
			font-size: 11.2px !important;
		}

	}
</style>
<!-- Hero Area Start -->
<section class="hero-area">
	<div class="container">
		<div class="row">
			<div class="col-lg-3 decrease-padding">
				<div class="featured-link-box">
					<h4 class="title">
						Featured Links
					</h4>
					<ul class="link-list">
						<li>
							<a href="{{url('product_view_by_id')}}" target="_blank"><img
									src="{{ asset('assets/website/images/featuredlink') }}/1615876694shorts.png" alt="">Shorts</a>
						</li>
						<li>
							<a href="category/T-shirt-and-Tops.html" target="_blank"><img
									src="{{ asset('assets/website/images/featuredlink') }}/1615876659shirt.png" alt="">T-shirt &amp;
								Tops</a>
						</li>
						<li>
							<a href="category/Coats-and-Jackets.html" target="_blank"><img
									src="{{ asset('assets/website/images/featuredlink') }}/1615876636jacket.png" alt="">Coats &amp;
								Jackets</a>
						</li>
						<li>
							<a href="category/Shirts.html" target="_blank"><img
									src="{{ asset('assets/website/images/featuredlink') }}/1615876609cloth.png" alt="">Shirts</a>
						</li>
						<li>
							<a href="category/Jeans.html" target="_blank"><img
									src="{{ asset('assets/website/images/featuredlink') }}/1615876584jeans.png" alt="">Jeans</a>
						</li>
						<li>
							<a href="category/Jewellery.html" target="_blank"><img
									src="{{ asset('assets/website/images/featuredlink') }}/1615876559gems.png" alt="">Jewellery</a>
						</li>
						<li>
							<a href="category/Shoes.html" target="_blank"><img
									src="{{ asset('assets/website/images/featuredlink') }}/1615876430high-heels.png" alt="">Shoes</a>
						</li>
						<li>
							<a href="category/Bags.html" target="_blank"><img
									src="{{ asset('assets/website/images/featuredlink') }}/1615876394bag.png" alt="">Bags</a>
						</li>
						<li>
							<a href="category/Beauty.html" target="_blank"><img
									src="{{ asset('assets/website/images/featuredlink') }}/1615876369makeover.png" alt="">Beauty</a>
						</li>
						<li>
							<a href="category/Accessories.html" target="_blank"><img
									src="{{ asset('assets/website/images/featuredlink') }}/1615876342eyeglasses.png" alt="">Accessories</a>
						</li>
					</ul>
				</div>
			</div>
			<div class="col-lg-9 decrease-padding">
				<div class="hero-area-slider">
					<div class="slide-progress"></div>
					<div class="intro-carousel">
						<div class="intro-content slide-three"
							style="background-image: url({{ asset('assets/website/images/sliders') }}/1.jpg)">
							<div class="slider-content">
								<!-- layer 1 -->
								<div class="layer-1">
									<h4 style="font-size: 24px; color: #000000" class="subtitle subtitle9"
										data-animation="animated slideInUp">World Fashion</h4>
									<h2 style="font-size: 60px; color: #e91e63" class="title title9"
										data-animation="animated slideInDown">Up to 40% Off</h2>
								</div>
								<!-- layer 2 -->
								<div class="layer-2">
									<p style="font-size: 16px; color: #000000" class="text text9"
										data-animation="animated slideInDown">Highlight your personality and look
										with these fabulous and exquisite fashion.</p>
								</div>
								<!-- layer 3 -->
								<div class="layer-3">
									<a href="category/Coats-and-Jackets.html" target="_blank"
										class="mybtn1"><span>Shop Now <i
												class="fas fa-chevron-right"></i></span></a>
								</div>
							</div>
						</div>
						<div class="intro-content slide-one"
							style="background-image: url({{ asset('assets/website/images/sliders') }}/2.jpg">
							<div class="slider-content">
								<!-- layer 1 -->
								<div class="layer-1">
									<h4 style="font-size: 24px; color: #000000" class="subtitle subtitle10"
										data-animation="animated slideInUp">World Fashion</h4>
									<h2 style="font-size: 60px; color: #e91e63" class="title title10"
										data-animation="animated slideInDown">Up to 40% Off</h2>
								</div>
								<!-- layer 2 -->
								<div class="layer-2">
									<p style="font-size: 16px; color: #000000" class="text text10"
										data-animation="animated slideInLeft">Highlight your personality and look
										with these fabulous and exquisite fashion.</p>
								</div>
								<!-- layer 3 -->
								<div class="layer-3">
									<a href="category/Accessories.html" target="_blank" class="mybtn1"><span>Shop
											Now <i class="fas fa-chevron-right"></i></span></a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- Hero Area End -->




<section class="slider_bottom_banner">
	<div class="container">
		<div class="row">
			<div class="col-lg-3 col-6">
				<a href="category/Accessories.html" target="_blank" class="banner-effect">
					<img src="{{ asset('assets/website/images/featuredbanner') }}/1571287040feature1.jpg" alt="">
				</a>
			</div>
			<div class="col-lg-3 col-6">
				<a href="category/Accessories.html" target="_blank" class="banner-effect">
					<img src="{{ asset('assets/website/images/featuredbanner') }}/1571287047feature2.jpg" alt="">
				</a>
			</div>
			<div class="col-lg-3 col-6">
				<a href="category/Accessories.html" target="_blank" class="banner-effect">
					<img src="{{ asset('assets/website/images/featuredbanner') }}/1571287054feature3.jpg" alt="">
				</a>
			</div>
			<div class="col-lg-3 col-6">
				<a href="category/Accessories.html" target="_blank" class="banner-effect">
					<img src="{{ asset('assets/website/images/featuredbanner') }}/1571287106feature4.jpg" alt="">
				</a>
			</div>
		</div>


	</div>
	</div>
</section>








<section class="info-area">
	<div class="container">


		<div class="row">

			<div class="col-lg-12 p-0">
				<div class="info-big-box">
					<div class="row">
						<div class="col-6 col-xl-3 p-0">
							<div class="info-box">
								<div class="icon">
									<img src="{{ asset('assets/website/images/services') }}/1571288944s1.png">
								</div>
								<div class="info">
									<div class="details">
										<h4 class="title">FREE SHIPPING</h4>
										<p class="text">
											Free Shipping All Order
										</p>
									</div>
								</div>
							</div>
						</div>
						<div class="col-6 col-xl-3 p-0">
							<div class="info-box">
								<div class="icon">
									<img src="{{ asset('assets/website/images/services') }}/1571288950s2.png">
								</div>
								<div class="info">
									<div class="details">
										<h4 class="title">PAYMENT METHOD</h4>
										<p class="text">
											Secure Payment
										</p>
									</div>
								</div>
							</div>
						</div>
						<div class="col-6 col-xl-3 p-0">
							<div class="info-box">
								<div class="icon">
									<img src="{{ asset('assets/website/images/services') }}/1571288955s3.png">
								</div>
								<div class="info">
									<div class="details">
										<h4 class="title">30 DAY RETURNS</h4>
										<p class="text">
											30-Day Return Policy
										</p>
									</div>
								</div>
							</div>
						</div>
						<div class="col-6 col-xl-3 p-0">
							<div class="info-box">
								<div class="icon">
									<img src="{{ asset('assets/website/images/services') }}/1571288959s4.png">
								</div>
								<div class="info">
									<div class="details">
										<h4 class="title">HELP CENTER</h4>
										<p class="text">
											24/7 Support System
										</p>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

		</div>


	</div>
</section>







<!-- Trending Item Area Start -->
<section class="trending">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 remove-padding">
				<div class="section-top">
					<h2 class="section-title">
						Featured
					</h2>

				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-12 remove-padding">
				<div class="trending-item-slider">
					<a href="{{url('product_view_by_id')}}"
						class="item">
						<div class="item-img">
						

							<img class="img-fluid" src="{{ asset('assets/website/images/thumbnails') }}/1633254748UAAKWWO0.jpg" alt="">

						</div>
						<div class="info">
							<h5 class="name">KingCommerce Fashion Products Title Will be here one 01</h5>
							<h4 class="price">20$
								<del><small>25$</small></del>
							</h4>
							<div class="stars">
								<div class="ratings">
									<div class="empty-stars"></div>
									<div class="full-stars" style="width:0%"></div>
								</div>
							</div>
							<div class="item-cart-area">

								<ul class="item-cart-options">
									<li>

										<span href="javascript:;" rel-toggle="tooltip" title="Add To Wishlist"
											data-toggle="modal" id="wish-btn" data-target="#comment-log-reg"
											data-placement="top">
											<i class="icofont-heart-alt"></i>
										</span>

									</li>
									<li>
										<span class="quick-view" rel-toggle="tooltip" title="Quick View"
											href="javascript:;"
											data-href="https://royalscripts.com/product/kingcommerce/fashion/item/quick/view/195"
											data-toggle="modal" data-target="#quickview" data-placement="top">
											<i class="fas fa-shopping-basket"></i>
										</span>
									</li>
									<li>
										<span href="javascript:;" class="add-to-compare"
											data-href="https://royalscripts.com/product/kingcommerce/fashion/item/compare/add/195"
											data-toggle="tooltip" data-placement="top" title="Compare">
											<i class="icofont-exchange"></i>
										</span>
									</li>
								</ul>
							</div>
						</div>
					</a>
					<a href="item/kingcommerce-fashion-products-title-will-be-here-one-01-nb90982lao.html"
						class="item">
						<div class="item-img">
							<img class="img-fluid" src="{{ asset('assets/website/images/thumbnails') }}/1615885140ReBGEOFx.jpg" alt="">
						</div>
						<div class="info">
							<h5 class="name">KingCommerce Fashion Products Title Will be here one 01</h5>
							<h4 class="price">12$
								<del><small>14$</small></del>
							</h4>
							<div class="stars">
								<div class="ratings">
									<div class="empty-stars"></div>
									<div class="full-stars" style="width:0%"></div>
								</div>
							</div>
							<div class="item-cart-area">

								<ul class="item-cart-options">
									<li>

										<span href="javascript:;" rel-toggle="tooltip" title="Add To Wishlist"
											data-toggle="modal" id="wish-btn" data-target="#comment-log-reg"
											data-placement="top">
											<i class="icofont-heart-alt"></i>
										</span>

									</li>
									<li>
										<span class="quick-view" rel-toggle="tooltip" title="Quick View"
											href="javascript:;"
											data-href="https://royalscripts.com/product/kingcommerce/fashion/item/quick/view/192"
											data-toggle="modal" data-target="#quickview" data-placement="top">
											<i class="fas fa-shopping-basket"></i>
										</span>
									</li>
									<li>
										<span href="javascript:;" class="add-to-compare"
											data-href="https://royalscripts.com/product/kingcommerce/fashion/item/compare/add/192"
											data-toggle="tooltip" data-placement="top" title="Compare">
											<i class="icofont-exchange"></i>
										</span>
									</li>
								</ul>
							</div>
						</div>
					</a>
					<a href="item/kingcommerce-fashion-products-title-will-be-here-one-01-nb90982lai.html"
						class="item">
						<div class="item-img">
							<img class="img-fluid" src="{{ asset('assets/website/images/thumbnails') }}/1615885173VC2dmnl3.jpg" alt="">
						</div>
						<div class="info">
							<h5 class="name">KingCommerce Fashion Products Title Will be here one 01</h5>
							<h4 class="price">30$
								<del><small>35$</small></del>
							</h4>
							<div class="stars">
								<div class="ratings">
									<div class="empty-stars"></div>
									<div class="full-stars" style="width:0%"></div>
								</div>
							</div>
							<div class="item-cart-area">

								<ul class="item-cart-options">
									<li>

										<span href="javascript:;" rel-toggle="tooltip" title="Add To Wishlist"
											data-toggle="modal" id="wish-btn" data-target="#comment-log-reg"
											data-placement="top">
											<i class="icofont-heart-alt"></i>
										</span>

									</li>
									<li>
										<span class="quick-view" rel-toggle="tooltip" title="Quick View"
											href="javascript:;"
											data-href="https://royalscripts.com/product/kingcommerce/fashion/item/quick/view/191"
											data-toggle="modal" data-target="#quickview" data-placement="top">
											<i class="fas fa-shopping-basket"></i>
										</span>
									</li>
									<li>
										<span href="javascript:;" class="add-to-compare"
											data-href="https://royalscripts.com/product/kingcommerce/fashion/item/compare/add/191"
											data-toggle="tooltip" data-placement="top" title="Compare">
											<i class="icofont-exchange"></i>
										</span>
									</li>
								</ul>
							</div>
						</div>
					</a>
					<a href="item/kingcommerce-fashion-products-title-will-be-here-one-01-nb90982lau.html"
						class="item">
						<div class="item-img">
							<img class="img-fluid" src="{{ asset('assets/website/images/thumbnails') }}/1615885288vGFHcIxR.jpg" alt="">
						</div>
						<div class="info">
							<h5 class="name">KingCommerce Fashion Products Title Will be here one 01</h5>
							<h4 class="price">10$
								<del><small>15$</small></del>
							</h4>
							<div class="stars">
								<div class="ratings">
									<div class="empty-stars"></div>
									<div class="full-stars" style="width:0%"></div>
								</div>
							</div>
							<div class="item-cart-area">

								<ul class="item-cart-options">
									<li>

										<span href="javascript:;" rel-toggle="tooltip" title="Add To Wishlist"
											data-toggle="modal" id="wish-btn" data-target="#comment-log-reg"
											data-placement="top">
											<i class="icofont-heart-alt"></i>
										</span>

									</li>
									<li>
										<span class="quick-view" rel-toggle="tooltip" title="Quick View"
											href="javascript:;"
											data-href="https://royalscripts.com/product/kingcommerce/fashion/item/quick/view/190"
											data-toggle="modal" data-target="#quickview" data-placement="top">
											<i class="fas fa-shopping-basket"></i>
										</span>
									</li>
									<li>
										<span href="javascript:;" class="add-to-compare"
											data-href="https://royalscripts.com/product/kingcommerce/fashion/item/compare/add/190"
											data-toggle="tooltip" data-placement="top" title="Compare">
											<i class="icofont-exchange"></i>
										</span>
									</li>
								</ul>
							</div>
						</div>
					</a>
					<a href="item/kingcommerce-fashion-products-title-will-be-here-one-01-nb90982lay.html"
						class="item">
						<div class="item-img">
							<img class="img-fluid" src="{{ asset('assets/website/images/thumbnails') }}/16158853269IywRAmW.jpg" alt="">
						</div>
						<div class="info">
							<h5 class="name">KingCommerce Fashion Products Title Will be here one 01</h5>
							<h4 class="price">40$
								<del><small>45$</small></del>
							</h4>
							<div class="stars">
								<div class="ratings">
									<div class="empty-stars"></div>
									<div class="full-stars" style="width:0%"></div>
								</div>
							</div>
							<div class="item-cart-area">

								<ul class="item-cart-options">
									<li>

										<span href="javascript:;" rel-toggle="tooltip" title="Add To Wishlist"
											data-toggle="modal" id="wish-btn" data-target="#comment-log-reg"
											data-placement="top">
											<i class="icofont-heart-alt"></i>
										</span>

									</li>
									<li>
										<span class="quick-view" rel-toggle="tooltip" title="Quick View"
											href="javascript:;"
											data-href="https://royalscripts.com/product/kingcommerce/fashion/item/quick/view/189"
											data-toggle="modal" data-target="#quickview" data-placement="top">
											<i class="fas fa-shopping-basket"></i>
										</span>
									</li>
									<li>
										<span href="javascript:;" class="add-to-compare"
											data-href="https://royalscripts.com/product/kingcommerce/fashion/item/compare/add/189"
											data-toggle="tooltip" data-placement="top" title="Compare">
											<i class="icofont-exchange"></i>
										</span>
									</li>
								</ul>
							</div>
						</div>
					</a>
					<a href="item/kingcommerce-fashion-products-title-will-be-here-one-01-nb90982lat.html"
						class="item">
						<div class="item-img">
							<img class="img-fluid" src="{{ asset('assets/website/images/thumbnails') }}/1615885365tPtXqQ9W.jpg" alt="">
						</div>
						<div class="info">
							<h5 class="name">KingCommerce Fashion Products Title Will be here one 01</h5>
							<h4 class="price">50$
								<del><small>55$</small></del>
							</h4>
							<div class="stars">
								<div class="ratings">
									<div class="empty-stars"></div>
									<div class="full-stars" style="width:0%"></div>
								</div>
							</div>
							<div class="item-cart-area">

								<ul class="item-cart-options">
									<li>

										<span href="javascript:;" rel-toggle="tooltip" title="Add To Wishlist"
											data-toggle="modal" id="wish-btn" data-target="#comment-log-reg"
											data-placement="top">
											<i class="icofont-heart-alt"></i>
										</span>

									</li>
									<li>
										<span class="quick-view" rel-toggle="tooltip" title="Quick View"
											href="javascript:;"
											data-href="https://royalscripts.com/product/kingcommerce/fashion/item/quick/view/188"
											data-toggle="modal" data-target="#quickview" data-placement="top">
											<i class="fas fa-shopping-basket"></i>
										</span>
									</li>
									<li>
										<span href="javascript:;" class="add-to-compare"
											data-href="https://royalscripts.com/product/kingcommerce/fashion/item/compare/add/188"
											data-toggle="tooltip" data-placement="top" title="Compare">
											<i class="icofont-exchange"></i>
										</span>
									</li>
								</ul>
							</div>
						</div>
					</a>
					<a href="item/kingcommerce-fashion-products-title-will-be-here-one-01-nb90982lar.html"
						class="item">
						<div class="item-img">
							<img class="img-fluid" src="{{ asset('assets/website/images/thumbnails') }}/1615885428lFqCDMth.jpg" alt="">
						</div>
						<div class="info">
							<h5 class="name">KingCommerce Fashion Products Title Will be here one 01</h5>
							<h4 class="price">15$
								<del><small>20$</small></del>
							</h4>
							<div class="stars">
								<div class="ratings">
									<div class="empty-stars"></div>
									<div class="full-stars" style="width:0%"></div>
								</div>
							</div>
							<div class="item-cart-area">

								<ul class="item-cart-options">
									<li>

										<span href="javascript:;" rel-toggle="tooltip" title="Add To Wishlist"
											data-toggle="modal" id="wish-btn" data-target="#comment-log-reg"
											data-placement="top">
											<i class="icofont-heart-alt"></i>
										</span>

									</li>
									<li>
										<span class="quick-view" rel-toggle="tooltip" title="Quick View"
											href="javascript:;"
											data-href="https://royalscripts.com/product/kingcommerce/fashion/item/quick/view/187"
											data-toggle="modal" data-target="#quickview" data-placement="top">
											<i class="fas fa-shopping-basket"></i>
										</span>
									</li>
									<li>
										<span href="javascript:;" class="add-to-compare"
											data-href="https://royalscripts.com/product/kingcommerce/fashion/item/compare/add/187"
											data-toggle="tooltip" data-placement="top" title="Compare">
											<i class="icofont-exchange"></i>
										</span>
									</li>
								</ul>
							</div>
						</div>
					</a>
					<a href="item/kingcommerce-fashion-products-title-will-be-here-one-01-nb90982lae.html"
						class="item">
						<div class="item-img">
							<img class="img-fluid" src="{{ asset('assets/website/images/thumbnails') }}/1615885461aMk85tfz.jpg" alt="">
						</div>
						<div class="info">
							<h5 class="name">KingCommerce Fashion Products Title Will be here one 01</h5>
							<h4 class="price">25$
								<del><small>28$</small></del>
							</h4>
							<div class="stars">
								<div class="ratings">
									<div class="empty-stars"></div>
									<div class="full-stars" style="width:0%"></div>
								</div>
							</div>
							<div class="item-cart-area">

								<ul class="item-cart-options">
									<li>

										<span href="javascript:;" rel-toggle="tooltip" title="Add To Wishlist"
											data-toggle="modal" id="wish-btn" data-target="#comment-log-reg"
											data-placement="top">
											<i class="icofont-heart-alt"></i>
										</span>

									</li>
									<li>
										<span class="quick-view" rel-toggle="tooltip" title="Quick View"
											href="javascript:;"
											data-href="https://royalscripts.com/product/kingcommerce/fashion/item/quick/view/186"
											data-toggle="modal" data-target="#quickview" data-placement="top">
											<i class="fas fa-shopping-basket"></i>
										</span>
									</li>
									<li>
										<span href="javascript:;" class="add-to-compare"
											data-href="https://royalscripts.com/product/kingcommerce/fashion/item/compare/add/186"
											data-toggle="tooltip" data-placement="top" title="Compare">
											<i class="icofont-exchange"></i>
										</span>
									</li>
								</ul>
							</div>
						</div>
					</a>
				</div>
			</div>

		</div>
	</div>
</section>
<!-- Tranding Item Area End -->

<!-- Banner Area One Start -->
<section class="banner-section">
	<div class="container">
		<div class="row">
			<div class="col-lg-6 remove-padding">
				<div class="left">
					<a class="banner-effect" href="category/Accessories.html" target="_blank">
						<img src="{{ asset('assets/website/images/banners') }}/1615883199Top%20small%20banner%20(2).jpg" alt="">
					</a>
				</div>
			</div>
			<div class="col-lg-6 remove-padding">
				<div class="left">
					<a class="banner-effect" href="category/Accessories.html" target="_blank">
						<img src="{{ asset('assets/website/images/banners') }}/1615883180Top%20small%20banner%20(1).jpg" alt="">
					</a>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- Banner Area One Start -->

<section id="extraData">
	<div class="text-center">
		<img src="{{ asset('assets/website/images') }}1564224328loading3.gif">
	</div>
</section>

</x-master>