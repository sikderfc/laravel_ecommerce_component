
<x-admin_master>
   
    <div class="row row-cards-one">
        <div class="col-md-12 col-lg-6 col-xl-4">
            <div class="mycard bg1">
                <div class="left">
                    <h5 class="title">Orders Pending! </h5>
                    <span class="number">845</span>
                    <a href="https://royalscripts.com/demo/kingcommerce/admin/orders/pending" class="link">View All</a>
                </div>
                <div class="right d-flex align-self-center">
                    <div class="icon">
                        <i class="icofont-dollar"></i>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-12 col-lg-6 col-xl-4">
            <div class="mycard bg2">
                <div class="left">
                    <h5 class="title">Orders Procsessing!</h5>
                    <span class="number">0</span>
                    <a href="https://royalscripts.com/demo/kingcommerce/admin/orders/processing" class="link">View All</a>
                </div>
                <div class="right d-flex align-self-center">
                    <div class="icon">
                        <i class="icofont-truck-alt"></i>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-12 col-lg-6 col-xl-4">
            <div class="mycard bg3">
                <div class="left">
                    <h5 class="title">Orders Completed!</h5>
                    <span class="number">159</span>
                    <a href="https://royalscripts.com/demo/kingcommerce/admin/orders/completed" class="link">View All</a>
                </div>
                <div class="right d-flex align-self-center">
                    <div class="icon">
                        <i class="icofont-check-circled"></i>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-12 col-lg-6 col-xl-4">
            <div class="mycard bg4">
                <div class="left">
                    <h5 class="title">Total Products!</h5>
                    <span class="number">45</span>
                    <a href="https://royalscripts.com/demo/kingcommerce/admin/products" class="link">View All</a>
                </div>
                <div class="right d-flex align-self-center">
                    <div class="icon">
                        <i class="icofont-cart-alt"></i>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-12 col-lg-6 col-xl-4">
            <div class="mycard bg5">
                <div class="left">
                    <h5 class="title">Total Customers!</h5>
                    <span class="number">170</span>
                    <a href="https://royalscripts.com/demo/kingcommerce/admin/users" class="link">View All</a>
                </div>
                <div class="right d-flex align-self-center">
                    <div class="icon">
                        <i class="icofont-users-alt-5"></i>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-12 col-lg-6 col-xl-4">
            <div class="mycard bg6">
                <div class="left">
                    <h5 class="title">Total Posts!</h5>
                    <span class="number">15</span>
                    <a href="https://royalscripts.com/demo/kingcommerce/admin/blog" class="link">View All</a>
                </div>
                <div class="right d-flex align-self-center">
                    <div class="icon">
                        <i class="icofont-newspaper"></i>
                    </div>
                </div>
            </div>
        </div>

    </div>

    <div class="row row-cards-one">
        <div class="col-md-6 col-xl-3">
            <div class="card c-info-box-area">
                <div class="c-info-box box1">
                    <p>4</p>
                </div>
                <div class="c-info-box-content">
                    <h6 class="title">New Customers</h6>
                    <p class="text">Last 30 Days</p>
                </div>
            </div>
        </div>
        <div class="col-md-6 col-xl-3">
            <div class="card c-info-box-area">
                <div class="c-info-box box2">
                    <p>170</p>
                </div>
                <div class="c-info-box-content">
                    <h6 class="title">Total Customers</h6>
                    <p class="text">All Time</p>
                </div>
            </div>
        </div>
        <div class="col-md-6 col-xl-3">
            <div class="card c-info-box-area">
                <div class="c-info-box box3">
                    <p>1</p>
                </div>
                <div class="c-info-box-content">
                    <h6 class="title">Total Sales</h6>
                    <p class="text">Last 30 days</p>
                </div>
            </div>
        </div>
        <div class="col-md-6 col-xl-3">
            <div class="card c-info-box-area">
                <div class="c-info-box box4">
                     <p>159</p>
                </div>
                <div class="c-info-box-content">
                    <h6 class="title">Total Sales</h6>
                    <p class="text">All Time</p>
                </div>
            </div>
        </div>
    </div>

    <div class="row row-cards-one">

        <div class="col-md-6 col-lg-6 col-xl-6">
            <div class="card">
                <h5 class="card-header">Recent Order(s)</h5>
                <div class="card-body">

                    <div class="my-table-responsiv">
                        <table class="table table-hover dt-responsive" cellspacing="0" width="100%">
                            <thead>
                                <tr>

                                    <th>Order Number</th>
                                    <th>Order Date</th>
                                </tr>
                                                                <tr>
                                    <td>4KRR1663934237</td>
                                    <td>2022-09-23</td>
                                    <td>
                                        <div class="action-list"><a href="https://royalscripts.com/demo/kingcommerce/admin/order/1039/show"><i
                                                    class="fas fa-eye"></i> Details</a>
                                        </div>
                                    </td>
                                </tr>
                                                                <tr>
                                    <td>wm0A1663032913</td>
                                    <td>2022-09-13</td>
                                    <td>
                                        <div class="action-list"><a href="https://royalscripts.com/demo/kingcommerce/admin/order/1038/show"><i
                                                    class="fas fa-eye"></i> Details</a>
                                        </div>
                                    </td>
                                </tr>
                                                                <tr>
                                    <td>Ontw1662805984</td>
                                    <td>2022-09-10</td>
                                    <td>
                                        <div class="action-list"><a href="https://royalscripts.com/demo/kingcommerce/admin/order/1037/show"><i
                                                    class="fas fa-eye"></i> Details</a>
                                        </div>
                                    </td>
                                </tr>
                                                                <tr>
                                    <td>uHE11662746467</td>
                                    <td>2022-09-09</td>
                                    <td>
                                        <div class="action-list"><a href="https://royalscripts.com/demo/kingcommerce/admin/order/1036/show"><i
                                                    class="fas fa-eye"></i> Details</a>
                                        </div>
                                    </td>
                                </tr>
                                                                <tr>
                                    <td>339g1662535560</td>
                                    <td>2022-09-07</td>
                                    <td>
                                        <div class="action-list"><a href="https://royalscripts.com/demo/kingcommerce/admin/order/1035/show"><i
                                                    class="fas fa-eye"></i> Details</a>
                                        </div>
                                    </td>
                                </tr>
                                                            </thead>
                        </table>
                    </div>

                </div>
            </div>

        </div>

        <div class="col-md-6 col-lg-6 col-xl-6">
                <div class="card">
                        <h5 class="card-header">Recent Customer(s)</h5>
                        <div class="card-body">
        
                            <div class="my-table-responsiv">
                                <table class="table table-hover dt-responsive" cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                            <th>Customer Email</th>
                                            <th>Joined</th>
                                        </tr>
                                                                                <tr>
                                            <td><a href="/cdn-cgi/l/email-protection" class="__cf_email__" data-cfemail="0f6b66686666616b666e7c3b4f68626e6663216c6062">[email&#160;protected]</a></td>
                                            <td>2022-09-20 11:37:24</td>
                                            <td>
                                                <div class="action-list"><a href="https://royalscripts.com/demo/kingcommerce/admin/user/194/show"><i
                                                            class="fas fa-eye"></i> Details</a>
                                                </div>
                                            </td>
                                        </tr>
                                                                                <tr>
                                            <td><a href="/cdn-cgi/l/email-protection" class="__cf_email__" data-cfemail="a1c3cdcececcd2cac8cdcdd2e1c6ccc0c8cd8fc2cecc">[email&#160;protected]</a></td>
                                            <td>2022-09-11 09:48:16</td>
                                            <td>
                                                <div class="action-list"><a href="https://royalscripts.com/demo/kingcommerce/admin/user/193/show"><i
                                                            class="fas fa-eye"></i> Details</a>
                                                </div>
                                            </td>
                                        </tr>
                                                                                <tr>
                                            <td><a href="/cdn-cgi/l/email-protection" class="__cf_email__" data-cfemail="0571646c686a6a776d707676646c6b363330456268646c692b666a68">[email&#160;protected]</a></td>
                                            <td>2022-09-05 23:48:28</td>
                                            <td>
                                                <div class="action-list"><a href="https://royalscripts.com/demo/kingcommerce/admin/user/192/show"><i
                                                            class="fas fa-eye"></i> Details</a>
                                                </div>
                                            </td>
                                        </tr>
                                                                                <tr>
                                            <td><a href="/cdn-cgi/l/email-protection" class="__cf_email__" data-cfemail="442534342825333737262a37042329252d286a272b29">[email&#160;protected]</a></td>
                                            <td>2022-09-05 21:51:03</td>
                                            <td>
                                                <div class="action-list"><a href="https://royalscripts.com/demo/kingcommerce/admin/user/191/show"><i
                                                            class="fas fa-eye"></i> Details</a>
                                                </div>
                                            </td>
                                        </tr>
                                                                                <tr>
                                            <td><a href="/cdn-cgi/l/email-protection" class="__cf_email__" data-cfemail="492a202f2c272c09242820252027283d263b672a2624">[email&#160;protected]</a></td>
                                            <td>2022-08-05 22:24:54</td>
                                            <td>
                                                <div class="action-list"><a href="https://royalscripts.com/demo/kingcommerce/admin/user/190/show"><i
                                                            class="fas fa-eye"></i> Details</a>
                                                </div>
                                            </td>
                                        </tr>
                                                                            </thead>
                                </table>
                            </div>
        
                        </div>
                    </div>
        </div>
    </div>

    <div class="row row-cards-one">

            <div class="col-md-12 col-lg-12 col-xl-12">
                    <div class="card">
                            <h5 class="card-header">Popular Product(s)</h5>
                            <div class="card-body">
            
                                <div class="table-responsiv  dashboard-home-table">
                                    <table id="poproducts" class="table table-hover dt-responsive" cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
                                                <th>Featured Image</th>
                                                <th>Name</th>
                                                <th>Category</th>
                                                <th>Type</th>
                                                <th>Price</th>
                                                <th></th>
                                                
                                            </tr>
                                        </thead>
                                        <tbody>
                                                                                        <tr>
                                            <td><img src="{{ asset('assets/admin/images') }}/products/1568026326RDSwScJe.png"></td>
                                            <td>Physical Product Title Title will Be Here 101</td>
                                            <td>Fashion &amp; Beauty
                                                                                                        <br>
                                                    BAGS
                                                                                                                                                            <br>
                                                    Deleted
                                                                                                    </td>
                                                <td>Physical</td>

                                                <td> 130$ </td>

                                                <td>
                                                    <div class="action-list"><a href="https://royalscripts.com/demo/kingcommerce/admin/products/edit/101"><i
                                                                class="fas fa-eye"></i> Details</a>
                                                    </div>
                                                </td>
                                            </tr>
                                                                                        <tr>
                                            <td><img src="{{ asset('assets/admin/images') }}/products/1568026368qU5AILZo.png"></td>
                                            <td>Physical Product Title Title will Be Here 100</td>
                                            <td>Fashion &amp; Beauty
                                                                                                        <br>
                                                    BAGS
                                                                                                                                                            <br>
                                                    Deleted
                                                                                                    </td>
                                                <td>Physical</td>

                                                <td> 130$ </td>

                                                <td>
                                                    <div class="action-list"><a href="https://royalscripts.com/demo/kingcommerce/admin/products/edit/100"><i
                                                                class="fas fa-eye"></i> Details</a>
                                                    </div>
                                                </td>
                                            </tr>
                                                                                        <tr>
                                            <td><img src="{{ asset('assets/admin/images') }}/products/1568026301A6SNpEFR.png"></td>
                                            <td>Physical Product Title Title will Be Here 102</td>
                                            <td>Fashion &amp; Beauty
                                                                                                        <br>
                                                    BAGS
                                                                                                                                                            <br>
                                                    Deleted
                                                                                                    </td>
                                                <td>Physical</td>

                                                <td> 130$ </td>

                                                <td>
                                                    <div class="action-list"><a href="https://royalscripts.com/demo/kingcommerce/admin/products/edit/102"><i
                                                                class="fas fa-eye"></i> Details</a>
                                                    </div>
                                                </td>
                                            </tr>
                                                                                        <tr>
                                            <td><img src="{{ asset('assets/admin/images') }}/products/1568027732dTwHda8l.png"></td>
                                            <td>Affiliate Product Title will Be Here. Affiliate Pr...</td>
                                            <td>Electronic
                                                                                                        <br>
                                                    Deleted
                                                                                                                                                            <br>
                                                    Deleted
                                                                                                    </td>
                                                <td>Physical</td>

                                                <td> 57.5$ </td>

                                                <td>
                                                    <div class="action-list"><a href="https://royalscripts.com/demo/kingcommerce/admin/products/edit/95"><i
                                                                class="fas fa-eye"></i> Details</a>
                                                    </div>
                                                </td>
                                            </tr>
                                                                                        <tr>
                                            <td><img src="{{ asset('assets/admin/images') }}/products/1570877286SxUGZME4.jpg"></td>
                                            <td>Physical Product Title Title will Be Here 99u</td>
                                            <td>Fashion &amp; Beauty
                                                                                                        <br>
                                                    BAGS
                                                                                                                                                            <br>
                                                    Deleted
                                                                                                    </td>
                                                <td>Physical</td>

                                                <td> 130$ </td>

                                                <td>
                                                    <div class="action-list"><a href="https://royalscripts.com/demo/kingcommerce/admin/products/edit/182"><i
                                                                class="fas fa-eye"></i> Details</a>
                                                    </div>
                                                </td>
                                            </tr>
                                                                                    </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
    
            </div>
    
        </div>

    <div class="row row-cards-one">

            <div class="col-md-12 col-lg-12 col-xl-12">
                    <div class="card">
                            <h5 class="card-header">Recent Product(s)</h5>
                            <div class="card-body">
            
                                <div class="table-responsiv dashboard-home-table">
                                    <table id="pproducts" class="table table-hover dt-responsive" cellspacing="0" width="100%">
                                            <thead>
                                                    <tr>
                                                        <th>Featured Image</th>
                                                        <th>Name</th>
                                                        <th>Category</th>
                                                        <th>Type</th>
                                                        <th>Price</th>
                                                        <th></th>
                                                        
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                                                                        <tr>
                                                    <td><img src="{{ asset('assets/admin/images') }}/products/1570877286SxUGZME4.jpg"></td>
                                                    <td>Physical Product Title Title will Be Here 99u</td>
                                                    <td>Fashion &amp; Beauty
                                                                                                                <br>
                                                        BAGS
                                                                                                                                                                        <br>
                                                        Deleted
                                                                                                            </td>
                                                        <td>Physical</td>
                                                        <td> 130$ </td>
                                                        <td>
                                                            <div class="action-list"><a href="https://royalscripts.com/demo/kingcommerce/admin/products/edit/182"><i
                                                                        class="fas fa-eye"></i> Details</a>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                                                                        <tr>
                                                    <td><img src="{{ asset('assets/admin/images') }}/products/1570877275UqISZURU.jpg"></td>
                                                    <td>Physical Product Title Title will Be Here 99u</td>
                                                    <td>Fashion &amp; Beauty
                                                                                                                <br>
                                                        BAGS
                                                                                                                                                                        <br>
                                                        Deleted
                                                                                                            </td>
                                                        <td>Physical</td>
                                                        <td> 130$ </td>
                                                        <td>
                                                            <div class="action-list"><a href="https://royalscripts.com/demo/kingcommerce/admin/products/edit/181"><i
                                                                        class="fas fa-eye"></i> Details</a>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                                                                        <tr>
                                                    <td><img src="{{ asset('assets/admin/images') }}/products/1570877254IpMreGOE.jpg"></td>
                                                    <td>Physical Product Title Title will Be Here 99u</td>
                                                    <td>Fashion &amp; Beauty
                                                                                                                <br>
                                                        BAGS
                                                                                                                                                                        <br>
                                                        Deleted
                                                                                                            </td>
                                                        <td>Physical</td>
                                                        <td> 130$ </td>
                                                        <td>
                                                            <div class="action-list"><a href="https://royalscripts.com/demo/kingcommerce/admin/products/edit/180"><i
                                                                        class="fas fa-eye"></i> Details</a>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                                                                        <tr>
                                                    <td><img src="{{ asset('assets/admin/images') }}/products/1570877127ByWwIJUA.jpg"></td>
                                                    <td>Physical Product Title Title will Be Here 99</td>
                                                    <td>Fashion &amp; Beauty
                                                                                                                <br>
                                                        BAGS
                                                                                                                                                                        <br>
                                                        Deleted
                                                                                                            </td>
                                                        <td>Physical</td>
                                                        <td> 130$ </td>
                                                        <td>
                                                            <div class="action-list"><a href="https://royalscripts.com/demo/kingcommerce/admin/products/edit/179"><i
                                                                        class="fas fa-eye"></i> Details</a>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                                                                        <tr>
                                                    <td><img src="{{ asset('assets/admin/images') }}/products/1570876820YXbcdnxW.jpg"></td>
                                                    <td>Physical Product Title Title will Be Here 99</td>
                                                    <td>Fashion &amp; Beauty
                                                                                                                <br>
                                                        BAGS
                                                                                                                                                                        <br>
                                                        Deleted
                                                                                                            </td>
                                                        <td>Physical</td>
                                                        <td> 130$ </td>
                                                        <td>
                                                            <div class="action-list"><a href="https://royalscripts.com/demo/kingcommerce/admin/products/edit/178"><i
                                                                        class="fas fa-eye"></i> Details</a>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                                                                    </tbody>
                                    </table>
                                </div>
            
                            </div>
                        </div>
    
            </div>
    
        </div>

    <div class="row row-cards-one">

        <div class="col-md-12 col-lg-12 col-xl-12">
            <div class="card">
                <h5 class="card-header">Total Sales in Last 30 Days</h5>
                <div class="card-body">

                    <canvas id="lineChart"></canvas>

                </div>
            </div>

        </div>

    </div>




    <div class="row row-cards-one">

        <div class="col-md-6 col-lg-6 col-xl-6">
            <div class="card">
                <h5 class="card-header">Top Referrals</h5>
                <div class="card-body">
                    <div class="admin-fix-height-card">
                         <div id="chartContainer-topReference"></div>
                    </div>
                       
                </div>
            </div>

        </div>

        <div class="col-md-6 col-lg-6 col-xl-6">
                <div class="card">
                        <h5 class="card-header">Most Used OS</h5>
                        <div class="card-body">
<div class="admin-fix-height-card">
                        <div id="chartContainer-os"></div>
</div>
                        </div>
                    </div>
        </div>
        
    </div>



</div>
</x-admin_master>