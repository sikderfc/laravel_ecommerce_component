<x-admin_master>
    <div class="content-area" id="modalEdit">
        <div class="mr-breadcrumb">
            <div class="row">
                <div class="col-lg-12">
                    <h4 class="heading"> Edit Product<a class="add-btn" href="http://127.0.0.1:8000/"><i class="fas fa-arrow-left"></i> Back</a></h4>
                    <ul class="links">
                        <li>
                            <a href="https://royalscripts.com/demo/kingcommerce/admin">Dashboard </a>
                        </li>
                        <li>
                            <a href="https://royalscripts.com/demo/kingcommerce/admin/products">Products </a>
                        </li>
                        <li>
                            <a href="javascript:;">Physical Product</a>
                        </li>
                        <li>
                            <a href="javascript:;">Edit</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="add-product-content">
            <div class="row">
                <div class="col-lg-12">
                    <div class="product-description">
                        <div class="body-area">

                            <div class="gocover" style="background: url(https://royalscripts.com/demo/kingcommerce/assets/images/1564224329loading3.gif) no-repeat scroll center center rgba(45, 45, 45, 0.5);"></div>
                            <form id="geniusform" action="https://royalscripts.com/demo/kingcommerce/admin/products/edit/182" method="POST" enctype="multipart/form-data">
                                <input type="hidden" name="_token" value="EN8YDewgmAdii1TaLiW2XSEGY29Cos5h6hbVfDxe">


                                <div class="alert alert-success validation" style="display: none;">
                                    <button type="button" class="close alert-close"><span>×</span></button>
                                    <p class="text-left"></p>
                                </div>
                                <div class="alert alert-danger validation" style="display: none;">
                                    <button type="button" class="close alert-close"><span>×</span></button>
                                    <ul class="text-left">
                                    </ul>
                                </div>
                                <div class="row">
                                    <div class="col-lg-4">
                                        <div class="left-area">
                                            <h4 class="heading">Product Name* </h4>
                                            <p class="sub-heading">(In Any Language)</p>
                                        </div>
                                    </div>
                                    <div class="col-lg-7">
                                        <input type="text" class="input-field" placeholder="Enter Product Name" name="name" required="" value="Physical Product Title Title will Be Here 99u">
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-lg-4">
                                        <div class="left-area">
                                            <h4 class="heading">Product Sku* </h4>
                                        </div>
                                    </div>
                                    <div class="col-lg-7">
                                        <input type="text" class="input-field" placeholder="Enter Product Sku" name="sku" required="" value="b017277kfm">

                                        <div class="checkbox-wrapper">
                                            <input type="checkbox" name="product_condition_check" class="checkclick" id="conditionCheck" value="1" checked="">
                                            <label for="conditionCheck">Allow Product Condition</label>
                                        </div>

                                    </div>
                                </div>


                                <div class="">

                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="left-area">
                                                <h4 class="heading">Product Condition*</h4>
                                            </div>
                                        </div>
                                        <div class="col-lg-7">
                                            <select name="product_condition">
                                                <option value="2" selected="">New</option>
                                                <option value="1">Used</option>
                                            </select>
                                        </div>

                                    </div>


                                </div>

                                <div class="row">
                                    <div class="col-lg-4">
                                        <div class="left-area">
                                            <h4 class="heading">Category*</h4>
                                        </div>
                                    </div>
                                    <div class="col-lg-7">
                                        <select id="cat" name="category_id" required="">
                                            <option>Select Category</option>

                                            <option data-href="https://royalscripts.com/demo/kingcommerce/admin/load/subcategories/4" value="4">Electronic</option>
                                            <option data-href="https://royalscripts.com/demo/kingcommerce/admin/load/subcategories/5" value="5" selected="">Fashion &amp; Beauty</option>
                                            <option data-href="https://royalscripts.com/demo/kingcommerce/admin/load/subcategories/6" value="6">Camera &amp; Photo</option>
                                            <option data-href="https://royalscripts.com/demo/kingcommerce/admin/load/subcategories/7" value="7">Smart Phone &amp; Table</option>
                                            <option data-href="https://royalscripts.com/demo/kingcommerce/admin/load/subcategories/8" value="8">Sport &amp; Outdoor</option>
                                            <option data-href="https://royalscripts.com/demo/kingcommerce/admin/load/subcategories/9" value="9">Jewelry &amp; Watches</option>
                                            <option data-href="https://royalscripts.com/demo/kingcommerce/admin/load/subcategories/10" value="10">Health &amp; Beauty</option>
                                            <option data-href="https://royalscripts.com/demo/kingcommerce/admin/load/subcategories/11" value="11">Books &amp; Office</option>
                                            <option data-href="https://royalscripts.com/demo/kingcommerce/admin/load/subcategories/12" value="12">Toys &amp; Hobbies</option>
                                            <option data-href="https://royalscripts.com/demo/kingcommerce/admin/load/subcategories/13" value="13">Books</option>
                                            <option data-href="https://royalscripts.com/demo/kingcommerce/admin/load/subcategories/15" value="15">Automobiles &amp; Motorcycles</option>
                                            <option data-href="https://royalscripts.com/demo/kingcommerce/admin/load/subcategories/16" value="16">Home decoration &amp; Appliance</option>
                                            <option data-href="https://royalscripts.com/demo/kingcommerce/admin/load/subcategories/17" value="17">Portable &amp; Personal Electronics</option>
                                            <option data-href="https://royalscripts.com/demo/kingcommerce/admin/load/subcategories/18" value="18">Outdoor, Recreation &amp; Fitness</option>
                                            <option data-href="https://royalscripts.com/demo/kingcommerce/admin/load/subcategories/19" value="19">Surveillance Safety &amp; Security</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-lg-4">
                                        <div class="left-area">
                                            <h4 class="heading">Sub Category*</h4>
                                        </div>
                                    </div>
                                    <div class="col-lg-7">
                                        <select id="subcat" name="subcategory_id">
                                            <option value="">Select Sub Category</option>
                                            <option data-href="https://royalscripts.com/demo/kingcommerce/admin/load/childcategories/6" value="6">ACCESSORIES</option>
                                            <option data-href="https://royalscripts.com/demo/kingcommerce/admin/load/childcategories/7" value="7" selected="">BAGS</option>
                                            <option data-href="https://royalscripts.com/demo/kingcommerce/admin/load/childcategories/8" value="8">CLOTHINGS</option>
                                            <option data-href="https://royalscripts.com/demo/kingcommerce/admin/load/childcategories/9" value="9">SHOES</option>


                                        </select>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-lg-4">
                                        <div class="left-area">
                                            <h4 class="heading">Child Category*</h4>
                                        </div>
                                    </div>
                                    <div class="col-lg-7">
                                        <select id="childcat" name="childcategory_id">
                                            <option value="">Select Child Category</option>
                                        </select>
                                    </div>
                                </div>





                                <div id="catAttributes">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="left-area">
                                                <h4 class="heading">Warranty Type *</h4>
                                            </div>
                                        </div>
                                        <div class="col-lg-7">


                                            <div class="row">
                                                <div class="col-lg-5">
                                                    <div class="custom-control custom-checkbox">
                                                        <input type="checkbox" id="warranty_type107" name="warranty_type[]" value="No Warranty" class="custom-control-input attr-checkbox">
                                                        <label class="custom-control-label" for="warranty_type107">No Warranty</label>
                                                    </div>
                                                </div>

                                                <div class="col-lg-7 ">
                                                    <div class="row">
                                                        <div class="col-2">
                                                            +
                                                        </div>
                                                        <div class="col-10">
                                                            <div class="price-container">
                                                                <span class="price-curr">$</span>
                                                                <input type="text" class="input-field price-input" id="warranty_type107_price" data-name="warranty_type_price[]" placeholder="0.00 (Additional Price)" value="">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>




                                            <div class="row">
                                                <div class="col-lg-5">
                                                    <div class="custom-control custom-checkbox">
                                                        <input type="checkbox" id="warranty_type108" name="warranty_type[]" value="Local seller Warranty" class="custom-control-input attr-checkbox">
                                                        <label class="custom-control-label" for="warranty_type108">Local seller Warranty</label>
                                                    </div>
                                                </div>

                                                <div class="col-lg-7 ">
                                                    <div class="row">
                                                        <div class="col-2">
                                                            +
                                                        </div>
                                                        <div class="col-10">
                                                            <div class="price-container">
                                                                <span class="price-curr">$</span>
                                                                <input type="text" class="input-field price-input" id="warranty_type108_price" data-name="warranty_type_price[]" placeholder="0.00 (Additional Price)" value="">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>




                                            <div class="row">
                                                <div class="col-lg-5">
                                                    <div class="custom-control custom-checkbox">
                                                        <input type="checkbox" id="warranty_type109" name="warranty_type[]" value="Non local warranty" class="custom-control-input attr-checkbox">
                                                        <label class="custom-control-label" for="warranty_type109">Non local warranty</label>
                                                    </div>
                                                </div>

                                                <div class="col-lg-7 ">
                                                    <div class="row">
                                                        <div class="col-2">
                                                            +
                                                        </div>
                                                        <div class="col-10">
                                                            <div class="price-container">
                                                                <span class="price-curr">$</span>
                                                                <input type="text" class="input-field price-input" id="warranty_type109_price" data-name="warranty_type_price[]" placeholder="0.00 (Additional Price)" value="">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>




                                            <div class="row">
                                                <div class="col-lg-5">
                                                    <div class="custom-control custom-checkbox">
                                                        <input type="checkbox" id="warranty_type110" name="warranty_type[]" value="International Manufacturer Warranty" class="custom-control-input attr-checkbox">
                                                        <label class="custom-control-label" for="warranty_type110">International Manufacturer Warranty</label>
                                                    </div>
                                                </div>

                                                <div class="col-lg-7 ">
                                                    <div class="row">
                                                        <div class="col-2">
                                                            +
                                                        </div>
                                                        <div class="col-10">
                                                            <div class="price-container">
                                                                <span class="price-curr">$</span>
                                                                <input type="text" class="input-field price-input" id="warranty_type110_price" data-name="warranty_type_price[]" placeholder="0.00 (Additional Price)" value="">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>




                                            <div class="row">
                                                <div class="col-lg-5">
                                                    <div class="custom-control custom-checkbox">
                                                        <input type="checkbox" id="warranty_type111" name="warranty_type[]" value="International Seller Warranty" class="custom-control-input attr-checkbox">
                                                        <label class="custom-control-label" for="warranty_type111">International Seller Warranty</label>
                                                    </div>
                                                </div>

                                                <div class="col-lg-7 ">
                                                    <div class="row">
                                                        <div class="col-2">
                                                            +
                                                        </div>
                                                        <div class="col-10">
                                                            <div class="price-container">
                                                                <span class="price-curr">$</span>
                                                                <input type="text" class="input-field price-input" id="warranty_type111_price" data-name="warranty_type_price[]" placeholder="0.00 (Additional Price)" value="">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>


                                        </div>

                                    </div>
                                </div>




                                <div id="subcatAttributes">
                                </div>




                                <div id="childcatAttributes">
                                </div>



                                <div class="row">
                                    <div class="col-lg-4">
                                        <div class="left-area">
                                            <h4 class="heading">Feature Image *</h4>
                                        </div>
                                    </div>
                                    <div class="col-lg-7">
                                        <div class="row">
                                            <div class="panel panel-body">
                                                <div class="span4 cropme text-center" id="landscape" style="width: 400px; height: 400px; border: 1px dashed black;"><img src="https://royalscripts.com/demo/kingcommerce/assets/images/products/1570877286SxUGZME4.jpg" alt=""></div>
                                            </div>
                                        </div>

                                        <a href="javascript:;" id="crop-image" class="d-inline-block mybtn1">
                                            <i class="icofont-upload-alt"></i> Upload Image Here
                                        </a>


                                    </div>
                                </div>

                                <input type="hidden" id="feature_photo" name="photo" value="1570877286SxUGZME4.jpg" accept="image/*">

                                <div class="row">
                                    <div class="col-lg-4">
                                        <div class="left-area">
                                            <h4 class="heading">
                                                Product Gallery Images *
                                            </h4>
                                        </div>
                                    </div>
                                    <div class="col-lg-7">
                                        <a href="javascript" class="set-gallery" data-toggle="modal" data-target="#setgallery">
                                            <input type="hidden" value="182">
                                            <i class="icofont-plus"></i> Set Gallery
                                        </a>
                                    </div>
                                </div>


                                <div class="row">
                                    <div class="col-lg-4">
                                        <div class="left-area">

                                        </div>
                                    </div>
                                    <div class="col-lg-7">
                                        <ul class="list">
                                            <li>
                                                <input class="checkclick1" name="shipping_time_check" type="checkbox" id="check1" value="1" checked="">
                                                <label for="check1">Allow Estimated Shipping Time</label>
                                            </li>
                                        </ul>
                                    </div>
                                </div>



                                <div class="">

                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="left-area">
                                                <h4 class="heading">Product Estimated Shipping Time* </h4>
                                            </div>
                                        </div>
                                        <div class="col-lg-7">
                                            <input type="text" class="input-field" placeholder="Estimated Shipping Time" name="ship" value="5-7 days">
                                        </div>
                                    </div>


                                </div>

                                <div class="row">
                                    <div class="col-lg-4">
                                        <div class="left-area">

                                        </div>
                                    </div>
                                    <div class="col-lg-7">
                                        <ul class="list">
                                            <li>
                                                <input name="size_check" type="checkbox" id="size-check" value="1" checked="">
                                                <label for="size-check">Allow Product Sizes</label>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="" id="size-display">
                                    <div class="row">
                                        <div class="col-lg-4">
                                        </div>
                                        <div class="col-lg-7">
                                            <div class="product-size-details" id="size-section">
                                                <div class="size-area">
                                                    <span class="remove size-remove"><i class="fas fa-times"></i></span>
                                                    <div class="row">
                                                        <div class="col-md-4 col-sm-6">
                                                            <label>
                                                                Size Name :
                                                                <span>
                                                                    (eg. S,M,L,XL,XXL,3XL,4XL)
                                                                </span>
                                                            </label>
                                                            <input type="text" name="size[]" class="input-field" placeholder="Size Name" value="S">
                                                        </div>
                                                        <div class="col-md-4 col-sm-6">
                                                            <label>
                                                                Size Qty :
                                                                <span>
                                                                    (Number of quantity of this size)
                                                                </span>
                                                            </label>
                                                            <input type="number" name="size_qty[]" class="input-field" placeholder="Size Qty" min="1" value="2147483384">
                                                        </div>
                                                        <div class="col-md-4 col-sm-6">
                                                            <label>
                                                                Size Price :
                                                                <span>
                                                                    (This price will be added with base price)
                                                                </span>
                                                            </label>
                                                            <input type="number" name="size_price[]" class="input-field" placeholder="Size Price" min="0" value="20">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <a href="javascript:;" id="size-btn" class="add-more"><i class="fas fa-plus"></i>Add More Size </a>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-lg-4">
                                        <div class="left-area">

                                        </div>
                                    </div>
                                    <div class="col-lg-7">
                                        <ul class="list">
                                            <li>
                                                <input class="checkclick1" name="color_check" type="checkbox" id="check3" value="1" checked="">
                                                <label for="check3">Allow Product Colors</label>
                                            </li>
                                        </ul>
                                    </div>
                                </div>



                                <div class="">

                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="left-area">
                                                <h4 class="heading">
                                                    Product Colors*
                                                </h4>
                                                <p class="sub-heading">
                                                    (Choose Your Favorite Colors)
                                                </p>
                                            </div>
                                        </div>
                                        <div class="col-lg-7">
                                            <div class="select-input-color" id="color-section">
                                                <div class="color-area">
                                                    <span class="remove color-remove"><i class="fas fa-times"></i></span>
                                                    <div class="input-group colorpicker-component cp colorpicker-element">
                                                        <input type="text" name="color[]" value="#000000" class="input-field cp colorpicker-element">
                                                        <span class="input-group-addon"><i style="background-color: rgb(0, 0, 0);"></i></span>
                                                    </div>
                                                </div>
                                                <div class="color-area">
                                                    <span class="remove color-remove"><i class="fas fa-times"></i></span>
                                                    <div class="input-group colorpicker-component cp colorpicker-element">
                                                        <input type="text" name="color[]" value="#851818" class="input-field cp colorpicker-element">
                                                        <span class="input-group-addon"><i style="background-color: rgb(133, 24, 24);"></i></span>
                                                    </div>
                                                </div>
                                                <div class="color-area">
                                                    <span class="remove color-remove"><i class="fas fa-times"></i></span>
                                                    <div class="input-group colorpicker-component cp colorpicker-element">
                                                        <input type="text" name="color[]" value="#ff0d0d" class="input-field cp colorpicker-element">
                                                        <span class="input-group-addon"><i style="background-color: rgb(255, 13, 13);"></i></span>
                                                    </div>
                                                </div>
                                                <div class="color-area">
                                                    <span class="remove color-remove"><i class="fas fa-times"></i></span>
                                                    <div class="input-group colorpicker-component cp colorpicker-element">
                                                        <input type="text" name="color[]" value="#1feb4c" class="input-field cp colorpicker-element">
                                                        <span class="input-group-addon"><i style="background-color: rgb(31, 235, 76);"></i></span>
                                                    </div>
                                                </div>
                                                <div class="color-area">
                                                    <span class="remove color-remove"><i class="fas fa-times"></i></span>
                                                    <div class="input-group colorpicker-component cp colorpicker-element">
                                                        <input type="text" name="color[]" value="#d620cf" class="input-field cp colorpicker-element">
                                                        <span class="input-group-addon"><i style="background-color: rgb(214, 32, 207);"></i></span>
                                                    </div>
                                                </div>
                                                <div class="color-area">
                                                    <span class="remove color-remove"><i class="fas fa-times"></i></span>
                                                    <div class="input-group colorpicker-component cp colorpicker-element">
                                                        <input type="text" name="color[]" value="#186ceb" class="input-field cp colorpicker-element">
                                                        <span class="input-group-addon"><i style="background-color: rgb(24, 108, 235);"></i></span>
                                                    </div>
                                                </div>
                                            </div>
                                            <a href="javascript:;" id="color-btn" class="add-more mt-4 mb-3"><i class="fas fa-plus"></i>Add More Color </a>
                                        </div>
                                    </div>

                                </div>



                                <div class="row">
                                    <div class="col-lg-4">
                                        <div class="left-area">

                                        </div>
                                    </div>
                                    <div class="col-lg-7">
                                        <ul class="list">
                                            <li>
                                                <input class="checkclick1" name="whole_check" type="checkbox" id="whole_check" value="1" checked="">
                                                <label for="whole_check">Allow Product Whole Sell</label>
                                            </li>
                                        </ul>
                                    </div>
                                </div>

                                <div class="">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="left-area">

                                            </div>
                                        </div>
                                        <div class="col-lg-7">
                                            <div class="featured-keyword-area">
                                                <div class="feature-tag-top-filds" id="whole-section">


                                                    <div class="feature-area">
                                                        <span class="remove whole-remove"><i class="fas fa-times"></i></span>
                                                        <div class="row">
                                                            <div class="col-lg-6">
                                                                <input type="number" name="whole_sell_qty[]" class="input-field" placeholder="Enter Quantity" min="0" value="10" required="">
                                                            </div>

                                                            <div class="col-lg-6">
                                                                <input type="number" name="whole_sell_discount[]" class="input-field" placeholder="Enter Discount Percentage" min="0" value="5" required="">
                                                            </div>
                                                        </div>
                                                    </div>



                                                    <div class="feature-area">
                                                        <span class="remove whole-remove"><i class="fas fa-times"></i></span>
                                                        <div class="row">
                                                            <div class="col-lg-6">
                                                                <input type="number" name="whole_sell_qty[]" class="input-field" placeholder="Enter Quantity" min="0" value="20" required="">
                                                            </div>

                                                            <div class="col-lg-6">
                                                                <input type="number" name="whole_sell_discount[]" class="input-field" placeholder="Enter Discount Percentage" min="0" value="10" required="">
                                                            </div>
                                                        </div>
                                                    </div>



                                                    <div class="feature-area">
                                                        <span class="remove whole-remove"><i class="fas fa-times"></i></span>
                                                        <div class="row">
                                                            <div class="col-lg-6">
                                                                <input type="number" name="whole_sell_qty[]" class="input-field" placeholder="Enter Quantity" min="0" value="30" required="">
                                                            </div>

                                                            <div class="col-lg-6">
                                                                <input type="number" name="whole_sell_discount[]" class="input-field" placeholder="Enter Discount Percentage" min="0" value="15" required="">
                                                            </div>
                                                        </div>
                                                    </div>



                                                    <div class="feature-area">
                                                        <span class="remove whole-remove"><i class="fas fa-times"></i></span>
                                                        <div class="row">
                                                            <div class="col-lg-6">
                                                                <input type="number" name="whole_sell_qty[]" class="input-field" placeholder="Enter Quantity" min="0" value="40" required="">
                                                            </div>

                                                            <div class="col-lg-6">
                                                                <input type="number" name="whole_sell_discount[]" class="input-field" placeholder="Enter Discount Percentage" min="0" value="20" required="">
                                                            </div>
                                                        </div>
                                                    </div>


                                                </div>

                                                <a href="javascript:;" id="whole-btn" class="add-fild-btn"><i class="icofont-plus"></i> Add More Field</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-lg-4">
                                        <div class="left-area">
                                            <h4 class="heading">
                                                Product Current Price*
                                            </h4>
                                            <p class="sub-heading">
                                                (In USD)
                                            </p>
                                        </div>
                                    </div>
                                    <div class="col-lg-7">
                                        <input name="price" type="number" class="input-field" placeholder="e.g 20" step="0.1" min="0" value="100" required="">
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-lg-4">
                                        <div class="left-area">
                                            <h4 class="heading">Product Previous Price*</h4>
                                            <p class="sub-heading">(Optional)</p>
                                        </div>
                                    </div>
                                    <div class="col-lg-7">
                                        <input name="previous_price" step="0.1" type="number" class="input-field" placeholder="e.g 20" value="200" min="0">
                                    </div>
                                </div>
                                <div class="showbox" id="stckprod">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="left-area">
                                                <h4 class="heading">Product Stock*</h4>
                                                <p class="sub-heading">(Leave Empty will Show Always Available)</p>
                                            </div>
                                        </div>
                                        <div class="col-lg-7">
                                            <input name="stock" type="text" class="input-field" placeholder="e.g 20" value="">
                                            <div class="checkbox-wrapper">
                                                <input type="checkbox" name="measure_check" class="checkclick1" id="allowProductMeasurement" value="1">
                                                <label for="allowProductMeasurement">Allow Product Measurement</label>
                                            </div>
                                        </div>
                                    </div>

                                </div>

                                <div class="showbox">

                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="left-area">
                                                <h4 class="heading">Product Measurement*</h4>
                                            </div>
                                        </div>
                                        <div class="col-lg-3">
                                            <select id="product_measure">
                                                <option value="" selected="">None</option>
                                                <option value="Gram">Gram</option>
                                                <option value="Kilogram">Kilogram</option>
                                                <option value="Litre">Litre</option>
                                                <option value="Pound">Pound</option>
                                                <option value="Custom" selected="">Custom</option>
                                            </select>
                                        </div>
                                        <div class="col-lg-1"></div>
                                        <div class="col-lg-3 " id="measure">
                                            <input name="measure" type="text" id="measurement" class="input-field" placeholder="Enter Unit" value="">
                                        </div>
                                    </div>

                                </div>


                                <div class="row">
                                    <div class="col-lg-4">
                                        <div class="left-area">
                                            <h4 class="heading">
                                                Product Description*
                                            </h4>
                                        </div>
                                    </div>
                                    <div class="col-lg-7">
                                        <div class="text-editor">
                                            <div unselectable="on" style="width: 100%;">
                                                <div class=" nicEdit-panelContain" unselectable="on" style="overflow: hidden; width: 100%; border: 1px solid rgb(204, 204, 204); background-color: rgb(239, 239, 239);">
                                                    <div class=" nicEdit-panel" unselectable="on" style="margin: 0px 2px 2px; zoom: 1; overflow: hidden;">
                                                        <div style="float: left; margin-top: 2px; display: none;">
                                                            <div class=" nicEdit-buttonContain" style="width: 20px; height: 20px; opacity: 0.6;">
                                                                <div class=" nicEdit-button-undefined" style="background-color: rgb(239, 239, 239); border: 1px solid rgb(239, 239, 239);">
                                                                    <div class=" nicEdit-button" unselectable="on" style="width: 18px; height: 18px; overflow: hidden; zoom: 1; cursor: pointer; background-image: url(&quot;http://js.nicedit.com/nicEditIcons-latest.gif&quot;); background-position: -432px 0px;"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div unselectable="on" style="float: left; margin-top: 2px;">
                                                            <div class=" nicEdit-buttonContain" unselectable="on" style="width: 20px; height: 20px; opacity: 0.6;">
                                                                <div class=" nicEdit-button-undefined" unselectable="on" style="background-color: rgb(239, 239, 239); border: 1px solid rgb(239, 239, 239);">
                                                                    <div class=" nicEdit-button" unselectable="on" style="width: 18px; height: 18px; overflow: hidden; zoom: 1; cursor: pointer; background-image: url(&quot;http://js.nicedit.com/nicEditIcons-latest.gif&quot;); background-position: -54px 0px;"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div unselectable="on" style="float: left; margin-top: 2px;">
                                                            <div class=" nicEdit-buttonContain" unselectable="on" style="width: 20px; height: 20px; opacity: 0.6;">
                                                                <div class=" nicEdit-button-undefined" unselectable="on" style="background-color: rgb(239, 239, 239); border: 1px solid rgb(239, 239, 239);">
                                                                    <div class=" nicEdit-button" unselectable="on" style="width: 18px; height: 18px; overflow: hidden; zoom: 1; cursor: pointer; background-image: url(&quot;http://js.nicedit.com/nicEditIcons-latest.gif&quot;); background-position: -126px 0px;"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div unselectable="on" style="float: left; margin-top: 2px;">
                                                            <div class=" nicEdit-buttonContain" unselectable="on" style="width: 20px; height: 20px; opacity: 0.6;">
                                                                <div class=" nicEdit-button-undefined" unselectable="on" style="background-color: rgb(239, 239, 239); border: 1px solid rgb(239, 239, 239);">
                                                                    <div class=" nicEdit-button" unselectable="on" style="width: 18px; height: 18px; overflow: hidden; zoom: 1; cursor: pointer; background-image: url(&quot;http://js.nicedit.com/nicEditIcons-latest.gif&quot;); background-position: -342px 0px;"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div unselectable="on" style="float: left; margin-top: 2px;">
                                                            <div class=" nicEdit-buttonContain" unselectable="on" style="width: 20px; height: 20px; opacity: 0.6;">
                                                                <div class=" nicEdit-button-undefined" unselectable="on" style="background-color: rgb(239, 239, 239); border: 1px solid rgb(239, 239, 239);">
                                                                    <div class=" nicEdit-button" unselectable="on" style="width: 18px; height: 18px; overflow: hidden; zoom: 1; cursor: pointer; background-image: url(&quot;http://js.nicedit.com/nicEditIcons-latest.gif&quot;); background-position: -162px 0px;"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div unselectable="on" style="float: left; margin-top: 2px;">
                                                            <div class=" nicEdit-buttonContain" unselectable="on" style="width: 20px; height: 20px; opacity: 0.6;">
                                                                <div class=" nicEdit-button-undefined" unselectable="on" style="background-color: rgb(239, 239, 239); border: 1px solid rgb(239, 239, 239);">
                                                                    <div class=" nicEdit-button" unselectable="on" style="width: 18px; height: 18px; overflow: hidden; zoom: 1; cursor: pointer; background-image: url(&quot;http://js.nicedit.com/nicEditIcons-latest.gif&quot;); background-position: -72px 0px;"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div unselectable="on" style="float: left; margin-top: 2px;">
                                                            <div class=" nicEdit-buttonContain" unselectable="on" style="width: 20px; height: 20px; opacity: 0.6;">
                                                                <div class=" nicEdit-button-undefined" unselectable="on" style="background-color: rgb(239, 239, 239); border: 1px solid rgb(239, 239, 239);">
                                                                    <div class=" nicEdit-button" unselectable="on" style="width: 18px; height: 18px; overflow: hidden; zoom: 1; cursor: pointer; background-image: url(&quot;http://js.nicedit.com/nicEditIcons-latest.gif&quot;); background-position: -234px 0px;"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div unselectable="on" style="float: left; margin-top: 2px;">
                                                            <div class=" nicEdit-buttonContain" unselectable="on" style="width: 20px; height: 20px; opacity: 0.6;">
                                                                <div class=" nicEdit-button-undefined" unselectable="on" style="background-color: rgb(239, 239, 239); border: 1px solid rgb(239, 239, 239);">
                                                                    <div class=" nicEdit-button" unselectable="on" style="width: 18px; height: 18px; overflow: hidden; zoom: 1; cursor: pointer; background-image: url(&quot;http://js.nicedit.com/nicEditIcons-latest.gif&quot;); background-position: -144px 0px;"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div unselectable="on" style="float: left; margin-top: 2px;">
                                                            <div class=" nicEdit-buttonContain" unselectable="on" style="width: 20px; height: 20px; opacity: 0.6;">
                                                                <div class=" nicEdit-button-undefined" unselectable="on" style="background-color: rgb(239, 239, 239); border: 1px solid rgb(239, 239, 239);">
                                                                    <div class=" nicEdit-button" unselectable="on" style="width: 18px; height: 18px; overflow: hidden; zoom: 1; cursor: pointer; background-image: url(&quot;http://js.nicedit.com/nicEditIcons-latest.gif&quot;); background-position: -180px 0px;"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div unselectable="on" style="float: left; margin-top: 2px;">
                                                            <div class=" nicEdit-buttonContain" unselectable="on" style="width: 20px; height: 20px; opacity: 0.6;">
                                                                <div class=" nicEdit-button-undefined" unselectable="on" style="background-color: rgb(239, 239, 239); border: 1px solid rgb(239, 239, 239);">
                                                                    <div class=" nicEdit-button" unselectable="on" style="width: 18px; height: 18px; overflow: hidden; zoom: 1; cursor: pointer; background-image: url(&quot;http://js.nicedit.com/nicEditIcons-latest.gif&quot;); background-position: -324px 0px;"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div unselectable="on" style="float: left; margin: 2px 1px 0px;">
                                                            <div class=" nicEdit-selectContain" unselectable="on" style="width: 90px; height: 20px; cursor: pointer; overflow: hidden; opacity: 0.6;">
                                                                <div unselectable="on" style="overflow: hidden; zoom: 1; border: 1px solid rgb(204, 204, 204); padding-left: 3px; background-color: rgb(255, 255, 255);">
                                                                    <div class=" nicEdit-selectControl" unselectable="on" style="overflow: hidden; float: right; height: 18px; width: 16px; background-image: url(&quot;http://js.nicedit.com/nicEditIcons-latest.gif&quot;); background-position: -450px 0px;"></div>
                                                                    <div class=" nicEdit-selectTxt" unselectable="on" style="overflow: hidden; float: left; width: 66px; height: 14px; margin-top: 1px; font-family: sans-serif; text-align: center; font-size: 12px;">Font&nbsp;Size...</div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div unselectable="on" style="float: left; margin: 2px 1px 0px;">
                                                            <div class=" nicEdit-selectContain" unselectable="on" style="width: 90px; height: 20px; cursor: pointer; overflow: hidden; opacity: 0.6;">
                                                                <div unselectable="on" style="overflow: hidden; zoom: 1; border: 1px solid rgb(204, 204, 204); padding-left: 3px; background-color: rgb(255, 255, 255);">
                                                                    <div class=" nicEdit-selectControl" unselectable="on" style="overflow: hidden; float: right; height: 18px; width: 16px; background-image: url(&quot;http://js.nicedit.com/nicEditIcons-latest.gif&quot;); background-position: -450px 0px;"></div>
                                                                    <div class=" nicEdit-selectTxt" unselectable="on" style="overflow: hidden; float: left; width: 66px; height: 14px; margin-top: 1px; font-family: sans-serif; text-align: center; font-size: 12px;">Font&nbsp;Family...</div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div unselectable="on" style="float: left; margin: 2px 1px 0px;">
                                                            <div class=" nicEdit-selectContain" unselectable="on" style="width: 90px; height: 20px; cursor: pointer; overflow: hidden; opacity: 0.6;">
                                                                <div unselectable="on" style="overflow: hidden; zoom: 1; border: 1px solid rgb(204, 204, 204); padding-left: 3px; background-color: rgb(255, 255, 255);">
                                                                    <div class=" nicEdit-selectControl" unselectable="on" style="overflow: hidden; float: right; height: 18px; width: 16px; background-image: url(&quot;http://js.nicedit.com/nicEditIcons-latest.gif&quot;); background-position: -450px 0px;"></div>
                                                                    <div class=" nicEdit-selectTxt" unselectable="on" style="overflow: hidden; float: left; width: 66px; height: 14px; margin-top: 1px; font-family: sans-serif; text-align: center; font-size: 12px;">Font&nbsp;Format...</div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div unselectable="on" style="float: left; margin-top: 2px;">
                                                            <div class=" nicEdit-buttonContain" unselectable="on" style="width: 20px; height: 20px; opacity: 0.6;">
                                                                <div class=" nicEdit-button-undefined" unselectable="on" style="background-color: rgb(239, 239, 239); border: 1px solid rgb(239, 239, 239);">
                                                                    <div class=" nicEdit-button" unselectable="on" style="width: 18px; height: 18px; overflow: hidden; zoom: 1; cursor: pointer; background-image: url(&quot;http://js.nicedit.com/nicEditIcons-latest.gif&quot;); background-position: -108px 0px;"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div unselectable="on" style="float: left; margin-top: 2px;">
                                                            <div class=" nicEdit-buttonContain" unselectable="on" style="width: 20px; height: 20px; opacity: 0.6;">
                                                                <div class=" nicEdit-button-undefined" unselectable="on" style="background-color: rgb(239, 239, 239); border: 1px solid rgb(239, 239, 239);">
                                                                    <div class=" nicEdit-button" unselectable="on" style="width: 18px; height: 18px; overflow: hidden; zoom: 1; cursor: pointer; background-image: url(&quot;http://js.nicedit.com/nicEditIcons-latest.gif&quot;); background-position: -198px 0px;"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div style="float: left; margin-top: 2px;">
                                                            <div class=" nicEdit-buttonContain" style="width: 20px; height: 20px; opacity: 0.6;">
                                                                <div class=" nicEdit-button-undefined" style="background-color: rgb(239, 239, 239); border: 1px solid rgb(239, 239, 239);">
                                                                    <div class=" nicEdit-button" unselectable="on" style="width: 18px; height: 18px; overflow: hidden; zoom: 1; cursor: pointer; background-image: url(&quot;http://js.nicedit.com/nicEditIcons-latest.gif&quot;); background-position: -360px 0px;"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div style="float: left; margin-top: 2px;">
                                                            <div class=" nicEdit-buttonContain" style="width: 20px; height: 20px; opacity: 0.6;">
                                                                <div class=" nicEdit-button-undefined" style="background-color: rgb(239, 239, 239); border: 1px solid rgb(239, 239, 239);">
                                                                    <div class=" nicEdit-button" unselectable="on" style="width: 18px; height: 18px; overflow: hidden; zoom: 1; cursor: pointer; background-image: url(&quot;http://js.nicedit.com/nicEditIcons-latest.gif&quot;); background-position: -468px 0px;"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div style="float: left; margin-top: 2px;">
                                                            <div class=" nicEdit-buttonContain" style="width: 20px; height: 20px; opacity: 0.6;">
                                                                <div class=" nicEdit-button-undefined" style="background-color: rgb(239, 239, 239); border: 1px solid rgb(239, 239, 239);">
                                                                    <div class=" nicEdit-button" unselectable="on" style="width: 18px; height: 18px; overflow: hidden; zoom: 1; cursor: pointer; background-image: url(&quot;http://js.nicedit.com/nicEditIcons-latest.gif&quot;); background-position: -378px 0px;"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div style="float: left; margin-top: 2px;">
                                                            <div class=" nicEdit-buttonContain" style="width: 20px; height: 20px; opacity: 0.6;">
                                                                <div class=" nicEdit-button-undefined" style="background-color: rgb(239, 239, 239); border: 1px solid rgb(239, 239, 239);">
                                                                    <div class=" nicEdit-button" unselectable="on" style="width: 18px; height: 18px; overflow: hidden; zoom: 1; cursor: pointer; background-image: url(&quot;http://js.nicedit.com/nicEditIcons-latest.gif&quot;); background-position: -396px 0px;"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div style="float: left; margin-top: 2px;">
                                                            <div class=" nicEdit-buttonContain" style="width: 20px; height: 20px; opacity: 0.6;">
                                                                <div class=" nicEdit-button-undefined" style="background-color: rgb(239, 239, 239); border: 1px solid rgb(239, 239, 239);">
                                                                    <div class=" nicEdit-button" unselectable="on" style="width: 18px; height: 18px; overflow: hidden; zoom: 1; cursor: pointer; background-image: url(&quot;http://js.nicedit.com/nicEditIcons-latest.gif&quot;); background-position: -36px 0px;"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div style="float: left; margin-top: 2px;">
                                                            <div class=" nicEdit-buttonContain" style="width: 20px; height: 20px; opacity: 0.6;">
                                                                <div class=" nicEdit-button-undefined" style="background-color: rgb(239, 239, 239); border: 1px solid rgb(239, 239, 239);">
                                                                    <div class=" nicEdit-button" unselectable="on" style="width: 18px; height: 18px; overflow: hidden; zoom: 1; cursor: pointer; background-image: url(&quot;http://js.nicedit.com/nicEditIcons-latest.gif&quot;); background-position: -18px 0px;"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div unselectable="on" style="float: left; margin-top: 2px;">
                                                            <div class=" nicEdit-buttonContain" unselectable="on" style="width: 20px; height: 20px; opacity: 0.6;">
                                                                <div class=" nicEdit-button-undefined" unselectable="on" style="background-color: rgb(239, 239, 239); border: 1px solid rgb(239, 239, 239);">
                                                                    <div class=" nicEdit-button" unselectable="on" style="width: 18px; height: 18px; overflow: hidden; zoom: 1; cursor: pointer; background-image: url(&quot;http://js.nicedit.com/nicEditIcons-latest.gif&quot;); background-position: -288px 0px;"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div unselectable="on" style="float: left; margin-top: 2px;">
                                                            <div class=" nicEdit-buttonContain" unselectable="on" style="width: 20px; height: 20px; opacity: 0.6;">
                                                                <div class=" nicEdit-button-undefined" unselectable="on" style="background-color: rgb(239, 239, 239); border: 1px solid rgb(239, 239, 239);">
                                                                    <div class=" nicEdit-button" unselectable="on" style="width: 18px; height: 18px; overflow: hidden; zoom: 1; cursor: pointer; background-image: url(&quot;http://js.nicedit.com/nicEditIcons-latest.gif&quot;); background-position: -306px 0px;"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div unselectable="on" style="float: left; margin-top: 2px;">
                                                            <div class=" nicEdit-buttonContain" unselectable="on" style="width: 20px; height: 20px; opacity: 0.6;">
                                                                <div class=" nicEdit-button-undefined" unselectable="on" style="background-color: rgb(239, 239, 239); border: 1px solid rgb(239, 239, 239);">
                                                                    <div class=" nicEdit-button" unselectable="on" style="width: 18px; height: 18px; overflow: hidden; zoom: 1; cursor: pointer; background-image: url(&quot;http://js.nicedit.com/nicEditIcons-latest.gif&quot;); background-position: -270px 0px;"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div unselectable="on" style="float: left; margin-top: 2px;">
                                                            <div class=" nicEdit-buttonContain" unselectable="on" style="width: 20px; height: 20px; opacity: 0.6;">
                                                                <div class=" nicEdit-button-undefined" unselectable="on" style="background-color: rgb(239, 239, 239); border: 1px solid rgb(239, 239, 239);">
                                                                    <div class=" nicEdit-button" unselectable="on" style="width: 18px; height: 18px; overflow: hidden; zoom: 1; cursor: pointer; background-image: url(&quot;http://js.nicedit.com/nicEditIcons-latest.gif&quot;); background-position: -216px 0px;"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div unselectable="on" style="float: left; margin-top: 2px;">
                                                            <div class=" nicEdit-buttonContain" unselectable="on" style="width: 20px; height: 20px; opacity: 0.6;">
                                                                <div class=" nicEdit-button-undefined" unselectable="on" style="background-color: rgb(239, 239, 239); border: 1px solid rgb(239, 239, 239);">
                                                                    <div class=" nicEdit-button" unselectable="on" style="width: 18px; height: 18px; overflow: hidden; zoom: 1; cursor: pointer; background-image: url(&quot;http://js.nicedit.com/nicEditIcons-latest.gif&quot;); background-position: -90px 0px;"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div style="float: left; margin-top: 2px;">
                                                            <div class=" nicEdit-buttonContain" style="width: 20px; height: 20px; opacity: 0.6;">
                                                                <div class=" nicEdit-button-undefined" style="background-color: rgb(239, 239, 239); border: 1px solid rgb(239, 239, 239);">
                                                                    <div class=" nicEdit-button" unselectable="on" style="width: 18px; height: 18px; overflow: hidden; zoom: 1; cursor: pointer; background-image: url(&quot;http://js.nicedit.com/nicEditIcons-latest.gif&quot;); background-position: 0px 0px;"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div style="width: 100%; border-width: 0px 1px 1px; border-top-style: initial; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-color: initial; border-right-color: rgb(204, 204, 204); border-bottom-color: rgb(204, 204, 204); border-left-color: rgb(204, 204, 204); border-image: initial; overflow: hidden auto;">
                                                <div class=" nicEdit-main" contenteditable="true" style="width: 182px; margin: 4px; min-height: 54px; overflow: hidden;">
                                                    <p style="margin-right: 0px; margin-bottom: 15px; margin-left: 0px; padding: 0px; text-align: justify; color: rgb(0, 0, 0); font-family: " open="" sans",="" arial,="" sans-serif;="" font-size:="" 14px;"="">Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, "Lorem ipsum dolor sit amet..", comes from a line in section 1.10.32.</p>
                                                    <p style="margin-right: 0px; margin-bottom: 15px; margin-left: 0px; padding: 0px; text-align: justify; color: rgb(0, 0, 0); font-family: " open="" sans",="" arial,="" sans-serif;="" font-size:="" 14px;"="">The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from "de Finibus Bonorum et Malorum" by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.</p>
                                                </div>
                                            </div><textarea name="details" class="nic-edit-p" style="display: none;">&lt;p style="margin-right: 0px; margin-bottom: 15px; margin-left: 0px; padding: 0px; text-align: justify; color: rgb(0, 0, 0); font-family: " open="" sans",="" arial,="" sans-serif;="" font-size:="" 14px;"=""&gt;Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, "Lorem ipsum dolor sit amet..", comes from a line in section 1.10.32.&lt;/p&gt;&lt;p style="margin-right: 0px; margin-bottom: 15px; margin-left: 0px; padding: 0px; text-align: justify; color: rgb(0, 0, 0); font-family: " open="" sans",="" arial,="" sans-serif;="" font-size:="" 14px;"=""&gt;The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from "de Finibus Bonorum et Malorum" by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.&lt;/p&gt;</textarea>
                                        </div>
                                    </div>
                                </div>



                                <div class="row">
                                    <div class="col-lg-4">
                                        <div class="left-area">
                                            <h4 class="heading">
                                                Product Buy/Return Policy*
                                            </h4>
                                        </div>
                                    </div>
                                    <div class="col-lg-7">
                                        <div class="text-editor">
                                            <div unselectable="on" style="width: 100%;">
                                                <div class=" nicEdit-panelContain" unselectable="on" style="overflow: hidden; width: 100%; border: 1px solid rgb(204, 204, 204); background-color: rgb(239, 239, 239);">
                                                    <div class=" nicEdit-panel" unselectable="on" style="margin: 0px 2px 2px; zoom: 1; overflow: hidden;">
                                                        <div style="float: left; margin-top: 2px; display: none;">
                                                            <div class=" nicEdit-buttonContain" style="width: 20px; height: 20px; opacity: 0.6;">
                                                                <div class=" nicEdit-button-undefined" style="background-color: rgb(239, 239, 239); border: 1px solid rgb(239, 239, 239);">
                                                                    <div class=" nicEdit-button" unselectable="on" style="width: 18px; height: 18px; overflow: hidden; zoom: 1; cursor: pointer; background-image: url(&quot;http://js.nicedit.com/nicEditIcons-latest.gif&quot;); background-position: -432px 0px;"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div unselectable="on" style="float: left; margin-top: 2px;">
                                                            <div class=" nicEdit-buttonContain" unselectable="on" style="width: 20px; height: 20px; opacity: 0.6;">
                                                                <div class=" nicEdit-button-undefined" unselectable="on" style="background-color: rgb(239, 239, 239); border: 1px solid rgb(239, 239, 239);">
                                                                    <div class=" nicEdit-button" unselectable="on" style="width: 18px; height: 18px; overflow: hidden; zoom: 1; cursor: pointer; background-image: url(&quot;http://js.nicedit.com/nicEditIcons-latest.gif&quot;); background-position: -54px 0px;"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div unselectable="on" style="float: left; margin-top: 2px;">
                                                            <div class=" nicEdit-buttonContain" unselectable="on" style="width: 20px; height: 20px; opacity: 0.6;">
                                                                <div class=" nicEdit-button-undefined" unselectable="on" style="background-color: rgb(239, 239, 239); border: 1px solid rgb(239, 239, 239);">
                                                                    <div class=" nicEdit-button" unselectable="on" style="width: 18px; height: 18px; overflow: hidden; zoom: 1; cursor: pointer; background-image: url(&quot;http://js.nicedit.com/nicEditIcons-latest.gif&quot;); background-position: -126px 0px;"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div unselectable="on" style="float: left; margin-top: 2px;">
                                                            <div class=" nicEdit-buttonContain" unselectable="on" style="width: 20px; height: 20px; opacity: 0.6;">
                                                                <div class=" nicEdit-button-undefined" unselectable="on" style="background-color: rgb(239, 239, 239); border: 1px solid rgb(239, 239, 239);">
                                                                    <div class=" nicEdit-button" unselectable="on" style="width: 18px; height: 18px; overflow: hidden; zoom: 1; cursor: pointer; background-image: url(&quot;http://js.nicedit.com/nicEditIcons-latest.gif&quot;); background-position: -342px 0px;"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div unselectable="on" style="float: left; margin-top: 2px;">
                                                            <div class=" nicEdit-buttonContain" unselectable="on" style="width: 20px; height: 20px; opacity: 0.6;">
                                                                <div class=" nicEdit-button-undefined" unselectable="on" style="background-color: rgb(239, 239, 239); border: 1px solid rgb(239, 239, 239);">
                                                                    <div class=" nicEdit-button" unselectable="on" style="width: 18px; height: 18px; overflow: hidden; zoom: 1; cursor: pointer; background-image: url(&quot;http://js.nicedit.com/nicEditIcons-latest.gif&quot;); background-position: -162px 0px;"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div unselectable="on" style="float: left; margin-top: 2px;">
                                                            <div class=" nicEdit-buttonContain" unselectable="on" style="width: 20px; height: 20px; opacity: 0.6;">
                                                                <div class=" nicEdit-button-undefined" unselectable="on" style="background-color: rgb(239, 239, 239); border: 1px solid rgb(239, 239, 239);">
                                                                    <div class=" nicEdit-button" unselectable="on" style="width: 18px; height: 18px; overflow: hidden; zoom: 1; cursor: pointer; background-image: url(&quot;http://js.nicedit.com/nicEditIcons-latest.gif&quot;); background-position: -72px 0px;"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div unselectable="on" style="float: left; margin-top: 2px;">
                                                            <div class=" nicEdit-buttonContain" unselectable="on" style="width: 20px; height: 20px; opacity: 0.6;">
                                                                <div class=" nicEdit-button-undefined" unselectable="on" style="background-color: rgb(239, 239, 239); border: 1px solid rgb(239, 239, 239);">
                                                                    <div class=" nicEdit-button" unselectable="on" style="width: 18px; height: 18px; overflow: hidden; zoom: 1; cursor: pointer; background-image: url(&quot;http://js.nicedit.com/nicEditIcons-latest.gif&quot;); background-position: -234px 0px;"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div unselectable="on" style="float: left; margin-top: 2px;">
                                                            <div class=" nicEdit-buttonContain" unselectable="on" style="width: 20px; height: 20px; opacity: 0.6;">
                                                                <div class=" nicEdit-button-undefined" unselectable="on" style="background-color: rgb(239, 239, 239); border: 1px solid rgb(239, 239, 239);">
                                                                    <div class=" nicEdit-button" unselectable="on" style="width: 18px; height: 18px; overflow: hidden; zoom: 1; cursor: pointer; background-image: url(&quot;http://js.nicedit.com/nicEditIcons-latest.gif&quot;); background-position: -144px 0px;"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div unselectable="on" style="float: left; margin-top: 2px;">
                                                            <div class=" nicEdit-buttonContain" unselectable="on" style="width: 20px; height: 20px; opacity: 0.6;">
                                                                <div class=" nicEdit-button-undefined" unselectable="on" style="background-color: rgb(239, 239, 239); border: 1px solid rgb(239, 239, 239);">
                                                                    <div class=" nicEdit-button" unselectable="on" style="width: 18px; height: 18px; overflow: hidden; zoom: 1; cursor: pointer; background-image: url(&quot;http://js.nicedit.com/nicEditIcons-latest.gif&quot;); background-position: -180px 0px;"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div unselectable="on" style="float: left; margin-top: 2px;">
                                                            <div class=" nicEdit-buttonContain" unselectable="on" style="width: 20px; height: 20px; opacity: 0.6;">
                                                                <div class=" nicEdit-button-undefined" unselectable="on" style="background-color: rgb(239, 239, 239); border: 1px solid rgb(239, 239, 239);">
                                                                    <div class=" nicEdit-button" unselectable="on" style="width: 18px; height: 18px; overflow: hidden; zoom: 1; cursor: pointer; background-image: url(&quot;http://js.nicedit.com/nicEditIcons-latest.gif&quot;); background-position: -324px 0px;"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div unselectable="on" style="float: left; margin: 2px 1px 0px;">
                                                            <div class=" nicEdit-selectContain" unselectable="on" style="width: 90px; height: 20px; cursor: pointer; overflow: hidden; opacity: 0.6;">
                                                                <div unselectable="on" style="overflow: hidden; zoom: 1; border: 1px solid rgb(204, 204, 204); padding-left: 3px; background-color: rgb(255, 255, 255);">
                                                                    <div class=" nicEdit-selectControl" unselectable="on" style="overflow: hidden; float: right; height: 18px; width: 16px; background-image: url(&quot;http://js.nicedit.com/nicEditIcons-latest.gif&quot;); background-position: -450px 0px;"></div>
                                                                    <div class=" nicEdit-selectTxt" unselectable="on" style="overflow: hidden; float: left; width: 66px; height: 14px; margin-top: 1px; font-family: sans-serif; text-align: center; font-size: 12px;">Font&nbsp;Size...</div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div unselectable="on" style="float: left; margin: 2px 1px 0px;">
                                                            <div class=" nicEdit-selectContain" unselectable="on" style="width: 90px; height: 20px; cursor: pointer; overflow: hidden; opacity: 0.6;">
                                                                <div unselectable="on" style="overflow: hidden; zoom: 1; border: 1px solid rgb(204, 204, 204); padding-left: 3px; background-color: rgb(255, 255, 255);">
                                                                    <div class=" nicEdit-selectControl" unselectable="on" style="overflow: hidden; float: right; height: 18px; width: 16px; background-image: url(&quot;http://js.nicedit.com/nicEditIcons-latest.gif&quot;); background-position: -450px 0px;"></div>
                                                                    <div class=" nicEdit-selectTxt" unselectable="on" style="overflow: hidden; float: left; width: 66px; height: 14px; margin-top: 1px; font-family: sans-serif; text-align: center; font-size: 12px;">Font&nbsp;Family...</div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div unselectable="on" style="float: left; margin: 2px 1px 0px;">
                                                            <div class=" nicEdit-selectContain" unselectable="on" style="width: 90px; height: 20px; cursor: pointer; overflow: hidden; opacity: 0.6;">
                                                                <div unselectable="on" style="overflow: hidden; zoom: 1; border: 1px solid rgb(204, 204, 204); padding-left: 3px; background-color: rgb(255, 255, 255);">
                                                                    <div class=" nicEdit-selectControl" unselectable="on" style="overflow: hidden; float: right; height: 18px; width: 16px; background-image: url(&quot;http://js.nicedit.com/nicEditIcons-latest.gif&quot;); background-position: -450px 0px;"></div>
                                                                    <div class=" nicEdit-selectTxt" unselectable="on" style="overflow: hidden; float: left; width: 66px; height: 14px; margin-top: 1px; font-family: sans-serif; text-align: center; font-size: 12px;">Font&nbsp;Format...</div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div unselectable="on" style="float: left; margin-top: 2px;">
                                                            <div class=" nicEdit-buttonContain" unselectable="on" style="width: 20px; height: 20px; opacity: 0.6;">
                                                                <div class=" nicEdit-button-undefined" unselectable="on" style="background-color: rgb(239, 239, 239); border: 1px solid rgb(239, 239, 239);">
                                                                    <div class=" nicEdit-button" unselectable="on" style="width: 18px; height: 18px; overflow: hidden; zoom: 1; cursor: pointer; background-image: url(&quot;http://js.nicedit.com/nicEditIcons-latest.gif&quot;); background-position: -108px 0px;"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div unselectable="on" style="float: left; margin-top: 2px;">
                                                            <div class=" nicEdit-buttonContain" unselectable="on" style="width: 20px; height: 20px; opacity: 0.6;">
                                                                <div class=" nicEdit-button-undefined" unselectable="on" style="background-color: rgb(239, 239, 239); border: 1px solid rgb(239, 239, 239);">
                                                                    <div class=" nicEdit-button" unselectable="on" style="width: 18px; height: 18px; overflow: hidden; zoom: 1; cursor: pointer; background-image: url(&quot;http://js.nicedit.com/nicEditIcons-latest.gif&quot;); background-position: -198px 0px;"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div style="float: left; margin-top: 2px;">
                                                            <div class=" nicEdit-buttonContain" style="width: 20px; height: 20px; opacity: 0.6;">
                                                                <div class=" nicEdit-button-undefined" style="background-color: rgb(239, 239, 239); border: 1px solid rgb(239, 239, 239);">
                                                                    <div class=" nicEdit-button" unselectable="on" style="width: 18px; height: 18px; overflow: hidden; zoom: 1; cursor: pointer; background-image: url(&quot;http://js.nicedit.com/nicEditIcons-latest.gif&quot;); background-position: -360px 0px;"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div style="float: left; margin-top: 2px;">
                                                            <div class=" nicEdit-buttonContain" style="width: 20px; height: 20px; opacity: 0.6;">
                                                                <div class=" nicEdit-button-undefined" style="background-color: rgb(239, 239, 239); border: 1px solid rgb(239, 239, 239);">
                                                                    <div class=" nicEdit-button" unselectable="on" style="width: 18px; height: 18px; overflow: hidden; zoom: 1; cursor: pointer; background-image: url(&quot;http://js.nicedit.com/nicEditIcons-latest.gif&quot;); background-position: -468px 0px;"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div style="float: left; margin-top: 2px;">
                                                            <div class=" nicEdit-buttonContain" style="width: 20px; height: 20px; opacity: 0.6;">
                                                                <div class=" nicEdit-button-undefined" style="background-color: rgb(239, 239, 239); border: 1px solid rgb(239, 239, 239);">
                                                                    <div class=" nicEdit-button" unselectable="on" style="width: 18px; height: 18px; overflow: hidden; zoom: 1; cursor: pointer; background-image: url(&quot;http://js.nicedit.com/nicEditIcons-latest.gif&quot;); background-position: -378px 0px;"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div style="float: left; margin-top: 2px;">
                                                            <div class=" nicEdit-buttonContain" style="width: 20px; height: 20px; opacity: 0.6;">
                                                                <div class=" nicEdit-button-undefined" style="background-color: rgb(239, 239, 239); border: 1px solid rgb(239, 239, 239);">
                                                                    <div class=" nicEdit-button" unselectable="on" style="width: 18px; height: 18px; overflow: hidden; zoom: 1; cursor: pointer; background-image: url(&quot;http://js.nicedit.com/nicEditIcons-latest.gif&quot;); background-position: -396px 0px;"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div style="float: left; margin-top: 2px;">
                                                            <div class=" nicEdit-buttonContain" style="width: 20px; height: 20px; opacity: 0.6;">
                                                                <div class=" nicEdit-button-undefined" style="background-color: rgb(239, 239, 239); border: 1px solid rgb(239, 239, 239);">
                                                                    <div class=" nicEdit-button" unselectable="on" style="width: 18px; height: 18px; overflow: hidden; zoom: 1; cursor: pointer; background-image: url(&quot;http://js.nicedit.com/nicEditIcons-latest.gif&quot;); background-position: -36px 0px;"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div style="float: left; margin-top: 2px;">
                                                            <div class=" nicEdit-buttonContain" style="width: 20px; height: 20px; opacity: 0.6;">
                                                                <div class=" nicEdit-button-undefined" style="background-color: rgb(239, 239, 239); border: 1px solid rgb(239, 239, 239);">
                                                                    <div class=" nicEdit-button" unselectable="on" style="width: 18px; height: 18px; overflow: hidden; zoom: 1; cursor: pointer; background-image: url(&quot;http://js.nicedit.com/nicEditIcons-latest.gif&quot;); background-position: -18px 0px;"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div unselectable="on" style="float: left; margin-top: 2px;">
                                                            <div class=" nicEdit-buttonContain" unselectable="on" style="width: 20px; height: 20px; opacity: 0.6;">
                                                                <div class=" nicEdit-button-undefined" unselectable="on" style="background-color: rgb(239, 239, 239); border: 1px solid rgb(239, 239, 239);">
                                                                    <div class=" nicEdit-button" unselectable="on" style="width: 18px; height: 18px; overflow: hidden; zoom: 1; cursor: pointer; background-image: url(&quot;http://js.nicedit.com/nicEditIcons-latest.gif&quot;); background-position: -288px 0px;"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div unselectable="on" style="float: left; margin-top: 2px;">
                                                            <div class=" nicEdit-buttonContain" unselectable="on" style="width: 20px; height: 20px; opacity: 0.6;">
                                                                <div class=" nicEdit-button-undefined" unselectable="on" style="background-color: rgb(239, 239, 239); border: 1px solid rgb(239, 239, 239);">
                                                                    <div class=" nicEdit-button" unselectable="on" style="width: 18px; height: 18px; overflow: hidden; zoom: 1; cursor: pointer; background-image: url(&quot;http://js.nicedit.com/nicEditIcons-latest.gif&quot;); background-position: -306px 0px;"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div unselectable="on" style="float: left; margin-top: 2px;">
                                                            <div class=" nicEdit-buttonContain" unselectable="on" style="width: 20px; height: 20px; opacity: 0.6;">
                                                                <div class=" nicEdit-button-undefined" unselectable="on" style="background-color: rgb(239, 239, 239); border: 1px solid rgb(239, 239, 239);">
                                                                    <div class=" nicEdit-button" unselectable="on" style="width: 18px; height: 18px; overflow: hidden; zoom: 1; cursor: pointer; background-image: url(&quot;http://js.nicedit.com/nicEditIcons-latest.gif&quot;); background-position: -270px 0px;"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div unselectable="on" style="float: left; margin-top: 2px;">
                                                            <div class=" nicEdit-buttonContain" unselectable="on" style="width: 20px; height: 20px; opacity: 0.6;">
                                                                <div class=" nicEdit-button-undefined" unselectable="on" style="background-color: rgb(239, 239, 239); border: 1px solid rgb(239, 239, 239);">
                                                                    <div class=" nicEdit-button" unselectable="on" style="width: 18px; height: 18px; overflow: hidden; zoom: 1; cursor: pointer; background-image: url(&quot;http://js.nicedit.com/nicEditIcons-latest.gif&quot;); background-position: -216px 0px;"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div unselectable="on" style="float: left; margin-top: 2px;">
                                                            <div class=" nicEdit-buttonContain" unselectable="on" style="width: 20px; height: 20px; opacity: 0.6;">
                                                                <div class=" nicEdit-button-undefined" unselectable="on" style="background-color: rgb(239, 239, 239); border: 1px solid rgb(239, 239, 239);">
                                                                    <div class=" nicEdit-button" unselectable="on" style="width: 18px; height: 18px; overflow: hidden; zoom: 1; cursor: pointer; background-image: url(&quot;http://js.nicedit.com/nicEditIcons-latest.gif&quot;); background-position: -90px 0px;"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div style="float: left; margin-top: 2px;">
                                                            <div class=" nicEdit-buttonContain" style="width: 20px; height: 20px; opacity: 0.6;">
                                                                <div class=" nicEdit-button-undefined" style="background-color: rgb(239, 239, 239); border: 1px solid rgb(239, 239, 239);">
                                                                    <div class=" nicEdit-button" unselectable="on" style="width: 18px; height: 18px; overflow: hidden; zoom: 1; cursor: pointer; background-image: url(&quot;http://js.nicedit.com/nicEditIcons-latest.gif&quot;); background-position: 0px 0px;"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div style="width: 100%; border-width: 0px 1px 1px; border-top-style: initial; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-color: initial; border-right-color: rgb(204, 204, 204); border-bottom-color: rgb(204, 204, 204); border-left-color: rgb(204, 204, 204); border-image: initial; overflow: hidden auto;">
                                                <div class=" nicEdit-main" contenteditable="true" style="width: 182px; margin: 4px; min-height: 54px; overflow: hidden;">
                                                    <p style="margin-right: 0px; margin-bottom: 15px; margin-left: 0px; padding: 0px; text-align: justify; color: rgb(0, 0, 0); font-family: " open="" sans",="" arial,="" sans-serif;="" font-size:="" 14px;"="">Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, "Lorem ipsum dolor sit amet..", comes from a line in section 1.10.32.</p>
                                                    <p style="margin-right: 0px; margin-bottom: 15px; margin-left: 0px; padding: 0px; text-align: justify; color: rgb(0, 0, 0); font-family: " open="" sans",="" arial,="" sans-serif;="" font-size:="" 14px;"="">The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from "de Finibus Bonorum et Malorum" by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.</p>
                                                </div>
                                            </div><textarea name="policy" class="nic-edit-p" style="display: none;">&lt;p style="margin-right: 0px; margin-bottom: 15px; margin-left: 0px; padding: 0px; text-align: justify; color: rgb(0, 0, 0); font-family: " open="" sans",="" arial,="" sans-serif;="" font-size:="" 14px;"=""&gt;Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, "Lorem ipsum dolor sit amet..", comes from a line in section 1.10.32.&lt;/p&gt;&lt;p style="margin-right: 0px; margin-bottom: 15px; margin-left: 0px; padding: 0px; text-align: justify; color: rgb(0, 0, 0); font-family: " open="" sans",="" arial,="" sans-serif;="" font-size:="" 14px;"=""&gt;The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from "de Finibus Bonorum et Malorum" by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.&lt;/p&gt;</textarea>
                                        </div>
                                    </div>
                                </div>


                                <div class="row">
                                    <div class="col-lg-4">
                                        <div class="left-area">
                                            <h4 class="heading">Youtube Video URL*</h4>
                                            <p class="sub-heading">(Optional)</p>
                                        </div>
                                    </div>
                                    <div class="col-lg-7">
                                        <input name="youtube" type="text" class="input-field" placeholder="Enter Youtube Video URL" value="https://www.youtube.com/watch?v=HxNydN5tScI">
                                        <div class="checkbox-wrapper">
                                            <input type="checkbox" name="seo_check" value="1" class="checkclick" id="allowProductSEO" checked="">
                                            <label for="allowProductSEO">Allow Product SEO</label>
                                        </div>
                                    </div>
                                </div>



                                <div class="">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="left-area">
                                                <h4 class="heading">Meta Tags *</h4>
                                            </div>
                                        </div>
                                        <div class="col-lg-7">
                                            <ul id="metatags" class="myTags tagit ui-widget ui-widget-content ui-corner-all">





                                                <li class="tagit-choice ui-widget-content ui-state-default ui-corner-all tagit-choice-editable"><span class="tagit-label">clothing</span><a class="tagit-close"><span class="text-icon">×</span><span class="ui-icon ui-icon-close"></span></a><input type="hidden" value="clothing" name="meta_tag[]" class="tagit-hidden-field"></li>
                                                <li class="tagit-choice ui-widget-content ui-state-default ui-corner-all tagit-choice-editable"><span class="tagit-label">bag</span><a class="tagit-close"><span class="text-icon">×</span><span class="ui-icon ui-icon-close"></span></a><input type="hidden" value="bag" name="meta_tag[]" class="tagit-hidden-field"></li>
                                                <li class="tagit-choice ui-widget-content ui-state-default ui-corner-all tagit-choice-editable"><span class="tagit-label">js</span><a class="tagit-close"><span class="text-icon">×</span><span class="ui-icon ui-icon-close"></span></a><input type="hidden" value="js" name="meta_tag[]" class="tagit-hidden-field"></li>
                                                <li class="tagit-choice ui-widget-content ui-state-default ui-corner-all tagit-choice-editable"><span class="tagit-label">css</span><a class="tagit-close"><span class="text-icon">×</span><span class="ui-icon ui-icon-close"></span></a><input type="hidden" value="css" name="meta_tag[]" class="tagit-hidden-field"></li>
                                                <li class="tagit-choice ui-widget-content ui-state-default ui-corner-all tagit-choice-editable"><span class="tagit-label">php</span><a class="tagit-close"><span class="text-icon">×</span><span class="ui-icon ui-icon-close"></span></a><input type="hidden" value="php" name="meta_tag[]" class="tagit-hidden-field"></li>
                                                <li class="tagit-new"><input type="text" class="ui-widget-content ui-autocomplete-input" autocomplete="off"></li>
                                            </ul>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="left-area">
                                                <h4 class="heading">
                                                    Meta Description *
                                                </h4>
                                            </div>
                                        </div>
                                        <div class="col-lg-7">
                                            <div class="text-editor">
                                                <textarea name="meta_description" class="input-field" placeholder="Details">clothing, bag</textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-lg-4">
                                        <div class="left-area">

                                        </div>
                                    </div>
                                    <div class="col-lg-7">
                                        <div class="featured-keyword-area">
                                            <div class="heading-area">
                                                <h4 class="title">Feature Tags</h4>
                                            </div>

                                            <div class="feature-tag-top-filds" id="feature-section">

                                                <div class="feature-area">
                                                    <span class="remove feature-remove"><i class="fas fa-times"></i></span>
                                                    <div class="row">
                                                        <div class="col-lg-6">
                                                            <input type="text" name="features[]" class="input-field" placeholder="Enter Your Keyword">
                                                        </div>

                                                        <div class="col-lg-6">
                                                            <div class="input-group colorpicker-component cp colorpicker-element">
                                                                <input type="text" name="colors[]" value="#000000" class="input-field cp colorpicker-element">
                                                                <span class="input-group-addon"><i style="background-color: rgb(0, 0, 0);"></i></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>

                                            <a href="javascript:;" id="feature-btn" class="add-fild-btn"><i class="icofont-plus"></i> Add More Field</a>
                                        </div>
                                    </div>
                                </div>


                                <div class="row">
                                    <div class="col-lg-4">
                                        <div class="left-area">
                                            <h4 class="heading">Tags *</h4>
                                        </div>
                                    </div>
                                    <div class="col-lg-7">
                                        <ul id="tags" class="myTags tagit ui-widget ui-widget-content ui-corner-all">


                                            <li class="tagit-choice ui-widget-content ui-state-default ui-corner-all tagit-choice-editable"><span class="tagit-label">clothing</span><a class="tagit-close"><span class="text-icon">×</span><span class="ui-icon ui-icon-close"></span></a><input type="hidden" value="clothing" name="tags[]" class="tagit-hidden-field"></li>
                                            <li class="tagit-choice ui-widget-content ui-state-default ui-corner-all tagit-choice-editable"><span class="tagit-label">bag</span><a class="tagit-close"><span class="text-icon">×</span><span class="ui-icon ui-icon-close"></span></a><input type="hidden" value="bag" name="tags[]" class="tagit-hidden-field"></li>
                                            <li class="tagit-new"><input type="text" class="ui-widget-content ui-autocomplete-input" autocomplete="off"></li>
                                        </ul>
                                    </div>
                                </div>



                                <div class="row">
                                    <div class="col-lg-4">
                                        <div class="left-area">

                                        </div>
                                    </div>
                                    <div class="col-lg-7 text-center">
                                        <button class="addProductSubmit-btn" type="submit">Save</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</x-admin_master>