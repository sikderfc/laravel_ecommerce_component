<x-admin_master>
    <div class="content-area" id="modalEdit">
        <div class="mr-breadcrumb">
            <div class="row">
                <div class="col-lg-12">
                    <h4 class="heading">Show Product</h4>
                    <ul class="links">
                        <li>
                            <a href="https://royalscripts.com/demo/kingcommerce/admin">Dashboard </a>
                        </li>
                        <li>
                            <a href="javascript:;">Show Product </a>
                        </li>
                        <li>
                            <a href="https://royalscripts.com/demo/kingcommerce/admin/products">All Products</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="product-area">
            <div class="row">
                <div class="col-lg-12">
                    <div class="mr-table allproduct">

                        <div class="alert alert-success validation" style="display: none;">
                            <button type="button" class="close alert-close"><span>×</span></button>
                            <p class="text-left"></p>
                        </div>

                        <div class="table-responsiv">
                            <div id="geniustable_wrapper" class="dataTables_wrapper dt-bootstrap4 no-footer">
                                <div class="row btn-area">
                                    <div class="col-sm-4">
                                        <div class="dataTables_length" id="geniustable_length"><label>Show <select name="geniustable_length" aria-controls="geniustable" class="custom-select custom-select-sm form-control form-control-sm">
                                                    <option product_liste="10">10</option>
                                                    <option product_liste="25">25</option>
                                                    <option product_liste="50">50</option>
                                                    <option product_liste="100">100</option>
                                                </select> entries</label></div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div id="geniustable_filter" class="dataTables_filter"><label>Search:<input type="search" class="form-control form-control-sm" placeholder="" aria-controls="geniustable"></label></div>
                                    </div>
                                    <div class="col-sm-4 table-contents"><a class="add-btn" href="{{url('add_product')}}"><i class="fas fa-plus"></i> <span class="remove-mobile">Add New Product<span></span></span></a></div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <table id="geniustable" class="table table-hover dt-responsive dataTable no-footer dtr-inline" cellspacing="0" width="100%" role="grid" aria-describedby="geniustable_info" style="width: 100%;">
                                            <thead>
                                                <tr role="row">
                                                    <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 56px;">Sl#</th>
                                                    <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 426px;">Name</th>
                                                    <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 66px;">Current Price</th>
                                                    <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 66px;">Privious Price</th>
                                                    <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 66px;">tag</th>

                                                    <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 66px;">Description</th>
                                                    <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 145px;">Status</th>
                                                    <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 128px;">Options</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr role="row" class="even">
                                                    <td></td>
                                                    <td tabindex="0">{{$product_list->product_name}}<br><small>ID: {{$product_list->current_stock}}</small><small class="ml-2"> SKU: {{$product_list->product_squ}}</small>
                                                        </td>
                                                    <td>{{$product_list->current_price}}</td>
                                                    <td>{{$product_list->privious_price}}</td>
                                                    <td>{{$product_list->tag}}</td>
                                                    <td>{{$product_list->description}}</td>
                                                    
                                                    <td>
                                                <div class="action-list"><select class="process select droplinks drop-success" style="display: none;">
                                                                <option data-val="1" product_liste="https://royalscripts.com/demo/kingcommerce/admin/products/status/181/1" selected="">Activated</option>&lt;<option data-val="0" product_liste="https://royalscripts.com/demo/kingcommerce/admin/products/status/181/0">Deactivated</option>/select&gt;
                                                            </select>
                                                            <div class="nice-select process select droplinks drop-success" tabindex="0"><span class="current">Activated</span>
                                                                <ul class="list">
                                                                    <li data-product_liste="https://royalscripts.com/demo/kingcommerce/admin/products/status/181/1" class="option selected">Activated</li>
                                                                    <li data-product_liste="https://royalscripts.com/demo/kingcommerce/admin/products/status/181/0" class="option">Deactivated</li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td></td>
                                                   
                                                </tr>
                                               
                                            </tbody>
                                        </table>
                                        <div id="geniustable_processing" class="dataTables_processing card" style="display: none;"><img src="https://royalscripts.com/demo/kingcommerce/assets/images/1564224329loading3.gif"></div>
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</x-admin_master>