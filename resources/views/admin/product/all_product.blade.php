<x-admin_master>
    <div class="content-area" id="modalEdit">
        <div class="mr-breadcrumb">
            <div class="row">
                <div class="col-lg-12">
                    <h4 class="heading">Products</h4>
                    <ul class="links">
                        <li>
                            <a href="https://royalscripts.com/demo/kingcommerce/admin">Dashboard </a>
                        </li>
                        <li>
                            <a href="javascript:;">Products </a>
                        </li>
                        <li>
                            <a href="https://royalscripts.com/demo/kingcommerce/admin/products">All Products</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="product-area">
            <div class="row">
                <div class="col-lg-12">
                    <div class="mr-table allproduct">

                        <div class="alert alert-success validation" style="display: none;">
                            <button type="button" class="close alert-close"><span>×</span></button>
                            <p class="text-left"></p>
                        </div>

                        <div class="table-responsiv">
                            <div id="geniustable_wrapper" class="dataTables_wrapper dt-bootstrap4 no-footer">
                                <div class="row btn-area">
                                    <div class="col-sm-4">
                                        <div class="dataTables_length" id="geniustable_length"><label>Show <select name="geniustable_length" aria-controls="geniustable" class="custom-select custom-select-sm form-control form-control-sm">
                                                    <option value="10">10</option>
                                                    <option value="25">25</option>
                                                    <option value="50">50</option>
                                                    <option value="100">100</option>
                                                </select> entries</label></div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div id="geniustable_filter" class="dataTables_filter"><label>Search:<input type="search" class="form-control form-control-sm" placeholder="" aria-controls="geniustable"></label></div>
                                    </div>
                                    <div class="col-sm-4 table-contents"><a class="add-btn" href="{{url('add_product')}}"><i class="fas fa-plus"></i> <span class="remove-mobile">Add New Product<span></span></span></a></div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <table id="geniustable" class="table table-hover dt-responsive dataTable no-footer dtr-inline" cellspacing="0" width="100%" role="grid" aria-describedby="geniustable_info" style="width: 100%;">
                                            <thead>
                                                <tr role="row">
                                                    <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 56px;">Sl#</th>
                                                    <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 426px;">Name</th>
                                                    <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 77px;">Stock</th>
                                                    <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 66px;">Price</th>
                                                    <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 66px;">tag</th>
                                                    <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 145px;">Status</th>
                                                    <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 128px;">Options</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach($all_products_data as $valu)
                                                <tr role="row" class="even">
                                                    <td>{{$loop->iteration}}</td>
                                                    <td tabindex="0">{{$valu->product_name}}<br><small>ID: {{$valu->id}}</small><small class="ml-2"> SKU: {{$valu->product_squ}}</small>
                                                        </td>
                                                    <td>{{$valu->current_stock}}</td>
                                                    <td>{{$valu->current_price}}</td>
                                                    <td>{{$valu->tag}}</td>
                                                    <td>$100</td>
                                                    <td>
                                                <div class="action-list"><select class="process select droplinks drop-success" style="display: none;">
                                                                <option data-val="1" value="https://royalscripts.com/demo/kingcommerce/admin/products/status/181/1" selected="">Activated</option>&lt;<option data-val="0" value="https://royalscripts.com/demo/kingcommerce/admin/products/status/181/0">Deactivated</option>/select&gt;
                                                            </select>
                                                            <div class="nice-select process select droplinks drop-success" tabindex="0"><span class="current">Activated</span>
                                                                <ul class="list">
                                                                    <li data-value="https://royalscripts.com/demo/kingcommerce/admin/products/status/181/1" class="option selected">Activated</li>
                                                                    <li data-value="https://royalscripts.com/demo/kingcommerce/admin/products/status/181/0" class="option">Deactivated</li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td>
                                                    <div class="godropdown">
                                                        <button class="go-dropdown-toggle"> Actions <i class="fas fa-chevron-down"></i>
                                                        </button>
                                                        <div class="action-list">
                                                            <a href="{{route('show_product',$valu->id)}}">
                                                            <i class="fas fa-edit"></i> show </a>
                                                            <a href="{{url('edit_product')}}">
                                                            <i class="fas fa-edit"></i> Edit </a>
                                                            <a href="{{route('delete_product',$valu->id)}}">
                                                            <i class="fas fa-delete"></i> Delete </a>
                                                            
                                                            
                                                        </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                               @endforeach
                                               
                                            </tbody>
                                        </table>
                                        <div id="geniustable_processing" class="dataTables_processing card" style="display: none;"><img src="https://royalscripts.com/demo/kingcommerce/assets/images/1564224329loading3.gif"></div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-12 col-md-5">
                                        <div class="dataTables_info" id="geniustable_info" role="status" aria-live="polite">Showing 1 to 10 of 37 entries</div>
                                    </div>
                                    <div class="col-sm-12 col-md-7">
                                        <div class="dataTables_paginate paging_simple_numbers" id="geniustable_paginate">
                                            <ul class="pagination">
                                                <li class="paginate_button page-item previous disabled" id="geniustable_previous"><a href="#" aria-controls="geniustable" data-dt-idx="0" tabindex="0" class="page-link">Previous</a></li>
                                                <li class="paginate_button page-item active"><a href="#" aria-controls="geniustable" data-dt-idx="1" tabindex="0" class="page-link">1</a></li>
                                                <li class="paginate_button page-item "><a href="#" aria-controls="geniustable" data-dt-idx="2" tabindex="0" class="page-link">2</a></li>
                                                <li class="paginate_button page-item "><a href="#" aria-controls="geniustable" data-dt-idx="3" tabindex="0" class="page-link">3</a></li>
                                                <li class="paginate_button page-item "><a href="#" aria-controls="geniustable" data-dt-idx="4" tabindex="0" class="page-link">4</a></li>
                                                <li class="paginate_button page-item next" id="geniustable_next"><a href="#" aria-controls="geniustable" data-dt-idx="5" tabindex="0" class="page-link">Next</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</x-admin_master>